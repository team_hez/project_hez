using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Hez
{
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class Dungeon : MonoBehaviour
    {
        public Room[] Rooms;
        [InlineEditor(InlineEditorModes.LargePreview), ReadOnly]
        public Mesh Mesh;
        [ReadOnly]
        public Vector3[] Normals;

        private void Awake() => Reload();

        [Button, DisableInEditorMode]
        private void Reload()
        {
            var vertices = new List<Vector3>();
            var triangles = new List<int>();
            var uv = new List<Vector2>();

            Action<int, int, int, int> addQuad = (x, z, width, length) =>
            {
                int baseIndex = vertices.Count;
                vertices.AddRange(new[]
                {
                    new Vector3(x, 0, z),
                    new Vector3(x + width, 0, z),
                    new Vector3(x, 0, z + length),
                    new Vector3(x + width, 0, z + length)
                });
                triangles.AddRange(new[]
                {
                    baseIndex,
                    baseIndex + 2,
                    baseIndex + 1,
                    baseIndex + 2,
                    baseIndex + 3,
                    baseIndex + 1
                });
                uv.AddRange(new[]
                {
                    new Vector2(0, 0),
                    new Vector2(1, 0),
                    new Vector2(0, 1),
                    new Vector2(1, 1)
                });
            };

            foreach (var room in Rooms)
            {
                addQuad(room.X, room.Z, room.Width, room.Length);
            }

            var meshFilter = gameObject.GetComponent<MeshFilter>();
            Mesh = meshFilter.mesh;
            Mesh.vertices = vertices.ToArray();
            Mesh.triangles = triangles.ToArray();
            Mesh.uv = uv.ToArray();
            Mesh.RecalculateNormals();
        }

        private void Update() => Normals = Mesh.normals;
    }

    [Serializable]
    public struct Room
    {
        [HorizontalGroup("Coord")]
        public int X;
        [HorizontalGroup("Coord")]
        public int Z;
        [HorizontalGroup("Size")]
        public int Width;
        [HorizontalGroup("Size")]
        public int Length;
    }
}