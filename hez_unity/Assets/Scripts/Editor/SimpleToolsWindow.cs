using Sirenix.OdinInspector.Editor;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;
using System.Linq;

namespace Hez
{
    [Serializable]
    public class ScriptOverride
    {
        private static readonly string _systemScriptTemplateDirPath = Path.Combine(
                EditorApplication.applicationContentsPath,
                "Resources", "ScriptTemplates");
        private static readonly string[] _filenames = new Func<string[]>(() => {
            string[] systemScriptTemplateFileNames = Directory
                .GetFiles(_systemScriptTemplateDirPath)
                .Select(fullpath => Path.GetFileName(fullpath)).ToArray();
            return systemScriptTemplateFileNames;
        })();

        [PropertyOrder(0), OnValueChanged(nameof(OnFileSelected)), ValueDropdown(nameof(_filenames))]
        public string FileToOverride;

        [PropertyOrder(2), BoxGroup("Source"), DisplayAsString(false), HideLabel]
        private string _srcText = "";
        [PropertyOrder(3), BoxGroup("Dest"), DisplayAsString(false), HideLabel]
        private string _destText = "";

        private string _srcFullPath
            => Path.Combine(
                Directory.GetParent(Application.dataPath).FullName,
                "ScriptTemplates", FileToOverride);
        private string _destFullPath
            => Path.Combine(_systemScriptTemplateDirPath, FileToOverride);

        [PropertyOrder(1), Button(ButtonSizes.Medium)]
        private void OverrideUnityScriptTemplates()
        {
            string templateFilename = FileToOverride;

            string srcFile = Path.Combine(
                Directory.GetParent(Application.dataPath).FullName,
                "ScriptTemplates",
                templateFilename);

            string destFile = Path.Combine(
                EditorApplication.applicationContentsPath,
                "Resources", "ScriptTemplates",
                templateFilename);

            File.Copy(srcFile, destFile, overwrite: true);
            Debug.Log("Overwrote " + templateFilename);
        }

        private void OnFileSelected()
        {
            try
            {
                _srcText = File.OpenText(_srcFullPath).ReadToEnd();
            }
            catch (Exception e)
            {
                _srcText = e.Message;
            }

            try
            {
                _destText = File.OpenText(_destFullPath).ReadToEnd();
            }
            catch (Exception e)
            {
                _destText = e.Message;
            }
        }
    }

    public class SimpleToolsWindow : OdinEditorWindow
    {
        [MenuItem(EditorConst.WindowMenuRoot + nameof(SimpleToolsWindow))]
        private static void Open()
            => GetWindow<SimpleToolsWindow>().Show();

        [TabGroup(nameof(ScriptOverride)), InlineProperty, HideLabel]
        public ScriptOverride ScriptOverride = new ScriptOverride();
    }
}