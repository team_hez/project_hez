using System;

namespace Hez
{
    public static class Util
    {
        public static void Repeat(int count, Action func)
        {
            for (int i = 0; i < count; ++i)
            {
                func();
            }
        }

        public static void Repeat(int count, Action<int> func)
        {
            for (int i = 0; i < count; ++i)
            {
                func(i);
            }
        }
    }

}
