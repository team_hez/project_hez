#include "sound.h"
#include "memory.h"

Sound::Sound()
{
}

Sound::~Sound()
{
}

void Sound::initDirectSound(HWND hwnd)
{
    // Initialize the direct sound interface pointer for the default sound device.
    HRESULT result = DirectSoundCreate8(NULL, &m_directSound, NULL);
    if (FAILED(result))
    {
        return;
    }

    // Set the cooperative level to priority so the format of the primary sound buffer can be modified.
    result = m_directSound->SetCooperativeLevel(hwnd, DSSCL_PRIORITY);
    if (FAILED(result))
    {
        return;
    }

    // Setup the primary buffer description.
    DSBUFFERDESC bufferDesc;
    bufferDesc.dwSize = sizeof(DSBUFFERDESC);
    bufferDesc.dwFlags = DSBCAPS_PRIMARYBUFFER | DSBCAPS_CTRLVOLUME;
    bufferDesc.dwBufferBytes = 0;
    bufferDesc.dwReserved = 0;
    bufferDesc.lpwfxFormat = NULL;
    bufferDesc.guid3DAlgorithm = GUID_NULL;

    // Get control of the primary sound buffer on the default sound device.
    result = m_directSound->CreateSoundBuffer(&bufferDesc, &m_primaryBuffer, NULL);
    if (FAILED(result))
    {
        return;
    }

    // Setup the format of the primary sound bufffer.
    // In this case it is a .WAV file recorded at 44,100 samples per second in 16-bit stereo (cd audio format).
    WAVEFORMATEX waveFormat;
    waveFormat.wFormatTag = WAVE_FORMAT_PCM;
    waveFormat.nSamplesPerSec = 44100;
    waveFormat.wBitsPerSample = 16;
    waveFormat.nChannels = 2;
    waveFormat.nBlockAlign = (waveFormat.wBitsPerSample / 8) * waveFormat.nChannels;
    waveFormat.nAvgBytesPerSec = waveFormat.nSamplesPerSec * waveFormat.nBlockAlign;
    waveFormat.cbSize = 0;

    // Set the primary buffer to be the wave format specified.
    result = m_primaryBuffer->SetFormat(&waveFormat);
    if (FAILED(result))
    {
        return;
    }
}

void Sound::shutdownDirectSound()
{
    // Release the primary sound buffer pointer.
    if (m_primaryBuffer != nullptr)
    {
        m_primaryBuffer->Release();
        m_primaryBuffer = nullptr;
    }

    // Release the direct sound interface pointer.
    if (m_directSound != nullptr)
    {
        m_directSound->Release();
        m_directSound = nullptr;
    }
}

void Sound::loadWaveFile(StringView path, IDirectSoundBuffer8** secondaryBuffer, MemoryArena& arena)
{
    // Open the wave file in binary.
    FILE* filePtr;

    int error = fopen_s(&filePtr, path.data(), "rb");
    if (error != 0)
    {
        return;
    }

    // Read in the wave file header.
    WaveFile waveFile;
    u64 count = fread(&waveFile, sizeof(waveFile), 1, filePtr);
    if (count != 1)
    {
        return;
    }

    // Check that the chunk ID is the RIFF format.
    if ((waveFile.m_chunkID[0] != 'R') || (waveFile.m_chunkID[1] != 'I') ||
        (waveFile.m_chunkID[2] != 'F') || (waveFile.m_chunkID[3] != 'F'))
    {
        return;
    }

    // Check that the file format is the WAVE format.
    if ((waveFile.m_format[0] != 'W') || (waveFile.m_format[1] != 'A') ||
        (waveFile.m_format[2] != 'V') || (waveFile.m_format[3] != 'E'))
    {
        return;
    }

    // Check that the sub chunk ID is the fmt format.
    if ((waveFile.m_subChunkID[0] != 'f') || (waveFile.m_subChunkID[1] != 'm') ||
        (waveFile.m_subChunkID[2] != 't') || (waveFile.m_subChunkID[3] != ' '))
    {
        return;
    }

    // Check that the audio format is WAVE_FORMAT_PCM.
    if (waveFile.m_audioFormat != WAVE_FORMAT_PCM)
    {
        return;
    }

    // Check that the wave file was recorded in stereo format.
    if (waveFile.m_numChannels != 2)
    {
        return;
    }

    // Check that the wave file was recorded at a sample rate of 44.1 KHz.
    if (waveFile.m_sampleRate != 44100)
    {
        return;
    }

    // Ensure that the wave file was recorded in 16 bit format.
    if (waveFile.m_bitsPerSample != 16)
    {
        return;
    }

    // Check for the data chunk header.
    if ((waveFile.m_dataChunkID[0] != 'd') || (waveFile.m_dataChunkID[1] != 'a') ||
        (waveFile.m_dataChunkID[2] != 't') || (waveFile.m_dataChunkID[3] != 'a'))
    {
        return;
    }

    // Set the wave format of secondary buffer that this wave file will be loaded onto.
    WAVEFORMATEX wave_format;
    wave_format.wFormatTag = WAVE_FORMAT_PCM;
    wave_format.nSamplesPerSec = 44100;
    wave_format.wBitsPerSample = 16;
    wave_format.nChannels = 2;
    wave_format.nBlockAlign = (wave_format.wBitsPerSample / 8) * wave_format.nChannels;
    wave_format.nAvgBytesPerSec = wave_format.nSamplesPerSec * wave_format.nBlockAlign;
    wave_format.cbSize = 0;

    // Set the buffer description of the secondary sound buffer that the wave file will be loaded onto.
    DSBUFFERDESC buffer_desc;
    buffer_desc.dwSize = sizeof(DSBUFFERDESC);
    buffer_desc.dwFlags = DSBCAPS_CTRLVOLUME;
    buffer_desc.dwBufferBytes = waveFile.m_dataSize;
    buffer_desc.dwReserved = 0;
    buffer_desc.lpwfxFormat = &wave_format;
    buffer_desc.guid3DAlgorithm = GUID_NULL;

    // Create a temporary sound buffer with the specific buffer settings.
    IDirectSoundBuffer* tempBuffer;
    HRESULT result = m_directSound->CreateSoundBuffer(&buffer_desc, &tempBuffer, NULL);
    if (FAILED(result))
    {
        return;
    }

    // Test the buffer format against the direct sound 8 interface and create the secondary buffer.
    result = tempBuffer->QueryInterface(IID_IDirectSoundBuffer8, (void**)&*secondaryBuffer);
    if (FAILED(result))
    {
        return;
    }

    // Release the temporary buffer.
    tempBuffer->Release();
    tempBuffer = nullptr;

    // Move to the beginning of the wave data which starts at the end of the data chunk header.
    fseek(filePtr, sizeof(WaveFile), SEEK_SET);

    // Create a temporary buffer to hold the wave file data.

    // TODO(illkwon): 힙 할당 제거
    //u8* waveData = new u8[waveFile.m_dataSize];
    u8* waveData = arena.push<u8>(waveFile.m_dataSize);

    // Read in the wave file data into the newly created buffer.
    count = fread(waveData, 1, waveFile.m_dataSize, filePtr);
    if (count != waveFile.m_dataSize)
    {
        return;
    }

    // Close the file once done reading.
    error = fclose(filePtr);
    if (error != 0)
    {
        return;
    }

    // Lock the secondary buffer to write wave data into it.
    u8* bufferPtr;
    u32 bufferSize;
    result = (*secondaryBuffer)->Lock(0, waveFile.m_dataSize, (void**)&bufferPtr, (DWORD*)&bufferSize, NULL, 0, 0);
    if (FAILED(result))
    {
        return;
    }

    // Copy the wave data into the buffer.
    memcpy(bufferPtr, waveData, waveFile.m_dataSize);

    // Unlock the secondary buffer after the data has been written to it.
    result = (*secondaryBuffer)->Unlock((void*)bufferPtr, bufferSize, NULL, 0);
    if (FAILED(result))
    {
        return;
    }

    // Release the wave data since it was copied into the secondary buffer.
    //delete[] waveData;
    //waveData = nullptr;
}

void Sound::unloadWaveFile(IDirectSoundBuffer8* secondaryBuffer)
{
    if (secondaryBuffer != nullptr)
    {
        secondaryBuffer->Release();
        secondaryBuffer = nullptr;
    }
}

void Sound::playWaveFile(IDirectSoundBuffer8* secondaryBuffer, bool isLooping, DWORD pos)
{
    // Set position at the beginning of the sound buffer.
    HRESULT result = secondaryBuffer->SetCurrentPosition(pos);
    if (FAILED(result))
    {
        return;
    }

    // Set volume of the buffer to 100%.
    result = secondaryBuffer->SetVolume(DSBVOLUME_MAX);
    if (FAILED(result))
    {
        return;
    }

    // Play the contents of the secondary sound buffer.
    if (isLooping == true)
    {
        result = secondaryBuffer->Play(0, 0, DSBPLAY_LOOPING);
    }
    else
    {
        result = secondaryBuffer->Play(0, 0, 0);
    }
    if (FAILED(result))
    {
        return;
    }
}

DWORD Sound::pauseWaveFile(IDirectSoundBuffer8* secondaryBuffer)
{
    DWORD pausedPos;
    HRESULT result = secondaryBuffer->GetCurrentPosition(&pausedPos, nullptr);
    if (FAILED(result))
    {
        return 0;
    }

    result = secondaryBuffer->Stop();
    if (FAILED(result))
    {
        return 0;
    }

    return pausedPos;
}

void Sound::resumeWaveFile(IDirectSoundBuffer8* secondaryBuffer, bool isLooping)
{
    // Set volume of the buffer to 100%.
    HRESULT result = secondaryBuffer->SetVolume(DSBVOLUME_MAX);
    if (FAILED(result))
    {
        return;
    }

    // Play the contents of the secondary sound buffer.
    if (isLooping == true)
    {
        result = secondaryBuffer->Play(0, 0, DSBPLAY_LOOPING);
    }
    else
    {
        result = secondaryBuffer->Play(0, 0, 0);
    }
    if (FAILED(result))
    {
        return;
    }
}

void Sound::setVolumeDirectly(IDirectSoundBuffer8* secondaryBuffer, i32 volume)
{
    //Volume range is -10000 to 0(source volume).
    //if 1 volume changed 1/100 dB changed (same as 1 mB)

    //also dB use common logarithm unit. you must consider this.

    HRESULT result = secondaryBuffer->SetVolume((LONG)volume);
    if (FAILED(result))
    {
        return;
    }
}

i32 Sound::calculateVolume(f32 percentage)
{
    if (percentage >= 1.0f)
    {
        return VOLUME_MAX;
    }

    if (percentage <= 0.0f)
    {
        return VOLUME_MIN;
    }

    //auto result = -100.0f * powf(10.0f, (0.5f / percentage));
    return i32(2000.0f * log10f(percentage));
}

void Sound::stopWaveFile(IDirectSoundBuffer8* secondaryBuffer)
{
    HRESULT result = secondaryBuffer->Stop();
    if (FAILED(result))
    {
        return;
    }

    result = secondaryBuffer->SetCurrentPosition(0);
    if (FAILED(result))
    {
        return;
    }
}

