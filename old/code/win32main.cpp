#include <cstdio>
#include <cstring>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <bounce\bounce.h>
#include "editor.h"
#include "general.h"
#include "asset.h"
#include "renderergl.h"
#include "sound.h"
#include "input.h"
#include "camera.h"
#include "tilemap.h"
#include "frame.h"
#include "platform.h"
#include "game.h"

constexpr int WINDOW_WIDTH = 1280;
constexpr int WINDOW_HEIGHT = 720;

static bool g_running;

void platformSetShowCursor(bool show)
{
    if (show)
    {
        while (ShowCursor(show) < 0);
    }
    else
    {
        while (ShowCursor(show) >= 0);
    }
}

LRESULT CALLBACK win32MainWindowCallback(HWND window, UINT message, WPARAM wp, LPARAM lp)
{
    LRESULT result = 0;

    switch (message)
    {
    case WM_EXITSIZEMOVE:
    {
        //auto dim = win32_get_window_dim(window);
        //LOG("WM_EXITSIZEMOVE from main_window_callback: %d %d\n", dim.x, dim.y);
    } break;
    case WM_SIZE:
    {
    } break;

    case WM_CLOSE:
    case WM_DESTROY:
    {
        g_running = false;
    } break;

    case WM_SYSKEYDOWN:
    case WM_SYSKEYUP:
    case WM_KEYDOWN:
    case WM_KEYUP:
    {
        ASSERT(!"키입력 이벤트가 이 콜백으로 들어오면 안됨");
    } break;

    default:
    {
        result = DefWindowProc(window, message, wp, lp);
    } break;
    }

    return result;
}

static bool win32HandleMessage(HWND window, GameInput& input)
{
    MSG msg;
    bool shouldQuit = false;

    while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
    {
        WPARAM wp = msg.wParam;
        LPARAM lp = msg.lParam;

        switch (msg.message)
        {
        case WM_QUIT:
        {
            shouldQuit = true;
        } break;

        case WM_SYSKEYDOWN:
        case WM_SYSKEYUP:
        case WM_KEYDOWN:
        case WM_KEYUP:
        {
            uint keyCode = (uint)(wp);
            bool wasDown = ((lp & (1 << 30)) != 0);
            bool isDown = ((lp & (1 << 31)) == 0);
            if (wasDown != isDown)
            {
                switch (keyCode)
                {
                case 'W':
                    input.m_moveUp.processInputEvent(isDown); break;
                case 'A':
                    input.m_moveLeft.processInputEvent(isDown); break;
                case 'S':
                    input.m_moveDown.processInputEvent(isDown); break;
                case 'D':
                    input.m_moveRight.processInputEvent(isDown); break;
                case VK_UP:
                    input.m_actionUp.processInputEvent(isDown); break;
                case VK_LEFT:
                    input.m_actionLeft.processInputEvent(isDown); break;
                case VK_DOWN:
                    input.m_actionDown.processInputEvent(isDown); break;
                case VK_RIGHT:
                    input.m_actionRight.processInputEvent(isDown); break;
                case VK_SPACE:
                    input.m_actionJump.processInputEvent(isDown); break;
                case VK_SHIFT:
                    input.m_actionRun.processInputEvent(isDown); break;
                case VK_MENU:
                    input.m_actionRoll.processInputEvent(isDown); break;
                case VK_F1:
                    input.m_debugButton[0].processInputEvent(isDown); break;
                case VK_F2:
                    input.m_debugButton[1].processInputEvent(isDown); break;
                case VK_F3:
                    input.m_debugButton[2].processInputEvent(isDown); break;
                case VK_F4:
                    input.m_debugButton[3].processInputEvent(isDown); break;
                case VK_F5:
                    input.m_debugButton[4].processInputEvent(isDown); break;
                case VK_F6:
                    input.m_debugButton[5].processInputEvent(isDown); break;
                }
            }

            bool altWasDown = ((lp & (1 << 29)) != 0);
            if ((keyCode == VK_F4 && altWasDown) || keyCode == VK_ESCAPE)
            {
                shouldQuit = true;
            }
        } break;

        case WM_LBUTTONDOWN:
            input.m_mouseLeft.processInputEvent(true);
            break;
        case WM_LBUTTONUP:
            input.m_mouseLeft.processInputEvent(false);
            break;
        default:
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        }
    }

    return shouldQuit;
}

class Win32WindowDimension
{
public:
    int width;
    int height;
};
static Win32WindowDimension win32GetWindowDimension(HWND window)
{
    RECT clientRect;
    GetClientRect(window, &clientRect);
    return { clientRect.right - clientRect.left, clientRect.bottom - clientRect.top };
}

int main()
{
    timeBeginPeriod(1);

#ifdef DEBUG
    LPVOID baseAddress = reinterpret_cast<LPVOID>(tb(2));

    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF); 
    _CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG); 
    _CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_DEBUG);
    _CrtSetBreakAlloc((-1));

#else
    LPVOID baseAddress = nullptr;
#endif

    GameMemory gameMemory = {};
    {
        gameMemory.m_persistentMemorySize = mb(100);
        gameMemory.m_tempMemorySize = mb(100);
        gameMemory.m_totalSize = gameMemory.m_persistentMemorySize + gameMemory.m_tempMemorySize;
        void* mem = VirtualAlloc(baseAddress, gameMemory.m_totalSize, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
        gameMemory.m_persistentMemory = mem;
        gameMemory.m_tempMemory = (u8*)mem + gameMemory.m_persistentMemorySize;
    }

    auto tempArena = (MemoryArena*)gameMemory.m_tempMemory;
    tempArena->init(
        (u8*)gameMemory.m_tempMemory + sizeof(*tempArena),
        gameMemory.m_tempMemorySize - sizeof(*tempArena));
    auto globalLoggerBufferArena = tempArena->makeSubArena(kb(1));
    debugInitGlobalLogger(globalLoggerBufferArena->m_memory, globalLoggerBufferArena->m_size);
    tempArena->beginTemp();

    HINSTANCE instance = GetModuleHandle(0);

    WNDCLASSEX windowClass = {};
    windowClass.cbSize = sizeof(windowClass);
    windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    windowClass.lpfnWndProc = win32MainWindowCallback;
    windowClass.hInstance = instance;
    windowClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
    //windowClass.hbrBackground = ;
    windowClass.lpszClassName = L"HandmadeHeroWindowClass";

    if (!RegisterClassEx(&windowClass))
    {
        DWORD error = GetLastError();
        LOG("RegisterClassEx failed: %d", error);
        return 0;
    }

    HWND fakeWindow = CreateWindowEx(0, windowClass.lpszClassName,
        L"fake",
        WS_OVERLAPPEDWINDOW,//|WS_VISIBLE,
        CW_USEDEFAULT, CW_USEDEFAULT,
        WINDOW_WIDTH, WINDOW_HEIGHT,
        0, 0, instance, 0);
    if (!fakeWindow)
    {
        LOG("CreateWindowEx failed");
        return 0;
    }
    
    HDC fakeDC = GetDC(fakeWindow);

    PIXELFORMATDESCRIPTOR fakePfd = {};
    fakePfd.nSize = sizeof(fakePfd);
    fakePfd.nVersion = 1;
    fakePfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    fakePfd.iPixelType = PFD_TYPE_RGBA;
    fakePfd.cColorBits = 32;
    fakePfd.cDepthBits = 24;
    fakePfd.cStencilBits = 8;
    fakePfd.iLayerType = PFD_MAIN_PLANE;

    int fakePixelFormat = ChoosePixelFormat(fakeDC, &fakePfd);
    if (fakePixelFormat == 0)
    {
        LOG("ChoosePixelFormat failed");
        return 0;
    }

    SetPixelFormat(fakeDC, fakePixelFormat, &fakePfd);
    HGLRC fakeRC = wglCreateContext(fakeDC);
    if (fakeRC == 0)
    {
        LOG("wglCreateContext failed");
        return 0;
    }
    if (!wglMakeCurrent(fakeDC, fakeRC))
    {
        LOG("wglMakeContext failed");
        return 0;
    }

    if (!gladLoadGL())
    {
        LOG("Failed to load extensions");
        return 0;
    }

    HWND window = CreateWindowEx(0, windowClass.lpszClassName,
        L"mogue",
        WS_OVERLAPPEDWINDOW | WS_VISIBLE,
        CW_USEDEFAULT, CW_USEDEFAULT,
        WINDOW_WIDTH, WINDOW_HEIGHT,
        0, 0, instance, 0);
    if (!window)
    {
        LOG("CreateWindowEx failed");
        return 0;
    }
    HDC dc = GetDC(window);

    if (!gladLoadWGL(dc))
    {
        LOG("Failed to load extensions");
        return 0;
    }

    int pixelAttribs[] =
    {
        WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
        WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
        WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
        WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
        WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
        WGL_COLOR_BITS_ARB, 32,
        WGL_ALPHA_BITS_ARB, 8,
        WGL_DEPTH_BITS_ARB, 24,
        WGL_STENCIL_BITS_ARB, 8,
        WGL_SAMPLE_BUFFERS_ARB, GL_TRUE,
        WGL_SAMPLES_ARB, 4,
        0
    };

    int pixelFormat;
    UINT numFormats;
    bool status = wglChoosePixelFormatARB(dc, pixelAttribs, nullptr, 1, &pixelFormat, &numFormats);
    if (!status || numFormats == 0)
    {
        LOG("wglChoosePixelFormatARB failed");
        return 0;
    }

    PIXELFORMATDESCRIPTOR pfd;
    DescribePixelFormat(dc, pixelFormat, sizeof(pfd), &pfd);
    SetPixelFormat(dc, pixelFormat, &pfd);
    const int MAJOR_MIN = 4, MINOR_MIN = 5;
    int contextAttribs[] =
    {
        WGL_CONTEXT_MAJOR_VERSION_ARB, MAJOR_MIN,
        WGL_CONTEXT_MINOR_VERSION_ARB, MINOR_MIN,
        WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
        0
    };
    HGLRC rc = wglCreateContextAttribsARB(dc, 0, contextAttribs);
    if (!rc)
    {
        LOG("wglCreateContextAttribsARB failed");
        return 0;
    }

    wglMakeCurrent(0, 0);
    wglDeleteContext(fakeRC);
    ReleaseDC(fakeWindow, fakeDC);
    DestroyWindow(fakeWindow);
    if (!wglMakeCurrent(dc, rc))
    {
        LOG("wglMakeCurrent failed");
        return 0;
    }

#ifdef GLAD_DEBUG
    glad_set_pre_callback([](const char*, void*, int, ...)
    {
        clearGLError();
    });
    glad_set_post_callback([](const char* name, void*, int, ...)
    {
        ASSERT(checkGLError(name));
    });
#endif//GLAD_DEBUG

    LOG("OpenGL version: %s", glGetString(GL_VERSION));
    LOG("GLSL version: %s", glGetString(GL_SHADING_LANGUAGE_VERSION));

    float r = 0.8f;
    float increment = 0.01f;

    wglSwapIntervalEXT(0);
    glDepthFunc(GL_LESS);

    GameInput input(1);

    EditorGui editor;

    auto gameState = (GameState*)gameMemory.m_persistentMemory;
    gameState->init(gameMemory, *tempArena);

    g_running = true;
    debugCameraStateEnter(gameState->m_cameraMoveState, window, input);

    tempArena->endTemp();

    Frame frame(60, true);

    while (g_running)
    {
        tempArena->beginTemp();

        // Reset input
        for (auto& button : input.m_buttons)
        {
            button.m_isPressed = false;
            button.m_isReleased = false;
        }

        // Update input
        if (bool shouldQuit = win32HandleMessage(window, input)
            ; shouldQuit)
        {
            break;
        }

        // Get window coordinates
        auto windowDim = win32GetWindowDimension(window);

        POINT mousePointInWindow = {};
        bool isWindowActive = (GetActiveWindow() == window);

        // Check Focus of window 
        if (isWindowActive)
        {
            POINT mousePointInDisplay;
            GetCursorPos(&mousePointInDisplay);
            mousePointInWindow = mousePointInDisplay;
            ScreenToClient(window, &mousePointInWindow);
        }

        input.m_mouseX = mousePointInWindow.x;
        input.m_mouseY = mousePointInWindow.y;
        input.m_dt = frame.m_vSync ? (1.f / frame.m_targetFps) : frame.m_dt;
        input.m_isWindowActive = isWindowActive;
        input.m_windowWidth = windowDim.width;
        input.m_windowHeight = windowDim.height;

        gameState->update(window, input, frame, *tempArena);

        tempArena->endTemp();

        SwapBuffers(dc);

        frame.update();
    }

    gameState->shutdown();
    VirtualFree(gameMemory.m_persistentMemory, gameMemory.m_totalSize, MEM_RELEASE);
    
    return 0;
}