#include "collisionsystem.h"
#include "game.h"

CollisionSystem::CollisionSystem(b3World& world, MemoryArena& arena)
    : m_world(world)
{
    m_world.SetContactListener(this);
    m_hitBoxes = arena.push<HitBox>(MAX_HITBOX_COUNT);
    m_lastFree = (FreeHitBoxHeader*)m_hitBoxes;
    for (int i = 0; i < MAX_HITBOX_COUNT; ++i)
    {
        auto curr = (FreeHitBoxHeader*)(m_hitBoxes + i);
        auto next = i < (MAX_HITBOX_COUNT - 1) ?
            (FreeHitBoxHeader*)(m_hitBoxes + i + 1) : nullptr;
        curr->m_next = next;
    }
    m_activeHitBoxCount = 0;
}

CollisionSystem::~CollisionSystem()
{
    m_lastFree = nullptr;
    for (int i = 0; i < m_activeHitBoxCount; ++i)
    {
        m_world.DestroyBody(m_activeHitBoxes[i]->m_hitBoxBody);
    }
    m_activeHitBoxCount = 0;
}

void CollisionSystem::BeginContact(b3Contact* contact)
{
    auto shapeA = contact->GetShapeA();
    auto shapeB = contact->GetShapeB();

    auto bodyA = shapeA->GetBody();
    auto bodyB = shapeB->GetBody();

    auto shapeDataA = shapeA->GetUserData();
    auto shapeDataB = shapeB->GetUserData();

    if (shapeDataA != nullptr && shapeDataB != nullptr)
    {
        CollisionType shapeDataAType = getCollisionType(shapeDataA);
        CollisionType shapeDataBType = getCollisionType(shapeDataB);

        if (shapeDataAType == CollisionType::Entity && shapeDataBType == CollisionType::Entity)
        {

        }
        else if (shapeDataAType == CollisionType::Entity && shapeDataBType == CollisionType::HitBox)
        {
            auto hitBox = (HitBox*)shapeDataB;
            auto entity = (Entity*)shapeDataA;
            entity->ApplyDamageInfo(hitBox->m_info);
        }
        else if (shapeDataAType == CollisionType::HitBox && shapeDataBType == CollisionType::Entity)
        {
            auto hitBox = (HitBox*)shapeDataA;
            auto entity = (Entity*)shapeDataB;
            entity->ApplyDamageInfo(hitBox->m_info);
        }

        else if (shapeDataAType == CollisionType::Item && shapeDataBType == CollisionType::Entity)
        {
            auto item = (Item*)shapeDataA;
            auto entity = (Entity*)shapeDataB;

            if (entity->m_entityType == EntityType::Player)
            {
                auto player = (Player*)entity;
                
            }
        }

        else if (shapeDataAType == CollisionType::Entity && shapeDataBType == CollisionType::Item)
        {
            auto item = (Item*)shapeDataB;
            auto entity = (Entity*)shapeDataA;

            if (entity->m_entityType == EntityType::Player)
            {
                auto player = (Player*)entity;
                
            }
        }
    }
}

void CollisionSystem::EndContact(b3Contact* contact)
{
    contact;
}

void CollisionSystem::PreSolve(b3Contact* contact)
{
    auto bodyA = contact->GetShapeA()->GetBody();
    auto bodyB = contact->GetShapeB()->GetBody();
}

HitBox* CollisionSystem::createHitBox(const Vec3& hitBoxPos, const Vec3& hitBoxScale, const DamageInfo& info)
{
    ASSERT(m_lastFree != nullptr);
    HitBox* hitBox = (HitBox*)m_lastFree;
    m_lastFree = m_lastFree->m_next;
    hitBox->m_colType = CollisionType::HitBox;
    hitBox->m_info = info;

    b3BodyDef hitBoxBodyDef;
    hitBoxBodyDef.position.Set(hitBoxPos.x, hitBoxPos.y, hitBoxPos.z);
    hitBoxBodyDef.fixedRotationX = true;
    hitBoxBodyDef.fixedRotationY = true;
    hitBoxBodyDef.fixedRotationZ = true;
    hitBoxBodyDef.type = e_staticBody;
    hitBox->m_hitBoxBody = m_world.CreateBody(hitBoxBodyDef);

    hitBox->m_hitBoxHull.Set(hitBoxScale.x, hitBoxScale.y, hitBoxScale.z);

    b3HullShape hull;
    hull.m_hull = &hitBox->m_hitBoxHull;

    b3ShapeDef hitBoxShapeDef;
    hitBoxShapeDef.shape = &hull;
    hitBoxShapeDef.density = 1.0f;
    hitBoxShapeDef.friction = 0.0f;
    hitBoxShapeDef.restitution = 0.0f;
    hitBoxShapeDef.isSensor = true;

    auto shape = hitBox->m_hitBoxBody->CreateShape(hitBoxShapeDef);
    shape->SetUserData(hitBox);

    m_activeHitBoxes[m_activeHitBoxCount++] = hitBox;
    return hitBox;
}

// TODO(illkwon): 테스트 필요. 아직 호출부가 없음.
// TEST(jangseok): 테스트 결과 업데이트 루프 도중에 삭제할때는 정상적으로 작동.
// 초기화 단계에서 생성하고 바로 삭제시에 바운스 라이브러리 자체에서 크래시.
// 크래시 콜스택
// GameState::Update
// b3World::Step
// b3ContactManager::FindNewContracts
// b3BroadPhase::FIndNewPairs
// b3ContactManager::AddPairs
// Exception thrown: read access violation.

void CollisionSystem::destroyHitBox(HitBox* hitBox)
{
    bool isHitBoxActive = false;
    for (int i = 0; i < m_activeHitBoxCount; ++i)
    {
        if (hitBox == m_activeHitBoxes[i])
        {
            isHitBoxActive = true;
            for (int j = i; j < m_activeHitBoxCount - 1; ++j)
            {
                m_activeHitBoxes[j] = m_activeHitBoxes[j + 1];
            }
            m_activeHitBoxes[m_activeHitBoxCount - 1] = nullptr;
            --m_activeHitBoxCount;
            break;
        }
    }
    ASSERT(isHitBoxActive);

    m_world.DestroyBody(hitBox->m_hitBoxBody);

    auto free = (FreeHitBoxHeader*)hitBox;
    free->m_next = m_lastFree;
    m_lastFree = free;
}

bool CollisionSystem::isDataIsCollisionType(void* data, CollisionType type)
{
    return (data && *((CollisionType*)data) == type);
}

CollisionType CollisionSystem::getCollisionType(void* data)
{
    return *((CollisionType*)data);
}

DamageInfo::DamageInfo(float hp, float sp)
    :m_deltaHP(hp), m_deltaSP(sp)
{
}
