#pragma once
#ifdef _DEBUG
#include <glad\glad_debug.h>
#include <glad\glad_wgl_debug.h>
#else
#include <glad\glad.h>
#include <glad\glad_wgl.h>
#endif
#include "general.h"
#include "camera.h"

/*#define GLCALL(x)\
(clearGLError(), x);\
ASSERT(checkGLError(#x, __FILE__, __LINE__))*/

uint createShaderFromFile(const char* filename);
uint createShader(const char* source);
void clearGLError();
bool checkGLError(const char* function, const char* file, int line);
bool checkGLError(const char* function);


class Light
{
public:
    Light()
    {

    }
    Light(Vec3 position, Vec3 color, float power) : m_position(position), m_color(color), m_power(power)
    {

    }
    ~Light()
    {

    }

    Vec3 m_position = { 0, 0, 0 };
    Vec3 m_color;
    float m_power = 10000.0f;

private:
};

class LightManager
{
public:
    LightManager()
    {

    }

    ~LightManager()
    {

    }

    void init(unsigned int count, MemoryArena& arena)
    {
        // TODO:: Light management determination
        arena.push(&m_Lights, count);

        /*
        for (int i = 0; i < count; ++i)
        {
        new (&m_Lights[i]) Light();
        }
        */

        new (&m_Lights[0]) Light(glm::vec3(5.0f, 5.0f, 5.0f), glm::vec3(1.0f, 1.0f, 1.0f), 500.0f);
        new (&m_Lights[1]) Light(glm::vec3(0.0f, 0.0f, 5.0f), glm::vec3(1.0f, 1.0f, 1.0f), 5000.0f);
        new (&m_Lights[2]) Light(glm::vec3(0.0f, 10.0f, 5.0f), glm::vec3(1.0f, 1.0f, 1.0f), 5000.0f);
        new (&m_Lights[3]) Light(glm::vec3(10.0f, 0.0f, 5.0f), glm::vec3(1.0f, 1.0f, 1.0f), 5000.0f);
        new (&m_Lights[4]) Light(glm::vec3(10.0f, 10.0f, 5.0f), glm::vec3(0.5f, 0.5f, 0.5f), 5000.0f);

        maxSize = 5;
        currSize = 5;
    }

    int maxSize;
    int currSize;
    Light* m_Lights = nullptr;

private:
};

//TODO(illkwon): Opengl를 prefix로 붙이는건 의미없는듯함.
class OpenglVertex
{
public:
    Vec3 m_pos;
    Vec3 m_color;
    Vec3 m_normal;
    //Vec2 m_uv;
};

class OpenglShader
{
public:
    uint m_internalID = 0;

    OpenglShader() = default;
    OpenglShader(const char* source)
    {
        m_internalID = createShader(source);
    }
};

class OpenglRenderable
{
public:
    uint m_vao; // vertex array object
    uint m_vbo; // vertex buffer object
    uint m_ebo; // element buffer object

    int m_indexCount;

    void draw(
        OpenglShader shaderID,
        const Mat4& modelMatrix,
        const Mat4& viewMatrix,
        const Mat4& projMatrix,
        const Light& light);

    void draw(
        OpenglShader shaderID,
        const Mat4& modelMatrix,
        const Mat4& viewMatrix,
        const Mat4& projMatrix
    );
};

class OpenglMesh
{
public:
    void box(MemoryArena& arena);
    void quad(MemoryArena& arena);
    OpenglRenderable createMeshRenderable();
    
public:
    int m_vertexCount;
    int m_indexCount;
    OpenglVertex* m_vertices;
    uint* m_indices;
};

OpenglRenderable createOpenglRenderable(OpenglVertex* vertices, int vertexCount, uint* indices, int indexCount);
Mat4 createTransform(float xPos = 0.0f, float yPos = 0.0f, float zPos = 0.0f, float xScl = 1.0f, float yScl = 1.0f, float zScl = 1.0f, float xRot = 0.0f, float yRot = 0.0f, float zRot = 0.0f);

//
// Debug stuff
//

class DebugDrawVertex
{
public:
    Vec3 m_pos;
};