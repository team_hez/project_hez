#pragma once
#include <glm\glm.hpp>
#include <glm\gtc\constants.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <bounce\common\math\mat.h>
#include <bounce\common\math\vec2.h>
#include <bounce\common\math\vec3.h>
#include <bounce\common\math\quat.h>

constexpr float PI_F = glm::pi<float>();
constexpr float TWO_PI_F = glm::two_pi<float>();
constexpr float HALF_PI_F = glm::half_pi<float>();

using Vec2 = glm::vec2;
using Vec3 = glm::vec3;
using Vec4 = glm::vec4;
using Mat4 = glm::mat4;

inline constexpr float radToDeg(float radians)
{
    return glm::degrees(radians);
}
inline constexpr float degToRad(float degrees)
{
    return glm::radians(degrees);
}

inline Mat4 scale(const Mat4& m, Vec3 v)
{
    return glm::scale(m, v);
}
inline Mat4 rotate(const Mat4& m, float radianAngle, Vec3 axis)
{
    return glm::rotate(m, radianAngle, axis);
}
inline Mat4 translate(const Mat4& m, Vec3 v)
{
    return glm::translate(m, v);
}
inline Mat4 perspective(float fovy, float aspect, float zNear, float zFar)
{
    return glm::perspective(fovy, aspect, zNear, zFar);
}
inline Mat4 ortho(float left, float right, float bottom, float top, float zNear, float zFar)
{
    return glm::ortho(left, right, bottom, top, zNear, zFar);
}
inline Mat4 lookAt(Vec3 eye, Vec3 center, Vec3 up)
{
    return glm::lookAt(eye, center, up);
}
inline const float* rawPtr(const Mat4& m)
{
    return glm::value_ptr(m);
}

inline float length(Vec3 v)
{
    return glm::length(v);
}
inline Vec3 normalize(Vec3 v)
{
    return glm::normalize(v);
}
inline float dot(Vec3 a, Vec3 b)
{
    return glm::dot(a, b);
}
inline Vec3 cross(Vec3 a, Vec3 b)
{
    return glm::cross(a, b);
}

inline Vec3 quatToEuler(const b3Quat& quat)
{
    // roll (x-axis rotation)
    float roll = atan2f(2.f * (quat.w * quat.x + quat.y * quat.z), 1.f - (2.f * (quat.x * quat.x + quat.y * quat.y)));

    // pitch (y-axis rotation)
    float pitch = 0.0f;
    float sinp = 2.0f * (quat.w * quat.y - quat.z * quat.x);
    
    if (fabsf(sinp) >= 1)
        pitch = copysignf(HALF_PI_F, sinp); // use 90 degrees if out of range
    else
        pitch = asinf(sinp);

    // yaw (z-axis rotation)
    float yaw =  atan2f(2.f * (quat.w * quat.z + quat.x * quat.y), 1.f - (2.f * (quat.y * quat.y + quat.z * quat.z)));

    return Vec3(roll, pitch, yaw);
}

inline b3Quat eulerToQuat(float xRotation, float yRotation, float zRotation)
{
    b3Quat result;

    float roll  = xRotation * 0.5f;
    float pitch = yRotation * 0.5f;
    float yaw   = zRotation * 0.5f;
    
    float cosRoll = cosf(roll);
    float sinRoll = sinf(roll);
    
    float cosPitch = cosf(pitch);
    float sinPitch = sinf(pitch);
    
    float cosYaw = cosf(yaw);
    float sinYaw = sinf(yaw);

    result.w = cosRoll * cosPitch * cosYaw + sinRoll * sinPitch * sinYaw;
    result.x = sinRoll * cosPitch * cosYaw - cosRoll * sinPitch * sinYaw;
    result.y = cosRoll * sinPitch * cosYaw + sinRoll * cosPitch * sinYaw;
    result.z = cosRoll * cosPitch * sinYaw - sinRoll * sinPitch * cosYaw;

    return result;
}