#pragma once
#include "platform.h"
#include <timeapi.h>

class Frame
{
public:
    Frame()
    {
        init();
    }
    Frame(uint fps, bool vSync) : m_targetFps(fps), m_vSync(vSync)
    {
        init();
    }

    void init()
    {
        if (timeGetDevCaps(&m_timecaps, sizeof(TIMECAPS)) == TIMERR_NOERROR)
        {
            LOG("minimal relinquish period (ms) : %d", m_timecaps.wPeriodMin);
            LOG("maximum relinquish period (ms) : %d", m_timecaps.wPeriodMax);
        }
        else
        {
            LOG("Framerate calculation failed");
            ASSERT(timeGetDevCaps(&m_timecaps, sizeof(TIMECAPS)));
        }

        DEVMODE lpDevMode;
        DWORD currentModeID = 0;

        bool result = EnumDisplaySettings(NULL, currentModeID, &lpDevMode);

        if (result)
        {
            // Other information about display can be loaded here
            m_targetFps = lpDevMode.dmDisplayFrequency;
        }
        else
        {
            LOG("Cannot read display frequency");
            ASSERT(EnumDisplaySettings(NULL, currentModeID, &lpDevMode));
        }

        if (QueryPerformanceFrequency(&m_cpuFrequency))
        {
            LOG("CPU frequency Loaded");
        }
        else
        {
            LOG("Cannot read CPU frequency");
            ASSERT(QueryPerformanceFrequency(&m_cpuFrequency));
        }

        // initial hz
        QueryPerformanceCounter(&m_currentCounter);
    }

    void update()
    {
        auto lastCounter = m_currentCounter;
        QueryPerformanceCounter(&m_currentCounter);

        auto getElapsedTime = [&](LARGE_INTEGER start, LARGE_INTEGER end)
        {
            float result =
                (float)(end.QuadPart - start.QuadPart) / (float)m_cpuFrequency.QuadPart;
            return result;
        };

        // get interval as us
        m_dt = getElapsedTime(lastCounter, m_currentCounter);
        if (!m_vSync)
        {
            return;
        }

        float targetDT = 1.f / m_targetFps;

        if (m_dt < targetDT)
        {
            DWORD sleepMS = (DWORD)(1000.f * (targetDT - m_dt));
            if (sleepMS > 0)
            {
                Sleep(sleepMS);
            }

            QueryPerformanceCounter(&m_currentCounter);
            m_dt = getElapsedTime(lastCounter, m_currentCounter);
            if (m_dt < targetDT)
            {
                //로그를 남기려 했으나 항상 로그가 뜬다ㅎ Sleep(ms)보다 높은 정밀도의 대기 기능이 필요할듯.
                //그래서 일단은 주석처리함.
                //LOG("%f(ms) sleep missed", 1000.f * (targetDT - m_dt));
            }
            while (m_dt < targetDT)
            {
                QueryPerformanceCounter(&m_currentCounter);
                m_dt = getElapsedTime(lastCounter, m_currentCounter);
            }
        }
        //TODO(illkwon): 프레임 렉이 발생하더라도 일단은 주석처리
        /*else
        {
            m_targetFps /= 2;
        }*/
    }

    float getMSPerFrame() const
    {
        return m_dt / 1000.f;
    }

    bool m_vSync = false;
    uint m_targetFps;
    TIMECAPS m_timecaps;
    float m_dt;
    float m_msPerFrame;

    LARGE_INTEGER m_currentCounter;
    LARGE_INTEGER m_cpuFrequency;
};