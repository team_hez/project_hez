#pragma once
#include <bounce\bounce.h>
#include <algorithm>

#include "camera.h"
#include "renderergl.h"
#include "editor.h"
#include "collisionsystem.h"

class Tilemap;
class Frame;

enum class DebugCameraMoveState
{
    Idle = 0,
    FreeLook,
    FollowPlayer,
    Last,
};

class CameraRaycastListener : public b3RayCastListener
{
public:
    Vec3 m_target;
    Vec3 m_clippedCamPos;

    // Inherited via b3RayCastListener
    float32 ReportShape(
        b3Shape* shape,
        const b3Vec3& point,
        const b3Vec3& normal,
        float32 fraction) override
    {
        auto shapeData = shape->GetUserData();
        if (!shapeData ||
            (shapeData && *((CollisionType*)shapeData) == CollisionType::Geometry))
        {
            Vec3 newClippedCamPos = {
                point.x,
                point.y,
                point.z
            };

            if (length(m_target - newClippedCamPos) < length(m_target - m_clippedCamPos))
            {
                m_clippedCamPos = newClippedCamPos;
            }
        }

        return 1.f;
    }
};

class Player;
class GameState
{
public:
    void init(GameMemory& gameMemory, MemoryArena& tempArena);
    void update(HWND window, GameInput& input, Frame& frame, MemoryArena& tempArena);
    void updatePlayerMovement(float dt, const GameInput& input);
    void setPlayerVelocityXZPlane(const Vec3& direction, float speed);
    void updatePlayerStatus(float dt);
    void shutdown();
    
public:
    bool m_isPause;
    MemoryArena m_gameArena;
    MemoryArena* m_tilemapArena;
    Tilemap* m_tilemap;
    Camera m_camera;
    OpenglShader m_shader;
    OpenglShader m_uiShader;
    OpenglRenderable m_tilemapRenderable;
    OpenglMesh m_tilemapMesh;
    OpenglMesh m_playerMesh;
    Light m_light;

    // playerPos, playerRot은 수정해도 영향을 안줌.
    Player* m_player;
    b3BoxHull m_tileHull;
    b3World m_world;
    CollisionSystem m_collisionSystem;
    
    OpenglRenderable m_uiRenderable;
    OpenglMesh m_uiMesh;

    int m_goldCount;
    int m_potionCount;
    bool m_hasAmulet;
    
    //
    // Debug stuff
    //

    DebugCameraMoveState m_cameraMoveState;
    EditorGui m_editor;
    uint m_debugDrawVbo;
    uint m_debugDrawVao;
    OpenglShader m_debugDrawShader;
    static const int DEBUG_DRAW_VERTEX_BUFFER_SIZE = 500 * sizeof(DebugDrawVertex);
    
    

};

void debugCameraStateEnter(DebugCameraMoveState cameraMoveState, HWND window, const GameInput& input);

enum class EntityType
{
    Undefined = 5,
    Player,
    TestDoor
};

class Entity
{
public:
    void ApplyDamageInfo(const DamageInfo& damageInfo);

public:
    CollisionType m_colType;
    EntityType m_entityType;
    OpenglRenderable m_entityRenderable;
    Vec3 m_entityPos;
    Vec3 m_entityRot;
    float m_health;
    float m_stamina;

    bool m_isInfiniteHealth;
    bool m_isInfiniteStamina;
};

class Player : public Entity
{
public:
    Vec3 getPlayerPos() const
    {
        auto pos = m_body->GetPosition();
        return Vec3(pos.x, pos.y, pos.z);
    }

public:
    const char* m_name;
    b3Body* m_body;
    b3BoxHull m_hull;

    float m_speed;
    float m_roll;
    bool m_isRolling;
    Vec3 m_rollingDirection;

    float m_maxHealth;
    float m_maxStamina;
    float m_staminaRecoverySpeed;
};

class TestDoor : public Entity
{
    bool m_open;
};

enum class ItemType
{
    Gold = 55,
    Potion,
    Amulet
};

class Item
{
public:
    CollisionType m_colType;
    ItemType m_itemType;
    OpenglRenderable m_itemRenderable;
    Vec3 m_itemPos;
    Vec3 m_itemRot;
};

constexpr int MAX_ENTITY_SIZE = (int)(std::max({
    sizeof(Entity),
    sizeof(Player),
    sizeof(TestDoor),
}));