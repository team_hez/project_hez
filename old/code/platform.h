#pragma once
#ifndef WIN32_LEAN_AND_MEAN 
#define WIN32_LEAN_AND_MEAN 1
#endif
#include <Windows.h>
#include <Xinput.h>

void platformSetShowCursor(bool show);