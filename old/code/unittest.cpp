#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>
#include "general.cpp"
#include "memory.cpp"

TEST_CASE("MemoryRegion test")
{
    initGlobalMemoryRegion(malloc(1000), 1000);
    auto region = debugGetGlobalMemoryRegion();
    void* mem[] =
    {
        globalRegionMalloc(100),
        globalRegionMalloc(100),
        globalRegionMalloc(100),
        globalRegionMalloc(100),
    };

    globalRegionFree(mem[2]);
    globalRegionFree(mem[3]);
    globalRegionFree(mem[0]);
    globalRegionFree(mem[1]);

    CHECK(region->m_size == 1000);
}

TEST_CASE("Memory Arena Test")
{
    MemoryArena arena;
    arena.init(malloc(1000), 1000);

    arena.beginTemp();
    auto* a = arena.push<int>(100);
    arena.endTemp();
}