#pragma once

#include <windows.h>
#include <mmsystem.h>
#include <dsound.h>
#include <stdio.h>
#include "general.h"

class MemoryArena;
class Sound
{
public:
    class WaveFile
    {
    public:
        i8 m_chunkID[4];
        u32 m_chunkSize;
        i8 m_format[4];
        i8 m_subChunkID[4];
        u32 m_subChunkSize;
        u16 m_audioFormat;
        u16 m_numChannels;
        u32 m_sampleRate;
        u32 m_bytesPerSecond;
        u16 m_blockAlign;
        u16 m_bitsPerSample;
        i8 m_dataChunkID[4];
        u32 m_dataSize;
    };

public:
    Sound();
    ~Sound();

    void initDirectSound(HWND hwnd);
    void shutdownDirectSound();

    void loadWaveFile(StringView path, IDirectSoundBuffer8** secondaryBuffer, MemoryArena& arena);
    void unloadWaveFile(IDirectSoundBuffer8* secondaryBuffer);

    void playWaveFile(IDirectSoundBuffer8* secondaryBuffer, bool isLooping = false, DWORD pos = 0);
    void stopWaveFile(IDirectSoundBuffer8* secondaryBuffer);
    DWORD pauseWaveFile(IDirectSoundBuffer8* secondaryBuffer);
    void resumeWaveFile(IDirectSoundBuffer8* secondaryBuffer, bool isLooping = false);
    
    void setVolumeDirectly(IDirectSoundBuffer8* secondaryBuffer, i32 volume = 0);
    i32 calculateVolume(f32 percentage);
    

public:
    IDirectSoundBuffer8 * m_secondaryBuffer;
    const i32 VOLUME_MAX = 0;
    const i32 VOLUME_MIN = -10000;
    
private:
    IDirectSound8 * m_directSound;
    IDirectSoundBuffer* m_primaryBuffer;
};

