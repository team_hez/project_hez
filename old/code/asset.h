#pragma once
#include "general.h"
#include "hmath.h"

class ObjFaceIndex
{
public:
    int m_vertexIndex;
    int m_texCoordIndex;
    int m_normalIndex;
};

class ObjFace
{
public:
    ObjFaceIndex m_indices[3];
};

class ObjAsset
{
public:
    void* mem;
    VectorView<Vec3> m_vertices;
    VectorView<Vec3> m_normals;
    VectorView<Vec3> m_texCoords;
    VectorView<ObjFace> m_faces;
};

// TODO(illkwon): 다시 생각해보자...
//void loadObj(const char* filename, ObjAsset* outObjAsset, MemoryArena& arena);
//void freeObj(ObjAsset& objAsset);

// Vox
//void loadVox(const char* filename);