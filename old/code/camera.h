#pragma once

#include <memory>
#include "input.h"
#include "general.h"
#include "hmath.h"

class CameraTransfrom
{
public:
    // roll은 아직 사용처가 보이지 않아 적용하지 않는다.
    float m_yaw;
    float m_pitch;
    Vec3 m_look;//실제로 view매트릭스의 z축과는 반대방향이다.
    static inline const Vec3 UP = Vec3(0, 1, 0);

    CameraTransfrom(float yaw, float pitch)
        : m_yaw(yaw), m_pitch(pitch)
    {
        applyYawPitchToLookVector();
    }

    void look(Vec3 eye, Vec3 target)
    {
        m_look = normalize(target - eye);

        m_pitch = asin(m_look.y);
        float cosPitch = cos(m_pitch);
        m_yaw = asin(cosPitch == 0.f ? 0.f : m_look.x / cosPitch);
    }

    void setYawPitchLook(float yaw, float pitch)
    {
        m_yaw = yaw;
        m_pitch = pitch;
        applyYawPitchToLookVector();
    }

    void applyYawPitchToLookVector()
    {
        m_look =
        {
            cos(m_pitch) * sin(m_yaw),
            sin(m_pitch),
            cos(m_pitch) * cos(m_yaw)
        };
    }

    Vec3 right() const
    {
        auto result = normalize(cross(m_look, UP));
        return result;
    }

    Vec3 up() const
    {
        return UP;
    }
};

class Camera
{
public:
    // Default Screen Ratio 4:3
    Camera(float x, float y, float z, float horizontalAngle, float verticalAngle, float fieldOfView, float range, float ratio = 4.0f / 3.0f, float mouseSpeed = 0.005f, float keyboardSpeed = 16.f);

    void moveFreelook(const GameInput& input, int windowCenterX, int windowCenterY);
    void moveFollow(const GameInput& input, Vec3 target, float yaw, float pitch, float distance);
    void updateFrustum();

    bool pointInView(const Vec3& point) const;
    bool pointInView(float x, float y, float z) const;

    Mat4 getViewMatrix() const;
    Mat4 getProjectionMatrix(const float ratio) const;

	Vec3 m_position = Vec3(0.0f, 0.0f, 5.0f);

    CameraTransfrom m_transform;
	/*float m_pitch = 0.0f;
	float m_yaw = 90.0f;*/
	float m_fieldOfView = 45.0f;
    float m_depthFar = 800.0f;
	float m_depthNear;
	float m_ratio;
	
	float m_frustumWidth;
    float m_frustumHeight;
	Vec3 m_frustumX;
	Vec3 m_frustumY;
	Vec3 m_frustumZ;
	float m_tang;

    float m_mouseSpeed;
    float m_keyboardSpeed;
};