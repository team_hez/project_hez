#include <bounce\bounce.h>
#include "Tilemap.h"


Tile::Tile(int x, int z, int layer, int type, Tilemap* tilemap)
    :m_x(x), m_z(z), m_layer(layer), m_type(type), m_tilemap(tilemap)
{
    //m_mesh.clear();
}

Tile::~Tile()
{
   
}

float Tile::getXPosition()
{
    return (float)m_x * m_tilemap->m_tileSize;
}

float Tile::getYPosition()
{
    return (float)m_layer * m_tilemap->m_tileSize;
}

float Tile::getZPosition()
{
    return (float)m_z * m_tilemap->m_tileSize;
}

Vec3 Tile::getPosition()
{
    return Vec3((float)m_x * m_tilemap->m_tileSize, (float)m_layer * m_tilemap->m_tileSize, (float)m_z * m_tilemap->m_tileSize);
}


void TileLayer::init(int xCount, int zCount, int currentLayer, Tilemap* tilemap, MemoryArena& arena)
{
    m_xCount = xCount;
    m_zCount = zCount;
    m_currentLayer = currentLayer;
    m_tilemap = tilemap;
    arena.push(&m_tiles, MAX_TILE_COUNT);

    for (int z = 0; z < zCount; ++z)
    {
        for (int x = 0; x < xCount; ++x)
        {
            new (&m_tiles[z * xCount + x])Tile(x, z, currentLayer, 1, m_tilemap);
            m_tiles[z * xCount + x].m_tileModel = createTransform(
                m_tiles[z * xCount + x].getXPosition(),
                m_tiles[z * xCount + x].getYPosition(),
                m_tiles[z * xCount + x].getZPosition(),
                m_tilemap->m_tileSize,
                m_tilemap->m_tileSize,
                m_tilemap->m_tileSize);
        }
    }
}

void TileLayer::createTile(int x, int z, int layer, int type)
{
    new (&m_tiles[z * m_xCount + x])Tile(x, z, layer, type, m_tilemap);
    m_tiles[z * m_xCount + x].m_tileModel = createTransform(
        m_tiles[z * m_xCount + x].getXPosition(),
        m_tiles[z * m_xCount + x].getYPosition(),
        m_tiles[z * m_xCount + x].getZPosition(),
        m_tilemap->m_tileSize,
        m_tilemap->m_tileSize,
        m_tilemap->m_tileSize);
}

void TileLayer::buildLayer(int prevXCount, int prevZCount, MemoryArena& arena)
{
    Tile* tempTiles = nullptr;
    arena.beginTemp();
    arena.push(&tempTiles, prevXCount * prevZCount);
    
    memcpy(tempTiles, m_tiles, sizeof(Tile) * prevXCount * prevZCount);
    for (int z = 0; z < m_zCount; ++z)
    {
        for (int x = 0; x < m_xCount; ++x)
        {
            if (tempTiles[z * prevXCount + x].m_z == z && tempTiles[z * prevXCount + x].m_x == x)
            {
                new (&m_tiles[z * m_xCount + x])Tile(x, z, m_currentLayer, tempTiles[z * prevXCount + x].m_type, m_tilemap);
                
            }
            else
            {
                new (&m_tiles[z * m_xCount + x])Tile(x, z, m_currentLayer, 1, m_tilemap);
            }

            m_tiles[z * m_xCount + x].m_tileModel = createTransform(
                m_tiles[z * m_xCount + x].getXPosition(),
                m_tiles[z * m_xCount + x].getYPosition(),
                m_tiles[z * m_xCount + x].getZPosition(),
                m_tilemap->m_tileSize,
                m_tilemap->m_tileSize,
                m_tilemap->m_tileSize
            );
        }
    }

    arena.endTemp();
}

void TileLayer::updateActualTileCount()
{
    int tileCount = m_xCount * m_zCount;
    m_actualTileCount = 0;

    for (int i = 0; i < tileCount; ++i)
    {
        if (m_tiles[i].m_isDrawable == true)
        {
            m_actualTileCount++;
        }
    }
}

Vec3 TileLayer::getCenterGroundPos() const
{
    float tilemapSizeX = (float)m_xCount * m_tilemap->m_tileSize;
    float tilemapSizeZ = (float)m_zCount * m_tilemap->m_tileSize;

    auto result = Vec3
    (
        (tilemapSizeX - m_tilemap->m_tileSize) * 0.5f,
        ((float)m_currentLayer + 0.5f) * m_tilemap->m_tileSize,
        (tilemapSizeZ - m_tilemap->m_tileSize) * 0.5f
    );

    return result;
}

Tile& TileLayer::getTile(int x, int z)
{
    ASSERT(x >= 0 && x < m_xCount && z >= 0 && z < m_zCount);
    return m_tiles[z * m_xCount + x];
}


Tilemap::Tilemap(int xCount, int zCount, int layerCount, MemoryArena& arena)
{
    m_layerCount = layerCount;
    arena.push(&m_layers, MAX_LAYER_COUNT);

    for (int i = 0; i < MAX_LAYER_COUNT; ++i)
    {
        m_layers[i].init(xCount, zCount, i, this, arena);
        m_isSelected[i] = false;
    }

    updateAllDrawable();
}

void Tilemap::draw(OpenglRenderable& tileRenderable, OpenglShader shader, const Mat4& viewMatrix, const Mat4& projMatrix, const Light& light)
{
    for (int i = 0; i < m_layerCount; ++i)
    {
        int xCount = m_layers[i].m_xCount;
        int zCount = m_layers[i].m_zCount;

        for (int z = 0; z < zCount; ++z)
        {
            for (int x = 0; x < xCount; ++x)
            {
                if (m_layers[i].m_tiles[z * xCount + x].m_isDrawable == true)
                {
                    tileRenderable.draw(shader, m_layers[i].m_tiles[z * xCount + x].m_tileModel, viewMatrix, projMatrix, light);
                }
                
            }
        }
    }

}

int Tilemap::getTileCount()
{
    int result = 0;
    
    for (int i = 0; i < m_layerCount; ++i)
    {
        result += m_layers[i].m_xCount * m_layers[i].m_zCount;
    }

    return result;
}

void Tilemap::updateAllDrawable()
{
    for (int i = 0; i < m_layerCount; ++i)
    {
        int xCount = m_layers[i].m_xCount;
        int zCount = m_layers[i].m_zCount;
        //must draw side - bottom side and top side of tiles
        if (i == 0 || i == (m_layerCount - 1))
        {
            for (int z = 0; z < zCount; ++z)
            {
                for (int x = 0; x < xCount; ++x)
                {
                    if (m_layers[i].m_tiles[z * xCount + x].m_type == 0)
                    {
                        m_layers[i].m_tiles[z * xCount + x].m_isDrawable = false;
                    }
                    else
                    {
                        m_layers[i].m_tiles[z * xCount + x].m_isDrawable = true;
                    }
                }
            }
        }
        else
        {
            for (int z = 0; z < zCount; ++z)
            {
                for (int x = 0; x < xCount; ++x)
                {
                    //also must draw case - every side of tiles
                    if (z == 0 || z == (zCount - 1) || x == 0 || x == (xCount - 1))
                    {
                        if (m_layers[i].m_tiles[z * xCount + x].m_type == 0)
                        {
                            m_layers[i].m_tiles[z * xCount + x].m_isDrawable = false;
                        }
                        else
                        {
                            m_layers[i].m_tiles[z * xCount + x].m_isDrawable = true;
                        }
                    }
                    else
                    {
                        //check is drawable with adjacent tiles!
                        if (m_layers[i].m_tiles[z * xCount + x].m_type == 0)
                        {
                            m_layers[i].m_tiles[z * xCount + x].m_isDrawable = false;
                            continue;
                        }

                        //already checked tiles.
                        if (m_layers[i - 1].m_tiles[z * xCount + x].m_isDrawable == false)
                        {
                            m_layers[i].m_tiles[z * xCount + x].m_isDrawable = true;
                        }
                        
                        if (m_layers[i].m_tiles[(z - 1) * xCount + x].m_isDrawable == false)
                        {
                            m_layers[i].m_tiles[z * xCount + x].m_isDrawable = true;
                        }

                        if (m_layers[i].m_tiles[z * xCount + (x - 1)].m_isDrawable == false)
                        {
                            m_layers[i].m_tiles[z * xCount + x].m_isDrawable = true;
                        }

                        //may not checked tiles.
                        if (m_layers[i - 1].m_tiles[z * xCount + x].m_type != 0 && 
                            m_layers[i + 1].m_tiles[z * xCount + x].m_type != 0 &&
                            m_layers[i].m_tiles[z * xCount + (x - 1)].m_type != 0 &&
                            m_layers[i].m_tiles[z * xCount + (x + 1)].m_type != 0 &&
                            m_layers[i].m_tiles[(z - 1) * xCount + x].m_type != 0 &&
                            m_layers[i].m_tiles[(z + 1) * xCount + x].m_type != 0 )
                        {
                            m_layers[i].m_tiles[z * xCount + x].m_isDrawable = false;
                        }
                        else
                        {
                            m_layers[i].m_tiles[z * xCount + x].m_isDrawable = true;
                        }
                    }
                }
            }
        }
    }
}

void Tilemap::updateAdjDrawable(int layer, int x, int z, int type)
{
    int xCount = m_layers[layer].m_xCount;
    int zCount = m_layers[layer].m_zCount;

    m_layers[layer].m_tiles[z * xCount + x].m_type = type;

    bool needToUpdate = false;

    if (type == 0)
    {
        m_layers[layer].m_tiles[z * xCount + x].m_isDrawable = false;

        if (layer < (m_layerCount - 1))
        {
            if (m_layers[layer + 1].m_tiles[z * xCount + x].m_type == 0)
            {
                m_layers[layer + 1].m_tiles[z * xCount + x].m_isDrawable = false;
            }
            else
            {
                m_layers[layer + 1].m_tiles[z * xCount + x].m_isDrawable = true;
            }
        }

        if (layer > 0)
        {
            if (m_layers[layer - 1].m_tiles[z * xCount + x].m_type == 0)
            {
                m_layers[layer - 1].m_tiles[z * xCount + x].m_isDrawable = false;
            }
            else
            {
                m_layers[layer - 1].m_tiles[z * xCount + x].m_isDrawable = true;
            }
        }

        if (x < (xCount - 1))
        {
            if (m_layers[layer].m_tiles[z * xCount + (x + 1)].m_type == 0)
            {
                m_layers[layer].m_tiles[z * xCount + (x + 1)].m_isDrawable = false;
            }
            else
            {
                m_layers[layer].m_tiles[z * xCount + (x + 1)].m_isDrawable = true;
            }
        }

        if (x > 0)
        {
            if (m_layers[layer].m_tiles[z * xCount + (x - 1)].m_type == 0)
            {
                m_layers[layer].m_tiles[z * xCount + (x - 1)].m_isDrawable = false;
            }
            else
            {
                m_layers[layer].m_tiles[z * xCount + (x - 1)].m_isDrawable = true;
            }
        }

        if (z < (zCount - 1))
        {
            if (m_layers[layer].m_tiles[(z + 1) * xCount + x].m_type == 0)
            {
                m_layers[layer].m_tiles[(z + 1) * xCount + x].m_isDrawable = false;
            }
            else
            {
                m_layers[layer].m_tiles[(z + 1) * xCount + x].m_isDrawable = true;
            }
        }
        if (z > 0)
        {
            if (m_layers[layer].m_tiles[(z - 1) * xCount + x].m_type == 0)
            {
                m_layers[layer].m_tiles[(z - 1) * xCount + x].m_isDrawable = false;
            }
            else
            {
                m_layers[layer].m_tiles[(z - 1) * xCount + x].m_isDrawable = true;
            }
        }
    }
    else
    {
        m_layers[layer].m_tiles[z * xCount + x].m_isDrawable = true;
        if (layer < (m_layerCount - 1))
        {
            if (m_layers[layer + 1].m_tiles[z * xCount + x].m_type == 0)
            {
                m_layers[layer + 1].m_tiles[z * xCount + x].m_isDrawable = false;
            }
            else
            {
                needToUpdate = true;
            }
        }

        if (layer > 0)
        {
            if (m_layers[layer - 1].m_tiles[z * xCount + x].m_type == 0)
            {
                m_layers[layer - 1].m_tiles[z * xCount + x].m_isDrawable = false;
            }
            else
            {
                needToUpdate = true;
            }
        }

        if (x < (xCount - 1))
        {
            if (m_layers[layer].m_tiles[z * xCount + (x + 1)].m_type == 0)
            {
                m_layers[layer].m_tiles[z * xCount + (x + 1)].m_isDrawable = false;
            }
            else
            {
                needToUpdate = true;
            }
        }

        if (x > 0)
        {
            if (m_layers[layer].m_tiles[z * xCount + (x - 1)].m_type == 0)
            {
                m_layers[layer].m_tiles[z * xCount + (x - 1)].m_isDrawable = false;
            }
            else
            {
                needToUpdate = true;
            }
        }

        if (z < (zCount - 1))
        {
            if (m_layers[layer].m_tiles[(z + 1) * xCount + x].m_type == 0)
            {
                m_layers[layer].m_tiles[(z + 1) * xCount + x].m_isDrawable = false;
            }
            else
            {
                needToUpdate = true;
            }
        }

        if (z > 0)
        {
            if (m_layers[layer].m_tiles[(z - 1) * xCount + x].m_type == 0)
            {
                m_layers[layer].m_tiles[(z - 1) * xCount + x].m_isDrawable = false;
            }
            else
            {
                needToUpdate = true;
            }
        }

        if (needToUpdate == true)
        {
            updateAllDrawable();
        }
    }
}

int Tilemap::biggestZCount()
{
    int result = 0;
    for (int i = 0; i < m_layerCount; ++i)
    {
        if (result < m_layers[i].m_zCount)
        {
            result = m_layers[i].m_zCount;
        }
    }

    return result;
}

int Tilemap::biggestXCount()
{
    int result = 0;
    for (int i = 0; i < m_layerCount; ++i)
    {
        if (result < m_layers[i].m_xCount)
        {
            result = m_layers[i].m_xCount;
        }
    }

    return result;
}

bool Tilemap::loadTilemap(const char* filename, MemoryArena& arena)
{
    arena.beginTemp();
    auto file = readEntireFile(filename, "rb", arena);
    
    auto buffer = file.m_data;
    auto layerBuffer = (int*)buffer;
    m_layerCount = *layerBuffer;

    buffer = (void*)((u8*)buffer + sizeof(int));

    for (int layer = 0; layer < m_layerCount; ++layer)
    {
        auto xCountBuffer = (int*)(buffer);
        m_layers[layer].m_xCount = *xCountBuffer;

        auto zCountBuffer = (int*)((u8*)xCountBuffer + sizeof(int));
        m_layers[layer].m_zCount = *zCountBuffer;
        
        int xCount = m_layers[layer].m_xCount;
        int zCount = m_layers[layer].m_zCount;

        auto tileBuffer = (int*)((u8*)zCountBuffer + sizeof(int));
        memcpy(m_layers[layer].m_tiles, tileBuffer, sizeof(Tile) * xCount * zCount);
        buffer = (void*)((u8*)tileBuffer + (sizeof(Tile) * xCount * zCount));
    }

    arena.endTemp();
    return true;
}

bool Tilemap::saveTilemap(const char* filename, MemoryArena& arena)
{
    FILE* file = fopen(filename, "wb");
    SCOPE_EXIT(fclose(file));

    if (file == nullptr)
    {
        return false;
    }

    arena.beginTemp();

    auto buffer = (int*)arena.push(sizeof(int));
    *buffer = m_layerCount;
    size_t bufferSize = sizeof(int);

    for (int layer = 0; layer < m_layerCount; ++layer)
    {
        auto xCountBuffer = (int*)arena.push(sizeof(int));
        *xCountBuffer = m_layers[layer].m_xCount;
        bufferSize += sizeof(int);

        auto zCountBuffer = (int*)arena.push(sizeof(int));
        *zCountBuffer = m_layers[layer].m_zCount;
        bufferSize += sizeof(int);

        int xCount = m_layers[layer].m_xCount;
        int zCount = m_layers[layer].m_zCount;

        auto tileBuffer = arena.push(sizeof(Tile) * xCount * zCount);
        memcpy(tileBuffer, m_layers[layer].m_tiles, sizeof(Tile) * xCount * zCount);
        bufferSize += sizeof(Tile) * xCount * zCount;
    }

    fwrite((void*)buffer, bufferSize, 1, file);
    arena.endTemp();

    return true;
}

void Tilemap::buildTilemapBodyChunk(b3World& world, MemoryArena& arena)
{
    destroyTilemapBodyChunk(world, arena);

    m_chunkStart = arena.m_used;
    m_chunks = (TilemapChunk*)((u8*)arena.m_memory + m_chunkStart);

    for (int layerIndex = 0; layerIndex < m_layerCount; ++layerIndex)
    {
        int xCount = m_layers[layerIndex].m_xCount;
        int zCount = m_layers[layerIndex].m_zCount;

        for (int z = 0; z < zCount; ++z)
        {
            for (int x = 0; x < xCount; ++x)
            {
                auto currTile = m_layers[layerIndex].m_tiles[z * xCount + x];
                if (currTile.m_type != 0 && currTile.m_hasBody == false)
                {
                    int tileXStart = currTile.m_x;
                    int tileXEnd = currTile.m_x;
                    
                    int tileZStart = currTile.m_z;
                    int tileZEnd = currTile.m_z;
                    
                    //find first x start to x end.
                    for (int nearX = tileXStart; nearX < xCount; ++nearX)
                    {
                        auto nearTile = m_layers[layerIndex].m_tiles[tileZStart * xCount + nearX];
                        if (nearTile.m_type != 0 && nearTile.m_hasBody == false)
                        {
                            tileXEnd = nearTile.m_x;
                        }
                        else
                        {
                            break;
                        }
                    }
                    
                    //check z start to z end.

                    bool breakZ = false;
                    for (int nearZ = tileZStart; nearZ < zCount; ++nearZ)
                    {
                        //test xStart to xEnd. if fail one of end, it just fail.
                        for (int nearX = tileXStart; nearX <= tileXEnd; ++nearX)
                        {
                            auto nearTile = m_layers[layerIndex].m_tiles[nearZ * xCount + nearX];
                            if (nearTile.m_type != 0 && nearTile.m_hasBody == false)
                            {
                                //ok
                                
                            }
                            else
                            {
                                //fail.
                                breakZ = true;
                                break;
                            }
                        }

                        if (breakZ == true)
                        {
                            break;
                        }

                        tileZEnd = nearZ;
                    }
                    
                    tileZEnd = m_layers[layerIndex].m_tiles[tileZEnd * xCount + tileXStart].m_z;

                    for (int foundZ = tileZStart; foundZ <= tileZEnd; ++foundZ)
                    {
                        for (int foundX = tileXStart; foundX <= tileXEnd; ++foundX)
                        {
                            m_layers[layerIndex].m_tiles[foundZ * xCount + foundX].m_hasBody = true;
                        }
                    }

                    //now tileXStart to tileXEnd is tile chunk x size. tileZStart to tileZEnd is tile chunk z size.
                    //this chunk is always box hull. so find mid point and build it.

                    int midXSize = tileXEnd - tileXStart;
                    int midZSize = tileZEnd - tileZStart;

                    int midXIndex = tileXStart + (midXSize / 2);
                    int midZIndex = tileZStart + (midZSize / 2);

                    float xPos = 0.0f;
                    float yPos = m_layers[layerIndex].m_tiles[midZIndex * xCount + midXIndex].getYPosition();
                    float zPos = 0.0f;

                    float xScl = (float)(midXSize + 1) * m_tileSize * 0.5f;
                    float zScl = (float)(midZSize + 1) * m_tileSize * 0.5f;

                    if (midXSize % 2 == 1)
                    {
                        xPos = m_layers[layerIndex].m_tiles[midZIndex * xCount + midXIndex].getXPosition() + (m_tileSize * 0.5f);
                    }
                    else
                    {
                        xPos = m_layers[layerIndex].m_tiles[midZIndex * xCount + midXIndex].getXPosition(); // +(m_tileSize * 0.5f);
                    }

                    if (midZSize % 2 == 1)
                    {
                        zPos = m_layers[layerIndex].m_tiles[midZIndex * xCount + midXIndex].getZPosition() + (m_tileSize * 0.5f);;
                    }
                    else
                    {
                        zPos = m_layers[layerIndex].m_tiles[midZIndex * xCount + midXIndex].getZPosition(); // +(m_tileSize * 0.5f);
                    }

                    
                    b3BodyDef tileBodyDef;
                    tileBodyDef.position.Set(xPos, yPos, zPos);
                    tileBodyDef.type = e_staticBody;

                    auto chunk = arena.push<TilemapChunk>();
                    chunk->type = CollisionType::Geometry;
                    chunk->body = world.CreateBody(tileBodyDef);

                    new (&chunk->hull) b3BoxHull();
                    chunk->hull.Set(xScl, m_tileSize * 0.5f, zScl);
                    
                    b3HullShape hull;
                    hull.m_hull = &chunk->hull;

                    b3ShapeDef tileShapeDef;
                    tileShapeDef.shape = &hull;
                    tileShapeDef.density = 1.0f;
                    tileShapeDef.friction = 0.0f;
                    tileShapeDef.restitution = 0.0f;

                    auto shape = chunk->body->CreateShape(tileShapeDef);
                    shape->SetUserData(chunk);
                    m_chunks[m_chunkCount] = *chunk;
                    m_chunkCount++;
                }
            }
        }
    }

}

void Tilemap::destroyTilemapBodyChunk(b3World& world, MemoryArena& arena)
{
    if (m_chunkCount > 0)
    {
        for (int i = 0; i < m_chunkCount; ++i)
        {
            world.DestroyBody(m_chunks[i].body);
        }

        memset(m_chunks, 0, sizeof(TilemapChunk) * m_chunkCount);
        m_chunkCount = 0;

        if (m_chunkStart != 0)
        {
            arena.m_used = m_chunkStart;
        }
        else
        {
            ASSERT(0);
        }

        for (int layerIndex = 0; layerIndex < m_layerCount; ++layerIndex)
        {
            int tileCount = m_layers[layerIndex].m_xCount * m_layers[layerIndex].m_zCount;
            for (int tileIndex = 0; tileIndex < tileCount; ++tileIndex)
            {
                m_layers[layerIndex].m_tiles[tileIndex].m_hasBody = false;
            }
        }

    }
}

void Tilemap::buildTilemapMesh(OpenglMesh& mesh, MemoryArena& arena)
{
    mesh.m_vertexCount = 36;
    mesh.m_indexCount = mesh.m_vertexCount;
    mesh.m_vertices = arena.push<OpenglVertex>(mesh.m_vertexCount);
    mesh.m_indices = arena.push<uint>(mesh.m_indexCount);

    int i = 0;
    auto pushVertex = [&](const OpenglVertex& vertex)
    {
        mesh.m_vertices[i] = vertex;
        mesh.m_indices[i] = i;
        ++i;
    };

    Vec3 leftTopFront       = { -0.5f, +0.5f, +0.5f };
    Vec3 leftTopBack        = { -0.5f, +0.5f, -0.5f };
    Vec3 leftBottomFront    = { -0.5f, -0.5f, +0.5f };
    Vec3 leftBottomBack     = { -0.5f, -0.5f, -0.5f };
    Vec3 rightTopFront      = { +0.5f, +0.5f, +0.5f };
    Vec3 rightTopBack       = { +0.5f, +0.5f, -0.5f };
    Vec3 rightBottomFront   = { +0.5f, -0.5f, +0.5f };
    Vec3 rightBottomBack    = { +0.5f, -0.5f, -0.5f };

    for (int layerIndex = 0; layerIndex < m_layerCount; ++layerIndex)
    {
        int xCount = m_layers[layerIndex].m_xCount;
        int zCount = m_layers[layerIndex].m_zCount;

        for (int z = 0; z < zCount; ++z)
        {
            for (int x = 0; x < xCount; ++x)
            {
                if (m_layers[layerIndex].m_tiles[z * xCount + x].m_type != 0)
                {
                    //push triangle
                    Vec3 origin = m_layers[layerIndex].m_tiles[z * xCount + x].getPosition();

                    //check previous vertices

                    /*pushVertex(OpenglVertex(Vec3(), Vec4(), Vec3()));
                    pushVertex(OpenglVertex());
                    pushVertex(OpenglVertex());*/
                }
            }
        }
    }

}

