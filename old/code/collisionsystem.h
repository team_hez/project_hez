#pragma once
#include <bounce\bounce.h>
#include "hmath.h"
#include "general.h"

class FreeHitBoxHeader
{
public:
    FreeHitBoxHeader* m_next;
};

enum class CollisionType
{
    Geometry = 1,
    Entity,
    HitBox,
    Item
};

class DamageInfo
{
public:
    DamageInfo(float hp = 0.0f, float sp = 0.0f);

public:
    float m_deltaHP;
    float m_deltaSP;
};

class HitBox
{
public:
    CollisionType m_colType;
    b3Body* m_hitBoxBody = nullptr;
    b3BoxHull m_hitBoxHull;
    DamageInfo m_info;
};

class CollisionSystem : public b3ContactListener
{
public:
    CollisionSystem(b3World& world, MemoryArena& arena);
    CollisionSystem() = delete;
    CollisionSystem(const CollisionSystem&) = delete;
    ~CollisionSystem();
    CollisionSystem& operator=(const CollisionSystem&) = delete;

    // @warning You cannot create/destroy Bounce objects inside these callbacks.
    // A contact has begun.
    void BeginContact(b3Contact* contact) override;

    // A contact has ended.
    void EndContact(b3Contact* contact) override;

    // The contact will be solved after this notification.
    void PreSolve(b3Contact* contact) override;

    HitBox* createHitBox(const Vec3& hitBoxPos, const Vec3& hitBoxScale, const DamageInfo& info);
    void destroyHitBox(HitBox* hitBox);

    bool isDataIsCollisionType(void* data, CollisionType type);
    CollisionType getCollisionType(void* data);

private:
    static const int MAX_HITBOX_COUNT = 100;

    b3World& m_world;

    HitBox* m_hitBoxes;
    FreeHitBoxHeader* m_lastFree;

    HitBox* m_activeHitBoxes[MAX_HITBOX_COUNT];
    int m_activeHitBoxCount;
};


