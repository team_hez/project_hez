#pragma once
#include <array>
#include <vector>
#include <unordered_map>
#include <string>
#include <cstdio>
#include <cstdarg>
#include <cassert>
#include "memory.h"

constexpr float PI = 3.141592653589793238462643385f;

class Logger
{
public:
    char* m_buffer;
    size_t m_size;
    size_t m_used;
    int m_lineCount;
};

const Logger& debugGetGlobalLoggerInstance();
void debugInitGlobalLogger(void* memory, size_t size);
void debugGlobalLoggerPushNewLineLog(const char* format, ...);
void debugGlobalLoggerPushLog(const char* format, ...);
void debugFlushGlobalLoggerBuffer();

#define FIXED_LOG(...) debugGlobalLoggerPushNewLineLog(__VA_ARGS__)
//TODO(illkwon): 인게임 로그로 대체
#define LOG(...) printf(__VA_ARGS__); printf("\n");
#ifdef DEBUG
#define ASSERT(expression) {if(!(expression)) {int* _assertionHappened_ = nullptr; *_assertionHappened_ = 0;}}
#else
#define ASSERT(expression)
#endif

using uint = unsigned int;
using u8 = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;
using i8 = int8_t;
using i16 = int16_t;
using i32 = int32_t;
using i64 = int64_t;
using f32 = float;
using f64 = double;

// Utility function/classes

constexpr u64 kb(u64 n) { return n * 1000LL; }
constexpr u64 mb(u64 n) { return kb(n) * 1000LL; }
constexpr u64 gb(u64 n) { return mb(n) * 1000LL; }
constexpr u64 tb(u64 n) { return gb(n) * 1000LL; }

template <typename T, int N>
constexpr inline int arrayCount(T(&)[N]) { return N; }

template <typename F>
class ScopeExit
{
public:
    F m_f;
    ScopeExit(F f) : m_f(f) {}
    ~ScopeExit() { m_f(); }
};
template <typename F>
inline ScopeExit<F> makeScopeExit(F f) { return ScopeExit<F>(f); }
#define MAKE_SCOPE_EXIT_NAME(prefix, line) MAKE_SCOPE_EXIT_NAME2(prefix, line)
#define MAKE_SCOPE_EXIT_NAME2(prefix, line) prefix ## line
#define SCOPE_EXIT(code) \
    auto MAKE_SCOPE_EXIT_NAME(___scopeExit, __LINE__) = makeScopeExit([&](){code;})

inline int getFileSize(FILE* file)
{
    long oldPos = ftell(file);
    fseek(file, 0, SEEK_END);
    int length = ftell(file);
    fseek(file, oldPos, SEEK_SET);
    return length;
}

class ReadFileData
{
public:
    void* m_data;
    int m_size;
};
// malloc사용하는 함수 제거
//ReadFileData readEntireFile(const char* filename, const char* mode);
ReadFileData readEntireFile(const char* filename, const char* mode, MemoryArena& arena);

// Containers

//template <typename T>
//using Vector = std::vector<T, StlRegionAllocator<T>>;
//template <typename K, typename V>
//using HashMap = std::unordered_map<K, V, std::hash<K>, std::equal_to<K>, StlRegionAllocator<std::pair<const K, V>>>;
//using String = std::basic_string<char, std::char_traits<char>, StlRegionAllocator<char>>;
using StringView = std::string_view;
template<typename T, size_t SIZE>
using Array = std::array<T, SIZE>;

/*
template <typename T>
class StaticVector
{
public:
    int m_size;
    T* m_data;

    using iterator = T*;

    explicit
    StaticVector(int size)
        : m_size(size)
    {
        m_data = NEW T[size];
    }

    ~StaticVector()
    {
        if (m_data)
        {
            delete[] m_data;
        }
    }

    iterator begin()
    {
        return m_data;
    }

    iterator end()
    {
        return m_data + m_size;
    }

    T& operator[](int idx)
    {
        ASSERT((idx >= 0) && (idx < m_size));
        return m_data[idx];
    }
};*/

template <typename T>
class ReservedVector
{
public:
    ReservedVector(MemoryArena& arena, int capacity)
        : m_size(0), m_capacity(capacity)
    {
        m_data = arena.push<T>(capacity);
    }

    ~ReservedVector()
    {
        m_size = 0;
        m_capacity = 0;
    }

    T& operator[](int index)
    {
        ASSERT((index >= 0) && (index < m_size));
        return m_data[index];
    }

    const T& operator[](int index) const
    {
        ASSERT((index >= 0) && (index < m_size));
        return m_data[index];
    }

    T& front()
    {
        return m_data[0];
    }

    const T& front() const
    {
        return m_data[0];
    }

    T& back()
    {
        return m_data[m_size-1];
    }

    const T& back() const
    {
        return m_data[m_size - 1];
    }

    bool empty() const
    {
        return m_size == 0;
    }

    void insert(const T& rhs)
    {
        ASSERT(m_size < m_capacity);
        m_data[m_size++] = rhs;
    }
    
    void remove(const T& rhs)
    {
        for (int i = 0; i < m_size; ++i)
        {
            if (rhs == m_data[i])
            {
                for (int j = i; j < m_size - 1; ++j)
                {
                    m_data[j] = m_data[j + 1];
                }
                
                m_data[m_size - 1] = 0;
                --m_size;
                break;
            }
        }
    }

    void erase(int index)
    {
        ASSERT((index >= 0) && (index < m_size));
        for (int i = 0; i < m_size; ++i)
        {
            if (index == i)
            {
                for (int j = i; j < m_size - 1; ++j)
                {
                    m_data[j] = m_data[j + 1];
                }

                m_data[m_size - 1] = 0;
                --m_size;
                break;
            }
        }
    }

    void clear()
    {
        memset(m_data, 0, sizeof(T) * m_size);
        m_size = 0;
    }

public:
    int m_size = 0;
    int m_capacity = 0;
    T* m_data = nullptr;
};

template <typename T>
class VectorView
{
public:
    int m_size = 0;
    T* m_dataView = nullptr;

    using iterator = T*;

    VectorView() = default;

    explicit
    VectorView(int size, void* data)
        : m_size(size), m_dataView((T*)(data))
    {
    }

    void setView(int size, void* data)
    {
        size = size;
        m_dataView = (T*)(data);
    }

    iterator begin()
    {
        return m_dataView;
    }

    iterator end()
    {
        return m_dataView + m_size;
    }

    T& operator[](int idx)
    {
        ASSERT((idx >= 0) && (idx < m_size));
        return m_dataView[idx];
    }
};

inline u64 djb2Hash(const char* str)
{
    u64 hash = 5381;
    int c;
    while (c = *str++)
    {
        hash = ((hash << 5) + hash) + c;
    }

    return hash;
}

// TODO(illkwon): 잘 동작하는지 충분한 테스트 필요
class HashMapStrInt
{
public:
    using K = StringView;
    using V = int;

    enum class NodeState
    {
        Empty = 0,
        Occupied,
        Deleted,
    };

    class Node
    {
    public:
        K m_key;
        V m_value;
        NodeState m_state;
    };

    int m_size;
    int m_capacity;
    Node* m_nodes;

    void init(void* buffer, int bufferSize)
    {
        m_size = 0;
        m_capacity = bufferSize / sizeof(Node);
        m_nodes = (Node*)buffer;

        for (int i = 0; i < m_capacity; ++i)
        {
            m_nodes[i] = {};
        }
    }

    V find(K key)
    {
        auto found = findNode(key);
        ASSERT(found);
        return found->m_value;
    }

    void insert(K key, V value)
    {
        ASSERT(m_size < m_capacity);
        u64 hash = djb2Hash(key.data());

        int index = hash % m_capacity;
        while (m_nodes[index].m_state == NodeState::Occupied)
        {
            index = (index + 1) % m_capacity;
        }

        m_nodes[index].m_key = key;
        m_nodes[index].m_value = value;
        m_nodes[index].m_state = NodeState::Occupied;

        ++m_size;
    }

    void remove(K key)
    {
        auto found = findNode(key);
        if (found)
        {
            found->m_state = NodeState::Deleted;
            --m_size;
        }
    }

    //
    // Internal functions
    //
private:
    Node* findNode(K key)
    {
        if (m_size == 0)
        {
            return nullptr;
        }

        uint64_t hash = djb2Hash(key.data());
        int index = hash % m_capacity;
        while (m_nodes[index].m_state != NodeState::Empty &&
            (key != m_nodes[index].m_key))
        {
            index = (index + 1) % m_capacity;
            if (index == hash % m_capacity)
            {
                break;
            }
        }

        if (m_nodes[index].m_state == NodeState::Occupied &&
            (key == m_nodes[index].m_key))
        {
            return &m_nodes[index];
        }

        return nullptr;
    }
};