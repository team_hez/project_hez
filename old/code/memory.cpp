#pragma once
#include "general.h"
#include "memory.h"

static MemoryRegion* g_globalRegion;

void* MemoryArena::push(size_t size)
{
    auto expectedSize = m_used + size;
    ASSERT(expectedSize <= m_size);
    void* mem = (u8*)m_memory + m_used;
    m_used = expectedSize;
    return mem;
}

void MemoryArena::beginTemp()
{
    ASSERT(!m_isTemp);
    m_isTemp = true;
    m_savedUsedSize = m_used;
}

void MemoryArena::endTemp()
{
    ASSERT(m_isTemp);
    m_isTemp = false;
    m_used = m_savedUsedSize;
    m_savedUsedSize = 0;
}

MemoryArena* MemoryArena::makeSubArena(size_t size)
{
    size_t headerSize = sizeof(MemoryArena);
    size_t totalAllocSize = headerSize + size;
    void* memory = push(totalAllocSize);
    auto subArena = (MemoryArena*)memory;
    subArena->init((u8*)memory + headerSize, size);
    return subArena;
}

void* MemoryRegion::alloc(int size)
{
    return debugAlloc(size, nullptr, nullptr, 0);
}

void MemoryRegion::free(void* p)
{
    auto block = (MemoryHeader*)((u8*)p - sizeof(MemoryHeader));
    ASSERT(block->m_id == m_id);

    m_used -= block->m_size;

    auto prev = block->m_prev;
    auto next = block->m_next;

    if (prev->m_free)
    {
        prev->m_next = block->m_next;
        prev->m_next->m_prev = prev;
        prev->m_size += block->m_size;
        if (m_freeBlock == block)
        {
            m_freeBlock = prev;
        }
        block = prev;
    }

    if (next->m_free)
    {
        block->m_next = next->m_next;
        block->m_next->m_prev = block;
        block->m_size += next->m_size;

        if (m_freeBlock == next)
        {
            m_freeBlock = block;
        }
    }

    block->m_tag = {};
    block->m_free = true;

    if (!m_freeBlock)
    {
        m_freeBlock = block;
    }
}

void* MemoryRegion::debugAlloc(int size, const char* file, const char* func, int line)
{
    ASSERT(m_freeBlock && "Failed to allocate memory : no free blocks");

    size += sizeof(MemoryHeader);

    auto block = m_freeBlock;
    auto end = block->m_prev;
    auto begin = block;

    while ((!(block->m_free && block->m_size >= size)) && block != end)
    {
        block = block->m_next;
    }

    ASSERT(block != end && "Failed to allocate memory : no enough sized blocks");

    block->m_tag.m_file = file;
    block->m_tag.m_func = func;
    block->m_tag.m_line = line;
    block->m_free = false;

    int extraSize = block->m_size - size;
    if (extraSize > sizeof(MemoryHeader))
    {
        auto newBlock = (MemoryHeader*)((u8*)block + size);
        newBlock->m_size = block->m_size - size;
        newBlock->m_id = m_id;
        newBlock->m_tag = {};
        newBlock->m_free = true;
        newBlock->m_prev = block;
        newBlock->m_next = block->m_next;
        newBlock->m_next->m_prev = newBlock;
        block->m_next = newBlock;
        block->m_size = size;
        m_freeBlock = block->m_next;
    }
    else
    {
        m_freeBlock = nullptr;
    }

    m_used += size;

    void* p = (u8*)block + sizeof(MemoryHeader);
    return p;
}

void initGlobalMemoryRegion(void* memory, int size)
{
    ASSERT(!g_globalRegion);
    const int GLOBAL_REGION_ID = 0x1d4a11;

    g_globalRegion = (MemoryRegion*)memory;
    g_globalRegion->m_size = size;
    g_globalRegion->m_used = sizeof(MemoryRegion);
    g_globalRegion->m_id = GLOBAL_REGION_ID;

    auto& head = g_globalRegion->m_head;
    head.m_size = g_globalRegion->m_size;
    head.m_id = g_globalRegion->m_id;
    head.m_free = false;
    auto block = head.m_prev = head.m_next = (MemoryHeader*)((u8*)memory + sizeof(MemoryRegion));
    block->m_size = size - sizeof(MemoryRegion);
    block->m_id = g_globalRegion->m_id;
    block->m_tag = {};
    block->m_free = true;
    block->m_prev = block->m_next = &g_globalRegion->m_head;

    g_globalRegion->m_freeBlock = block;
}

MemoryRegion* debugGetGlobalMemoryRegion()
{
    return g_globalRegion;
}

void* globalRegionMalloc(int size)
{
    ASSERT(g_globalRegion);
    return g_globalRegion->alloc(size);
}

void globalRegionFree(void* p)
{
    ASSERT(g_globalRegion);
    g_globalRegion->free(p);
}

void* debugGlobalRegionMalloc(int size, const char* file, const char* func, int line)
{
    ASSERT(g_globalRegion);
    return g_globalRegion->debugAlloc(size, file, func, line);
}

#if 0
void* operator new(size_t n, NewAllocStubArgument, const char* file, const char* func, int line)
{
    void* mem = debugGlobalRegionMalloc((int)(n), file, func, line);
    return mem;
}

void operator delete(void* p)
{
    FREE(p);
}

void* operator new[](size_t n, NewAllocStubArgument, const char* file, const char* func, int line)
{
    void* mem = debugGlobalRegionMalloc((int)(n), file, func, line);
    return mem;
}

void operator delete[](void* p)
{
    FREE(p);
}
#endif