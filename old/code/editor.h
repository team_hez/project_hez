#pragma once
#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#include "general.h"
#define NK_ASSERT(expression) ASSERT(expression)
#include <nuklear.h>
#include "renderergl.h"
#include "input.h"

static constexpr nk_flags EDITOR_WINDOW_DEFAULT =
NK_WINDOW_BORDER | NK_WINDOW_MOVABLE | NK_WINDOW_SCALABLE |
NK_WINDOW_MINIMIZABLE | NK_WINDOW_TITLE;

class Sound;
class Tilemap;
class b3World;
struct b3BoxHull;

class EditorGui
{
public:
    nk_context m_context;
    nk_font_atlas m_atlas;
    uint m_fontTexture;
    nk_draw_null_texture m_nullTexture;
    OpenglShader m_shader;
    OpenglRenderable m_glObject;
    nk_buffer m_cmds;    
    DWORD m_pausedPos;
    f32 m_volume;
    int m_isTilemapLayerSelector = 0;
    int m_selectedTilemapLayer = 0;

    EditorGui();
    ~EditorGui();

    void processInput(const GameInput& input);
    void soundPlayer(Sound& sound);
    void logger(const Logger& logger);
    void loggerLayout(const Logger& logger);
    void tilemapEditor(Tilemap& tileMap, MemoryArena& arena, b3World& world, const b3BoxHull* colliderShape);
    void tileMapEditorLayout(Tilemap& tileMap, MemoryArena& arena, b3World& world, const b3BoxHull* colliderShape);
    void lightEditor(Light& light);
    void lightEditorLayout(Light& light);
    void draw(int windowWidth, int windowHeight);
};