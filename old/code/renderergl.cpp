#include "renderergl.h"
#include "hmath.h"

static uint compileShader(uint type, const char* source)
{
    // TODO(illkwon): Handle other types of shader
    ASSERT(type == GL_VERTEX_SHADER || type == GL_FRAGMENT_SHADER);

    uint id = glCreateShader(type);

    const char* header;
    if (type == GL_VERTEX_SHADER)
    {
        header = "#version 330 core\n#define VERTEX_SHADER\n";
    }
    else
    {
        header = "#version 330 core\n#define FRAGMENT_SHADER\n";
    }

    Array<const char*, 2> sources =
    {
        header,
        source,
    };
    glShaderSource(id, (int)(sources.size()), sources.data(), nullptr);

    // Compile Shader
    glCompileShader(id);

    // Check Shader;
    int result;
    glGetShaderiv(id, GL_COMPILE_STATUS, &result);
    if (result == GL_FALSE)
    {
        int length;
        glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
        char* message = (char*)(alloca(length * sizeof(char)));
        glGetShaderInfoLog(id, length, &length, message);
        LOG("Failed to compile %s shader", type == GL_VERTEX_SHADER ? "vertex" : "fragment");
        LOG(message);
        glDeleteShader(id);
        return 0;
    }

    return id;
}

uint createShaderFromFile(const char* filename, MemoryArena& arena)
{
    arena.beginTemp();
    auto file = readEntireFile(filename, "r", arena);
    uint result = createShader((char*)(file.m_data));
    arena.endTemp();
    return result;
}

uint createShader(const char* source)
{
    uint program = glCreateProgram();

    // compile shader
    uint vertexShader = compileShader(GL_VERTEX_SHADER, source);
    uint fragmentShader = compileShader(GL_FRAGMENT_SHADER, source);
    SCOPE_EXIT({
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
    });

    // linking program
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);
    int result;
    glGetProgramiv(program, GL_LINK_STATUS, &result);
    if (result == GL_FALSE)
    {
        int length;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
        char* message = (char*)(alloca(length * sizeof(char)));
        glGetProgramInfoLog(program, length, &length, message);
        LOG("Failed to link shader program");
        LOG(message);
        glDeleteProgram(program);
        return 0;
    }
    glValidateProgram(program);
    glGetProgramiv(program, GL_VALIDATE_STATUS, &result);
    if (result == GL_FALSE)
    {
        int length;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
        char* message = (char*)(alloca(length * sizeof(char)));
        glGetProgramInfoLog(program, length, &length, message);
        LOG("Failed to validate shader program");
        LOG(message);
        glDeleteProgram(program);
        return 0;
    }

    return program;
}

void clearGLError()
{
    while (glad_glGetError() != GL_NO_ERROR) {}
}

bool checkGLError(const char* function, const char* file, int line)
{
    if (auto error = glad_glGetError(); error != GL_NO_ERROR)
    {
        LOG("[OpenGL Error] (%x): %s %s:%d", error, function, file, line);
        return false;
    }

    return true;
}

bool checkGLError(const char* function)
{
    if (auto error = glad_glGetError(); error != GL_NO_ERROR)
    {
        LOG("[OpenGL Error] (%x): %s", error, function);
        return false;
    }

	return true;
}

void OpenglMesh::box(MemoryArena& arena)
{
    m_vertexCount = 36;
    m_indexCount = m_vertexCount;
    m_vertices = arena.push<OpenglVertex>(m_vertexCount);
    m_indices = arena.push<uint>(m_indexCount);

    int i = 0;
    auto pushVertex = [&](const OpenglVertex& vertex)
    {
        m_vertices[i] = vertex;
        m_indices[i] = i;
        ++i;
    };

    pushVertex(OpenglVertex{ Vec3(-0.5f,-0.5f,-0.5f), Vec3(0.583f,  0.771f,  0.014f), Vec3(-1.0f, 0.0f, 0.0f) });
    pushVertex(OpenglVertex{ Vec3(-0.5f,-0.5f, 0.5f), Vec3(0.609f,  0.115f,  0.436f), Vec3(-1.0f, 0.0f, 0.0f) });
    pushVertex(OpenglVertex{ Vec3(-0.5f, 0.5f, 0.5f), Vec3(0.327f,  0.483f,  0.844f), Vec3(-1.0f, 0.0f, 0.0f) });
                                                         
    pushVertex(OpenglVertex{ Vec3(+0.5f, 0.5f,-0.5f), Vec3(0.822f,  0.569f,  0.201f), Vec3(0.0f, 0.0f, -1.0f) });
    pushVertex(OpenglVertex{ Vec3(-0.5f,-0.5f,-0.5f), Vec3(0.435f,  0.602f,  0.223f), Vec3(0.0f, 0.0f, -1.0f) });
    pushVertex(OpenglVertex{ Vec3(-0.5f, 0.5f,-0.5f), Vec3(0.310f,  0.747f,  0.185f), Vec3(0.0f, 0.0f, -1.0f) });
                                                         
    pushVertex(OpenglVertex{ Vec3(+0.5f,-0.5f, 0.5f), Vec3(0.597f,  0.770f,  0.761f), Vec3(0.0f, -1.0f, 0.0f) });
    pushVertex(OpenglVertex{ Vec3(-0.5f,-0.5f,-0.5f), Vec3(0.559f,  0.436f,  0.730f), Vec3(0.0f, -1.0f, 0.0f) });
    pushVertex(OpenglVertex{ Vec3(+0.5f,-0.5f,-0.5f), Vec3(0.359f,  0.583f,  0.152f), Vec3(0.0f, -1.0f, 0.0f) });
                                                         
    pushVertex(OpenglVertex{ Vec3(+0.5f, 0.5f,-0.5f), Vec3(0.483f,  0.596f,  0.789f), Vec3(0.0f, 0.0f, -1.0f) });
    pushVertex(OpenglVertex{ Vec3(+0.5f,-0.5f,-0.5f), Vec3(0.559f,  0.861f,  0.639f), Vec3(0.0f, 0.0f, -1.0f) });
    pushVertex(OpenglVertex{ Vec3(-0.5f,-0.5f,-0.5f), Vec3(0.195f,  0.548f,  0.859f), Vec3(0.0f, 0.0f, -1.0f) });
                                                         
    pushVertex(OpenglVertex{ Vec3(-0.5f,-0.5f,-0.5f), Vec3(0.014f,  0.184f,  0.576f), Vec3(-1.0f, 0.0f, 0.0f) });
    pushVertex(OpenglVertex{ Vec3(-0.5f, 0.5f, 0.5f), Vec3(0.771f,  0.328f,  0.970f), Vec3(-1.0f, 0.0f, 0.0f) });
    pushVertex(OpenglVertex{ Vec3(-0.5f, 0.5f,-0.5f), Vec3(0.406f,  0.615f,  0.116f), Vec3(-1.0f, 0.0f, 0.0f) });
                                                         
    pushVertex(OpenglVertex{ Vec3(+0.5f,-0.5f, 0.5f), Vec3(0.676f,  0.977f,  0.133f), Vec3(0.0f, -1.0f, 0.0f) });
    pushVertex(OpenglVertex{ Vec3(-0.5f,-0.5f, 0.5f), Vec3(0.971f,  0.572f,  0.833f), Vec3(0.0f, -1.0f, 0.0f) });
    pushVertex(OpenglVertex{ Vec3(-0.5f,-0.5f,-0.5f), Vec3(0.140f,  0.616f,  0.489f), Vec3(0.0f, -1.0f, 0.0f) });
                                                         
    pushVertex(OpenglVertex{ Vec3(-0.5f, 0.5f, 0.5f), Vec3(0.997f,  0.513f,  0.064f), Vec3(0.0f, 0.0f, 1.0f) });
    pushVertex(OpenglVertex{ Vec3(-0.5f,-0.5f, 0.5f), Vec3(0.945f,  0.719f,  0.592f), Vec3(0.0f, 0.0f, 1.0f) });
    pushVertex(OpenglVertex{ Vec3(+0.5f,-0.5f, 0.5f), Vec3(0.543f,  0.021f,  0.978f), Vec3(0.0f, 0.0f, 1.0f) });
                                                         
    pushVertex(OpenglVertex{ Vec3(+0.5f, 0.5f, 0.5f), Vec3(0.279f,  0.317f,  0.505f), Vec3(1.0f, 0.0f, 0.0f) });
    pushVertex(OpenglVertex{ Vec3(+0.5f,-0.5f,-0.5f), Vec3(0.167f,  0.620f,  0.077f), Vec3(1.0f, 0.0f, 0.0f) });
    pushVertex(OpenglVertex{ Vec3(+0.5f, 0.5f,-0.5f), Vec3(0.347f,  0.857f,  0.137f), Vec3(1.0f, 0.0f, 0.0f) });
                                                         
    pushVertex(OpenglVertex{ Vec3(+0.5f,-0.5f,-0.5f), Vec3(0.055f,  0.953f,  0.042f), Vec3(1.0f, 0.0f, 0.0f) });
    pushVertex(OpenglVertex{ Vec3(+0.5f, 0.5f, 0.5f), Vec3(0.714f,  0.505f,  0.345f), Vec3(1.0f, 0.0f, 0.0f) });
    pushVertex(OpenglVertex{ Vec3(+0.5f,-0.5f, 0.5f), Vec3(0.783f,  0.290f,  0.734f), Vec3(1.0f, 0.0f, 0.0f) });
                                                         
    pushVertex(OpenglVertex{ Vec3(+0.5f, 0.5f, 0.5f), Vec3(0.722f,  0.645f,  0.174f), Vec3(0.0f, 1.0f, 0.0f) });
    pushVertex(OpenglVertex{ Vec3(+0.5f, 0.5f,-0.5f), Vec3(0.302f,  0.455f,  0.848f), Vec3(0.0f, 1.0f, 0.0f) });
    pushVertex(OpenglVertex{ Vec3(-0.5f, 0.5f,-0.5f), Vec3(0.225f,  0.587f,  0.040f), Vec3(0.0f, 1.0f, 0.0f) });
                                                         
    pushVertex(OpenglVertex{ Vec3(+0.5f, 0.5f, 0.5f), Vec3(0.517f,  0.713f,  0.338f), Vec3(0.0f, 1.0f, 0.0f) });
    pushVertex(OpenglVertex{ Vec3(-0.5f, 0.5f,-0.5f), Vec3(0.053f,  0.959f,  0.120f), Vec3(0.0f, 1.0f, 0.0f) });
    pushVertex(OpenglVertex{ Vec3(-0.5f, 0.5f, 0.5f), Vec3(0.393f,  0.621f,  0.362f), Vec3(0.0f, 1.0f, 0.0f) });
                                                         
    pushVertex(OpenglVertex{ Vec3(+0.5f, 0.5f, 0.5f), Vec3(0.673f,  0.211f,  0.457f), Vec3(0.0f, 0.0f, 1.0f) });
    pushVertex(OpenglVertex{ Vec3(-0.5f, 0.5f, 0.5f), Vec3(0.820f,  0.883f,  0.371f), Vec3(0.0f, 0.0f, 1.0f) });
    pushVertex(OpenglVertex{ Vec3(+0.5f,-0.5f, 0.5f), Vec3(0.982f,  0.099f,  0.879f), Vec3(0.0f, 0.0f, 1.0f) });
}

void OpenglMesh::quad(MemoryArena & arena)
{
    m_vertexCount = 6;
    m_indexCount = m_vertexCount;
    m_vertices = arena.push<OpenglVertex>(m_vertexCount);
    m_indices = arena.push<uint>(m_indexCount);

    int i = 0;
    auto pushVertex = [&](const OpenglVertex& vertex)
    {
        m_vertices[i] = vertex;
        m_indices[i] = i;
        ++i;
    };

    pushVertex(OpenglVertex{ Vec3(-0.5f, 0.5f, 0.5f), Vec3(0.997f,  0.513f,  0.064f), Vec3(0.0f, 0.0f, 1.0f) });
    pushVertex(OpenglVertex{ Vec3(-0.5f,-0.5f, 0.5f), Vec3(0.945f,  0.719f,  0.592f), Vec3(0.0f, 0.0f, 1.0f) });
    pushVertex(OpenglVertex{ Vec3(+0.5f,-0.5f, 0.5f), Vec3(0.543f,  0.021f,  0.978f), Vec3(0.0f, 0.0f, 1.0f) });

    pushVertex(OpenglVertex{ Vec3(+0.5f, 0.5f, 0.5f), Vec3(0.673f,  0.211f,  0.457f), Vec3(0.0f, 0.0f, 1.0f) });
    pushVertex(OpenglVertex{ Vec3(-0.5f, 0.5f, 0.5f), Vec3(0.820f,  0.883f,  0.371f), Vec3(0.0f, 0.0f, 1.0f) });
    pushVertex(OpenglVertex{ Vec3(+0.5f,-0.5f, 0.5f), Vec3(0.982f,  0.099f,  0.879f), Vec3(0.0f, 0.0f, 1.0f) });
}

OpenglRenderable OpenglMesh::createMeshRenderable()
{
    return createOpenglRenderable(
        m_vertices, m_vertexCount,
        m_indices, m_vertexCount);
}

void OpenglRenderable::draw(
    OpenglShader shader,
    const Mat4& modelMatrix,
    const Mat4& viewMatrix,
    const Mat4& projMatrix, const Light& light)
{
    glBindVertexArray(m_vao);
    uint shaderID = shader.m_internalID;

    uint mvpMatrixID = glGetUniformLocation(shaderID, "MVP");
    uint viewMatrixID = glGetUniformLocation(shaderID, "V");
    uint modelMatrixID = glGetUniformLocation(shaderID, "M");
    uint lightPositionID = glGetUniformLocation(shaderID, "lightPosition_worldSpace");
    uint lightPowerID = glGetUniformLocation(shaderID, "lightPower");
    uint lightColorID = glGetUniformLocation(shaderID, "lightColor");

    // Camera
    Mat4 MVP = projMatrix * viewMatrix * modelMatrix;

    glUniformMatrix4fv(mvpMatrixID, 1, GL_FALSE, &MVP[0][0]);
    glUniformMatrix4fv(modelMatrixID, 1, GL_FALSE, &modelMatrix[0][0]);
    glUniformMatrix4fv(viewMatrixID, 1, GL_FALSE, &viewMatrix[0][0]);

    // Lighting
    glUniform3f(lightPositionID, light.m_position.x, light.m_position.y, light.m_position.z);
    glUniform1f(lightPowerID, light.m_power);
    glUniform3f(lightColorID, light.m_color.x, light.m_color.y, light.m_color.z);
        
    // Rendering
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    // glEnableVertexAttribArray(3);

    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
    glDrawElements(GL_TRIANGLES, m_indexCount, GL_UNSIGNED_INT, nullptr);
}

void OpenglRenderable::draw(OpenglShader shader, const Mat4& modelMatrix,
    const Mat4& viewMatrix,
    const Mat4& projMatrix)
{
    glBindVertexArray(m_vao);
    uint shaderID = shader.m_internalID;

    uint mvpMatrixID = glGetUniformLocation(shaderID, "MVP");
    Mat4 MVP = projMatrix * viewMatrix * modelMatrix;
    glUniformMatrix4fv(mvpMatrixID, 1, GL_FALSE, &MVP[0][0]);

    uint colorID = glGetUniformLocation(shaderID, "color");
    glUniform3f(colorID, 1.0f, 0.f, 0.f);

    // Rendering
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
    glDrawElements(GL_TRIANGLES, m_indexCount, GL_UNSIGNED_INT, nullptr);
}


OpenglRenderable createOpenglRenderable(OpenglVertex* vertices, int vertexCount, uint* indices, int indexCount)
{
    uint vao;
    uint vbo;
    uint ebo;
    
    glCreateBuffers(1, &vbo);
    glNamedBufferData(vbo, vertexCount * sizeof(OpenglVertex), vertices, GL_STATIC_DRAW);

    glCreateVertexArrays(1, &vao);

    glEnableVertexArrayAttrib(vao, 0); // Position
    glEnableVertexArrayAttrib(vao, 1); // Color
    glEnableVertexArrayAttrib(vao, 2); // Normal
    // glEnableVertexArrayAttrib(vao, 3); // UV

    // layout
    glVertexArrayAttribFormat(vao, 0, 3, GL_FLOAT, false, offsetof(OpenglVertex, m_pos));
    glVertexArrayAttribFormat(vao, 1, 3, GL_FLOAT, true, offsetof(OpenglVertex, m_color));
    glVertexArrayAttribFormat(vao, 2, 3, GL_FLOAT, true, offsetof(OpenglVertex, m_normal));

    glVertexArrayAttribBinding(vao, 0, 0);
    glVertexArrayAttribBinding(vao, 1, 0);
    glVertexArrayAttribBinding(vao, 2, 0);

    // Bind a buffer to a vertex buffer bind point
    glVertexArrayVertexBuffer(vao, 0, vbo, 0, sizeof(OpenglVertex));

    // Generate Buffer for indices
    glCreateBuffers(1, &ebo);
    glNamedBufferData(ebo, indexCount * sizeof(uint), indices, GL_STATIC_DRAW);

    return { vao, vbo, ebo, indexCount };
}

Mat4 createTransform(float xPos, float yPos, float zPos, float xScl, float yScl, float zScl, float xRot, float yRot, float zRot)
{
    Mat4 identity(1.0f);
    Mat4 scaleMat = scale(identity, Vec3(xScl, yScl, zScl));

    Mat4 rotZMat = rotate(identity, zRot, Vec3(0.0f, 0.0f, 1.0f));
    Mat4 rotYMat = rotate(identity, yRot, Vec3(0.0f, 1.0f, 0.0f));
    Mat4 rotXMat = rotate(identity, xRot, Vec3(1.0f, 0.0f, 0.0f));

    Mat4 transMat = translate(identity, Vec3(xPos, yPos, zPos));

    Mat4 result = transMat * rotXMat * rotYMat * rotZMat * scaleMat;

    return result;
}