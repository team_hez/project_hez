#pragma once

#include "platform.h"

static const int NUMBER_OF_BUTTONS = 14;

static const WORD XINPUT_GAMEPAD_BUTTONS[]
{
    XINPUT_GAMEPAD_A,
    XINPUT_GAMEPAD_B,
    XINPUT_GAMEPAD_A,
    XINPUT_GAMEPAD_B,
    XINPUT_GAMEPAD_X,
    XINPUT_GAMEPAD_Y,
    XINPUT_GAMEPAD_DPAD_UP,
    XINPUT_GAMEPAD_DPAD_DOWN,
    XINPUT_GAMEPAD_DPAD_LEFT,
    XINPUT_GAMEPAD_DPAD_RIGHT,
    XINPUT_GAMEPAD_LEFT_SHOULDER,
    XINPUT_GAMEPAD_RIGHT_SHOULDER,
    XINPUT_GAMEPAD_LEFT_THUMB,
    XINPUT_GAMEPAD_RIGHT_THUMB,
    XINPUT_GAMEPAD_START,
    XINPUT_GAMEPAD_BACK
};

struct XINPUT_GAMEPAD_BUTTONS_IDS
{
    XINPUT_GAMEPAD_BUTTONS_IDS()
    {
        A = 0;
        B = 1;
        X = 2;
        Y = 3;

        D_Up = 4;
        D_Down = 5;
        D_Left = 6;
        D_Right = 7;

        L_Bumper = 8;
        R_Bumper = 9;

        L_Thumbstick = 10;
        R_Thumbstick = 11;

        start = 12;
        back = 13;
    }

    int A, B, X, Y;
    int D_Up, D_Down, D_Left, D_Right;
    int L_Bumper, R_Bumper;
    int L_Thumbstick, R_Thumbstick;
    int start, back;
};

class ButtonState
{
public:
    bool m_isDown;
    bool m_isPressed;
    bool m_isReleased;

    void processInputEvent(bool isDown)
    {
        m_isDown = isDown;
        if (m_isDown)
        {
            m_isPressed = true;
        }
        else
        {
            m_isReleased = true;
        }
    }
};

class GameInput
{
public:
    union
    {
        class
        {
        public:
            ButtonState m_moveUp;
            ButtonState m_moveDown;
            ButtonState m_moveLeft;
            ButtonState m_moveRight;
            
            ButtonState m_actionUp;
            ButtonState m_actionDown;
            ButtonState m_actionLeft;
            ButtonState m_actionRight;

            ButtonState m_actionJump;
            ButtonState m_actionRoll;
            ButtonState m_actionRun;

            ButtonState m_mouseLeft;
            ButtonState m_mouseRight;
            ButtonState m_debugButton[6];
        };

        ButtonState m_buttons[19];
    };
    
    int m_mouseX;
    int m_mouseY;

    float m_dt;
    bool m_isWindowActive;
    int m_windowWidth;
    int m_windowHeight;


    // Gamepad Support
    GameInput(int index)
    {
        m_Index = index - 1;

        for (int i = 0; i < NUMBER_OF_BUTTONS; i++)
        {
            m_prevButtonStates[i] = false;
            m_currButtonStates[i] = false;
            m_buttonIsDown[i] = false;
        }
    }

    XINPUT_STATE GetState()
    {
        XINPUT_STATE result;
        ZeroMemory(&result, sizeof(XINPUT_STATE));
        XInputGetState(m_Index, &result);
        return result;
    }
    bool IsConnected()
    {
        ZeroMemory(&m_State, sizeof(XINPUT_STATE));
        DWORD result = XInputGetState(m_Index, &m_State);
    
        if (result == ERROR_SUCCESS)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    void GamepadUpdate()
    {
        m_State = GetState();

        for (int i = 0; i < NUMBER_OF_BUTTONS; i++)
        {
            m_currButtonStates[i] = (m_State.Gamepad.wButtons & XINPUT_GAMEPAD_BUTTONS[i]) == XINPUT_GAMEPAD_BUTTONS[i];

            m_buttonIsDown[i] = !m_prevButtonStates[i] && m_currButtonStates[i];
        }
    }
    void ResetButtons()
    {
        memcpy(m_prevButtonStates, m_currButtonStates, sizeof(m_prevButtonStates));
    }

    bool IsLeftDeadzone()
    {
        short currX = m_State.Gamepad.sThumbLX;
        short currY = m_State.Gamepad.sThumbLY;

        if (currX > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE ||
            currX < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
        {
            return false;
        }

        if (currY > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE ||
            currY < -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
        {
            return false;
        }

        return true;
    }
    bool IsRightDeadzone()
    {
        short currX = m_State.Gamepad.sThumbRX;
        short currY = m_State.Gamepad.sThumbRY;

        if (currX > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE ||
            currX < -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
        {
            return false;
        }

        if (currY > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE ||
            currY < -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
        {
            return false;
        }

        return true;
    }

    // return 0 = not triggered
    float LeftTrigger()
    {
        BYTE Trigger = m_State.Gamepad.bLeftTrigger;

        if (Trigger = m_State.Gamepad.bLeftTrigger)
            return Trigger / 255.0f;

        return 0.0f;
    }
    float RightTrigger()
    {
        BYTE Trigger = m_State.Gamepad.bRightTrigger;

        if (Trigger = m_State.Gamepad.bRightTrigger)
            return Trigger / 255.0f;

        return 0.0f;
    }

    float GetLeftX()
    {
        short x = m_State.Gamepad.sThumbLX;
        return (float)x / 32768.0f;
    }
    float GetLeftY()
    {
        short y = m_State.Gamepad.sThumbLY;
        return (float)y / 32768.0f;
    }
    float GetRightX()
    {
        short x = m_State.Gamepad.sThumbRX;
        return (float)x / 32768.0f;
    }
    float GetRightY()
    {
        short y = m_State.Gamepad.sThumbRY;
        return (float)y / 32768.0f;
    }

    bool GetButtonPressed(int index)
    {
        if (m_State.Gamepad.wButtons & XINPUT_GAMEPAD_BUTTONS[index])
        {
            return true;
        }

        return false;
    }
    bool GetButtonDown(int index)
    {
        return m_buttonIsDown[index];
    }

    void Rumble(const float lPower, const float rPower)
    {
        XINPUT_VIBRATION VibrationState;
        ZeroMemory(&VibrationState, sizeof(XINPUT_VIBRATION));
        
        int resultL = (int)(lPower * 65535.0f);
        int resultR = (int)(rPower * 65535.0f);

        VibrationState.wLeftMotorSpeed = (WORD)resultL;
        VibrationState.wRightMotorSpeed = (WORD)resultR;

        XInputSetState(m_Index, &VibrationState);
    }

    XINPUT_STATE m_State;
    int m_Index;
    bool m_prevButtonStates[NUMBER_OF_BUTTONS];
    bool m_currButtonStates[NUMBER_OF_BUTTONS];
    bool m_buttonIsDown[NUMBER_OF_BUTTONS];
};

extern XINPUT_GAMEPAD_BUTTONS_IDS XButtons;