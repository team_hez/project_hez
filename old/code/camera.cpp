#include "camera.h"
#include "platform.h"

Camera::Camera(float x,
    float y,
    float z,
    float yaw,
    float pitch,
    float fieldOfView,
    float range,
    float ratio,
    float mouseSpeed,
    float keyboardSpeed)
    : m_position(x, y, z)
    , m_transform(yaw, pitch)
    , m_fieldOfView(fieldOfView)
    , m_depthFar(range) , m_depthNear(0.1f) , m_ratio(ratio)
    , m_frustumWidth(0.0f) , m_frustumHeight(0.0f) , m_tang(0.0f)
    , m_mouseSpeed(mouseSpeed) , m_keyboardSpeed(keyboardSpeed)
{
}

void Camera::moveFreelook(const GameInput& input, int windowCenterX, int windowCenterY)
{
    // Freelook
    Vec2 leftControllerDt(0, 0);
    Vec2 rightControllerDt(0, 0);

    leftControllerDt.x = m_mouseSpeed * (windowCenterX - input.m_mouseX);
    leftControllerDt.y = m_mouseSpeed * (windowCenterY - input.m_mouseY);

    if (input.m_moveUp.m_isDown == true)
    {
        rightControllerDt.y = m_keyboardSpeed * input.m_dt;
    }
    if (input.m_moveDown.m_isDown == true)
    {
        rightControllerDt.y = -(m_keyboardSpeed * input.m_dt);
    }
    if (input.m_moveLeft.m_isDown == true)
    {
        rightControllerDt.x = -(m_keyboardSpeed * input.m_dt);
    }
    if (input.m_moveRight.m_isDown == true)
    {
        rightControllerDt.x = m_keyboardSpeed * input.m_dt;
    }
 
    m_transform.m_yaw += leftControllerDt.x;
    m_transform.m_pitch += leftControllerDt.y;
    m_transform.applyYawPitchToLookVector();

    // Check Keyboard Input
    m_position += m_transform.m_look * rightControllerDt.y;
    m_position += m_transform.right() * rightControllerDt.x;
}

void Camera::moveFollow(const GameInput& input, Vec3 target, float yaw, float pitch, float distance)
{
    m_transform.setYawPitchLook(yaw, pitch);
    m_position = target - m_transform.m_look * distance;
}

void Camera::updateFrustum()
{
    // static cam
    /*
    // Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
    m_projectMatrix = perspective(radToDeg(45.0f), (float)m_windowWidth / (float)m_windowHeight, 0.1f, 100.0f);

    // Or, for an ortho camera :
    //Mat4 Projection = ortho(-10.0f,10.0f,-10.0f,10.0f,0.0f,100.0f); // In world coordinates

    // Camera matrix
    m_viewMatrix = lookAt(
        Vec3(4, 4, 3), // Camera is at (4,3,3), in World Space
        Vec3(0, 0, 0), // and looks at the origin
        Vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
    );

    // Model matrix : an identity matrix (model will be at the origin)
    Mat4 Model = Mat4(1.0f);
    // Our ModelViewProjection : multiplication of our 3 matrices
    Mat4 mvp = m_projectMatrix * m_viewMatrix * Model; // Remember, matrix multiplication is the other way around
    */

    // frustums^
    m_tang = (float)(tan(degToRad(m_fieldOfView)));

    m_frustumHeight = m_depthNear * m_tang;
    m_frustumWidth = m_frustumHeight * m_ratio;

    Vec3 pointAt = (m_position + m_transform.m_look);
    Vec3 frustumZAxis = m_position - pointAt;
    m_frustumZ = normalize(frustumZAxis);

    Vec3 frustumXAxis = m_transform.up() * m_frustumZ;
    m_frustumX = normalize(frustumXAxis);

    m_frustumY = m_frustumZ * m_frustumX;
}

Mat4 Camera::getViewMatrix() const
{
    // Spherical to Cartesian
    auto result = lookAt
    (
        m_position, // Vec3(0, 0, 5),
        m_position + m_transform.m_look, //Vec3(0, 0, 0), 
        m_transform.up()
    );

    return result;
}

Mat4 Camera::getProjectionMatrix(const float ratio) const
{
    auto result = perspective(degToRad(m_fieldOfView), ratio, m_depthNear, m_depthFar);

    return result;
}

bool Camera::pointInView(const Vec3& point) const
{
    // get default vector
    Vec3 vec = point - m_position;

    // check depth
    float pdz = dot(vec, -m_frustumZ); // reverse frustumZ
    if (pdz > m_depthFar || pdz < m_depthNear)
    {
        return false;
    }
    
    // check Y
    float pdy = dot(vec, m_frustumY);
    float aux = pdz * m_tang;
    if (pdy > aux || pdy < -aux)
    {
        return false;
    }

    // check X
    float pdx = dot(vec, m_frustumX);
    aux = aux * m_ratio;
    if (pdx > aux || pdx < -aux)
    {
        return false;
    }

    return true;
}

bool Camera::pointInView(float x, float y, float z) const
{
    return pointInView(Vec3(x, y ,z));
}
