#include "game.h"
#include "platform.h"
#include "hmath.h"
#include "editor.h"
#include "tilemap.h"
#include "frame.h"
#include "general.h"

const Array<const char*, (size_t)DebugCameraMoveState::Last> DEBUG_CAMERA_MOVE_STATE_STRING =
{
    "DebugCameraMoveState::Idle",
    "DebugCameraMoveState::FreeLook",
    "DebugCameraMoveState::FollowPlayer",
};

const int BUTTON_SWITCH_CAMERA_MOVE_STATE = 0;
const int BUTTON_VSYNC_TOGGLE = 1;

void GameState::init(GameMemory& gameMemory, MemoryArena& tempArena)
{
    const int TEST_MAP_WIDTH = 10;
    const int TEST_MAP_HEIGHT = 10;
    char TEST_MAP_DATA[TEST_MAP_WIDTH * TEST_MAP_HEIGHT + 1] =
        ".wwwwwww.."
        "ww.....ww."
        "w.......ww"
        "w........w"
        "w........w"
        "w........w"
        "w........w"
        "w........w"
        "w......www"
        "wwwwwwww..";

    m_gameArena.init(
        (u8*)gameMemory.m_persistentMemory + sizeof(*this),
        gameMemory.m_persistentMemorySize - sizeof(*this));
    m_tilemapArena = m_gameArena.makeSubArena(mb(4));
    m_tilemapArena->push(&m_tilemap);
    new (m_tilemap) Tilemap(10, 10, 2, *m_tilemapArena);
    {
        auto layer = m_tilemap->m_layers[1];
        int tileCount = layer.m_xCount * layer.m_zCount;
        for (int i = 0; i < tileCount; ++i)
        {
            layer.m_tiles[i].m_type = (TEST_MAP_DATA[i] != '.');
        }
        layer.updateActualTileCount();
    }
    m_tilemap->updateAllDrawable();

    new (&m_camera) Camera(0, 0, 0, 3.14f, 0, 45, 200);

    new (&m_shader) OpenglShader((const char*)readEntireFile("base.shader", "r", tempArena).m_data);
    new (&m_uiShader) OpenglShader((const char*)readEntireFile("debugdraw.shader", "r", tempArena).m_data);

    m_tilemapMesh.box(tempArena);
    m_tilemapRenderable = m_tilemapMesh.createMeshRenderable();

    m_playerMesh.box(tempArena);
    
    tempArena.push(&m_player);
    m_player->m_entityRenderable = m_playerMesh.createMeshRenderable();

    m_uiMesh.quad(tempArena);
    m_uiRenderable = m_uiMesh.createMeshRenderable();

    new (&m_light) Light(Vec3(5.0f, 5.0f, 5.0f), Vec3(0.7f, 0.7f, 0.7f), 60.0f);
    m_player->m_speed = 11.25f;
    auto& groundLayer = m_tilemap->m_layers[m_tilemap->m_layerCount - 1];
    auto playerPos = groundLayer.getCenterGroundPos() + Vec3(0, 7.5f, 0);
    m_player->m_isRolling = false;
    m_player->m_roll = 0.0f;
    m_player->m_colType = CollisionType::Entity;
    m_player->m_entityType = EntityType::Player;
    m_player->m_isInfiniteHealth = false;
    m_player->m_isInfiniteStamina = false;

    m_light.m_position = playerPos + Vec3(0, 5, 0);
    m_camera.m_position = playerPos + Vec3(0, 0, 5);

    new (&m_world) b3World();
    m_world.SetGravity(b3Vec3(0.0f, -45.0f, 0.0f));
    m_world.SetWarmStart(true);

    new (&m_collisionSystem) CollisionSystem(m_world, m_gameArena);

    b3BodyDef playerBodyDef;
    playerBodyDef.position.Set(playerPos.x, playerPos.y, playerPos.z);
    playerBodyDef.fixedRotationX = true;
    playerBodyDef.fixedRotationY = true;
    playerBodyDef.fixedRotationZ = true;
    playerBodyDef.type = e_dynamicBody;

    m_player->m_body = m_world.CreateBody(playerBodyDef);

    //m_boxHull.SetIdentity();
    m_player->m_hull.Set(0.5f, 0.5f, 0.5f);

    b3HullShape hull;
    hull.m_hull = &m_player->m_hull;

    b3ShapeDef playerShapeDef;
    playerShapeDef.shape = &hull;
    playerShapeDef.density = 1.0f;
    playerShapeDef.friction = 0.0f;
    playerShapeDef.restitution = 0.0f;
    
    auto playerShape = m_player->m_body->CreateShape(playerShapeDef);
    playerShape->SetUserData(m_player);

    m_tilemap->buildTilemapBodyChunk(m_world, *m_tilemapArena);

    m_player->m_maxHealth = 100.0f;
    m_player->m_maxStamina = 100.0f;

    m_player->m_health = m_player->m_maxHealth;
    m_player->m_stamina = m_player->m_maxStamina;

    m_player->m_staminaRecoverySpeed = 10.0f;

    m_collisionSystem.createHitBox(Vec3(0.0f, 3.5f, 0.0f), Vec3(3.5f, 3.5f, 3.5f), DamageInfo(30.0f, 20.0f));
    m_collisionSystem.createHitBox(Vec3(4.0f, 3.5f, 0.0f), Vec3(3.5f, 3.5f, 3.5f), DamageInfo(50.0f, 12.0f));
    m_collisionSystem.createHitBox(Vec3(4.0f, 3.5f, 10.0f), Vec3(3.5f, 3.5f, 3.5f), DamageInfo(10.0f, 2.0f));
    m_collisionSystem.createHitBox(Vec3(4.0f, 3.5f, 18.0f), Vec3(3.5f, 3.5f, 3.5f), DamageInfo(20.0f, 0.0f));
    m_collisionSystem.createHitBox(Vec3(4.0f, 3.5f, 26.0f), Vec3(3.5f, 3.5f, 3.5f), DamageInfo(35.0f, 1.0f));

    m_goldCount = 0;
    m_potionCount = 0;
    m_hasAmulet = false;

    // Debug stuff
    m_cameraMoveState = DebugCameraMoveState::FollowPlayer;
    new (&m_editor) EditorGui();
    {
        glCreateBuffers(1, &m_debugDrawVbo);
        glNamedBufferData(m_debugDrawVbo, DEBUG_DRAW_VERTEX_BUFFER_SIZE, nullptr, GL_STREAM_DRAW);

        glCreateVertexArrays(1, &m_debugDrawVao);
        // TODO(illkwon): 버텍스의 레이아웃 정보를 자동으로 생성해낼수 있으면 더 좋을듯?
        glEnableVertexArrayAttrib(m_debugDrawVao, 0);
        glVertexArrayAttribFormat
        (
            m_debugDrawVao, 0,
            sizeof(DebugDrawVertex::m_pos) / sizeof(float),
            GL_FLOAT, GL_FALSE,
            offsetof(DebugDrawVertex, m_pos)
        );
        glVertexArrayAttribBinding(m_debugDrawVao, 0, 0);

        glVertexArrayVertexBuffer(m_debugDrawVao, 0, m_debugDrawVbo, 0, sizeof(DebugDrawVertex));

        new (&m_debugDrawShader) OpenglShader((const char*)readEntireFile("debugdraw.shader", "r", tempArena).m_data);
    }
}

void GameState::update(HWND window, GameInput& input, Frame& frame, MemoryArena& tempArena)
{
    // Update ratio in camera, for frustum
    m_camera.m_ratio = (float)input.m_windowWidth / (float)input.m_windowHeight;

    POINT windowCenter = { input.m_windowWidth / 2, input.m_windowHeight / 2 };

    if (input.m_isWindowActive && m_cameraMoveState == DebugCameraMoveState::FreeLook)
    {
        // Set Cursor to center
        POINT windowCenterInDisplay = windowCenter;
        ClientToScreen(window, &windowCenterInDisplay);
        SetCursorPos(windowCenterInDisplay.x, windowCenterInDisplay.y);
    }

    if (input.m_debugButton[BUTTON_SWITCH_CAMERA_MOVE_STATE].m_isPressed)
    {
        if (m_cameraMoveState != DebugCameraMoveState::Idle)
        {
            m_cameraMoveState = DebugCameraMoveState::Idle;
            debugCameraStateEnter(m_cameraMoveState, window, input);
        }
    }

    if (input.m_debugButton[BUTTON_VSYNC_TOGGLE].m_isPressed == true)
    {
        if (frame.m_vSync)
        {
            frame.m_vSync = false;
            frame.m_targetFps = 60; // TODO: Get monitorFrequency
        }
        else
        {
            frame.m_vSync = true;
        }
    }

    if (input.IsConnected())
    {
        FIXED_LOG("Gamepad is Connected");
    }
    else
    {
        FIXED_LOG("Gamepad is Not Connected");
    }

    FIXED_LOG("FPS: %d, MSPF: %f", (int)(1.f / frame.m_dt), frame.getMSPerFrame());
    if (frame.m_vSync == true)
    {
        FIXED_LOG("V Sync Enabled (Press F2 to disable)");
    }
    else
    {
        FIXED_LOG("V Sync Disabled (Press F2 to enable)");
    }

    // Editor
    float cameraPlayerFollowYaw = 0;
    float cameraPlayerFollowPitch = -30;
    float cameraPlayerFollowDistance = 5;
    {
        m_editor.processInput(input);
        auto& editorWindowStyle = m_editor.m_context.style.window;
        auto editorWindowColor = nk_rgba(45, 45, 45, 40);
        nk_style_push_color(&m_editor.m_context, &editorWindowStyle.background, editorWindowColor);
        nk_style_push_style_item(
            &m_editor.m_context, &editorWindowStyle.fixed_background,
            nk_style_item_color(editorWindowColor));
        SCOPE_EXIT(
            {
                nk_style_pop_color(&m_editor.m_context);
                nk_style_pop_style_item(&m_editor.m_context);
            }
        );

        auto ctx = &m_editor.m_context;
        if (nk_begin(ctx, "m_Editor Group", nk_rect(0, 0, (float)input.m_windowWidth * 0.45f, (float)input.m_windowHeight),
            EDITOR_WINDOW_DEFAULT))
        {
            if (nk_tree_push(ctx, NK_TREE_TAB, "Settings", NK_MINIMIZED))
            {
                SCOPE_EXIT(nk_tree_pop(ctx));
                nk_layout_row_dynamic(ctx, 60, 1);
                if (nk_group_begin(ctx, "Camera Move Mode (F1 to reset to Idle)", NK_WINDOW_TITLE | NK_WINDOW_NO_SCROLLBAR))
                {
                    SCOPE_EXIT(nk_group_end(ctx));
                    nk_layout_row_dynamic(ctx, 25, 1);
                    auto old = m_cameraMoveState;
                    m_cameraMoveState = (DebugCameraMoveState)nk_combo(ctx,
                        (const char**)DEBUG_CAMERA_MOVE_STATE_STRING.data(),
                        (int)DEBUG_CAMERA_MOVE_STATE_STRING.size(),
                        (int)m_cameraMoveState, 25, nk_vec2(ctx->current->bounds.w * 0.9f, 200));
                    if (m_cameraMoveState != old)
                    {
                        debugCameraStateEnter(m_cameraMoveState, window, input);
                    }
                }
            }
            if (nk_tree_push(ctx, NK_TREE_TAB, "DataView", NK_MINIMIZED))
            {
                SCOPE_EXIT(nk_tree_pop(ctx));

                m_editor.loggerLayout(debugGetGlobalLoggerInstance());
            }
            if (nk_tree_push(ctx, NK_TREE_TAB, "Tile Map Editor", NK_MINIMIZED))
            {
                SCOPE_EXIT(nk_tree_pop(ctx));

                m_editor.tileMapEditorLayout(*m_tilemap, *m_tilemapArena, m_world, &m_tileHull);
            }
            if (nk_tree_push(ctx, NK_TREE_TAB, "Light Control", NK_MINIMIZED))
            {
                SCOPE_EXIT(nk_tree_pop(ctx));

                m_editor.lightEditorLayout(m_light);
            }
            if (nk_tree_push(ctx, NK_TREE_TAB, "Camera Inspector", NK_MINIMIZED))
            {
                SCOPE_EXIT(nk_tree_pop(ctx));
                nk_layout_row_dynamic(ctx, 30, 2);
                static float yaw;
                nk_slider_float(ctx, 0, &yaw, 360, 1);
                nk_value_float(ctx, "yaw", yaw);
                static float pitch = -30;
                nk_slider_float(ctx, -90, &pitch, 0, 1);
                nk_value_float(ctx, "pitch", pitch);
                static float dist = 5;
                nk_slider_float(ctx, 5, &dist, 30, 1);
                nk_value_float(ctx, "distance", dist);
                cameraPlayerFollowYaw = yaw;
                cameraPlayerFollowPitch = pitch;
                cameraPlayerFollowDistance = dist;
            }
        }
        nk_end(ctx);
    }

    // Player
    updatePlayerMovement(frame.m_dt, input);
    updatePlayerStatus(frame.m_dt);

    /*static bool first = false;

    if (input.m_debugButton[2].m_isPressed && first == false)
    {
        m_collisionSystem.destroyHitBox(forDelete);
        first = true;
    }*/

    //World
    {
        m_world.Step(frame.m_dt, 8, 2);
    }

    m_light.m_position = m_player->getPlayerPos() + Vec3(0, 5, 0);

    // NOTE(illkwon): 카메라는 언제나 플레이어의 업데이트 이후의 최종 위치를 추적한다.
    {
        switch (m_cameraMoveState)
        {
        case DebugCameraMoveState::FreeLook:
            if (input.m_isWindowActive)
            {
                m_camera.moveFreelook(input, windowCenter.x, windowCenter.y);
            }
            break;
        case DebugCameraMoveState::FollowPlayer:
        {
            auto playerPos = m_player->getPlayerPos();
            m_camera.moveFollow(input, playerPos,
                degToRad(cameraPlayerFollowYaw),
                degToRad(cameraPlayerFollowPitch),
                cameraPlayerFollowDistance);
            b3Vec3 p1(playerPos.x, playerPos.y, playerPos.z);
            b3Vec3 p2(
                m_camera.m_position.x,
                m_camera.m_position.y,
                m_camera.m_position.z
            );
            b3RayCastSingleOutput raycastResult;
            CameraRaycastListener listener;
            listener.m_target = playerPos;
            listener.m_clippedCamPos = m_camera.m_position;
            m_world.RayCast(&listener, p1, p2);
            m_camera.m_position = listener.m_clippedCamPos;
        } break;
        }

        m_camera.updateFrustum();
        // Check Origin is drawing
        if (m_camera.pointInView(50.0f, 20.0f, 40.0f))
        {
            FIXED_LOG("visible");
        }
        else
        {
            FIXED_LOG("not visible");
        }
    }


    debugFlushGlobalLoggerBuffer();

    // Render 
    float debugDrawDepthOffset = 0.000005f;

    glEnable(GL_DEPTH_TEST);
    glDepthRange(debugDrawDepthOffset, 1.f);
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_SRC_ALPHA_SATURATE);

    glViewport(0, 0, input.m_windowWidth, input.m_windowHeight);
    glClearColor(0.0f, 0.0f, 0.4f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    auto viewMatrix = m_camera.getViewMatrix();
    auto projMatrix = m_camera.getProjectionMatrix((float)input.m_windowWidth / (float)input.m_windowHeight);

    glUseProgram(m_shader.m_internalID);

    // Player
    {
        auto playerPos = m_player->getPlayerPos();
        auto meshTransform = createTransform(
            playerPos.x, playerPos.y, playerPos.z,
            1.0f, 1.0f, 1.0f,
            m_player->m_entityRot.x, m_player->m_entityRot.y, m_player->m_entityRot.z
        );
        m_player->m_entityRenderable.draw(m_shader, meshTransform, viewMatrix, projMatrix, m_light);
    }

    // Tilemap
    m_tilemap->draw(m_tilemapRenderable, m_shader, viewMatrix, projMatrix, m_light);

    // basic UI
    {
        // no lighting edit for UI drawing : reuse debug drawing
        glUseProgram(m_debugDrawShader.m_internalID);
        SCOPE_EXIT(glUseProgram(0));

        Vec2 windowDims = { input.m_windowWidth * 0.5f, input.m_windowHeight * 0.5f };

        float hpScale = windowDims.x * (m_player->m_health / m_player->m_maxHealth);
        float spScale = windowDims.x * (m_player->m_stamina / m_player->m_maxStamina);
        float uiScale = windowDims.y * 0.09f;
        Vec3 hpPos = {};

        auto uiTransformHP = createTransform(
            windowDims.x - (hpScale * 0.5f), windowDims.y - (uiScale * 0.5f), 0.0f,
            hpScale, uiScale, uiScale,
            0.0f, 0.0f, 0.0f
        );

        auto uiTransformSP = createTransform(
            windowDims.x  - (spScale * 0.5f), windowDims.y - (uiScale * 1.5f), 0.0f,
            spScale, uiScale, uiScale,
            0.0f, 0.0f, 0.0f
        );

        auto orthoProjMatrix = ortho(-windowDims.x, windowDims.x, -windowDims.y, windowDims.y, m_camera.m_depthNear, m_camera.m_depthFar);
        auto orthoViewMatrix = lookAt(Vec3(0.0f, 0.0f, -1.0f), Vec3(), Vec3(0.0f, 1.0f, 0.0f));

        m_uiRenderable.draw(m_uiShader, uiTransformHP, orthoViewMatrix, orthoProjMatrix);
        m_uiRenderable.draw(m_uiShader, uiTransformSP, orthoViewMatrix, orthoProjMatrix);
    }
    

    // Draw collider volumes
    {
        glDepthRange(0.f, 1.f - debugDrawDepthOffset);
        SCOPE_EXIT(glDepthRange(debugDrawDepthOffset, 1.f));
        glUseProgram(m_debugDrawShader.m_internalID);
        SCOPE_EXIT(glUseProgram(0));
        uint mvpUniformLocation = glGetUniformLocation(m_debugDrawShader.m_internalID, "MVP");
        uint colorUniformLocation = glGetUniformLocation(m_debugDrawShader.m_internalID, "color");
        glUniform3f(colorUniformLocation, 1.f, 1.f, 1.f);

        glBindVertexArray(m_debugDrawVao);
        SCOPE_EXIT(glBindVertexArray(0));

        const auto& bodies = m_world.GetBodyList();
        const auto* body = bodies.m_head;
        for (uint bodyIndex = 0; bodyIndex < bodies.m_count; ++bodyIndex)
        {
            auto& transform = body->GetTransform();
            Mat4 translation = translate(Mat4(1.f), Vec3{ transform.position.x, transform.position.y, transform.position.z });
            Mat4 rotationScale = Mat4(1.f);
            //TODO(illkwon): rotation/scale잘 작동되는지 테스트 필요
            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 3; ++j)
                {
                    rotationScale[i][j] = transform.rotation[j][i];
                }
            }
            Mat4 modelMatrix = translation * rotationScale;
            auto mvp = projMatrix * viewMatrix * modelMatrix;
            glUniformMatrix4fv(mvpUniformLocation, 1, GL_FALSE, &mvp[0][0]);

            createTransform(
                transform.position.x, transform.position.y, transform.position.z);

            const auto& shapes = body->GetShapeList();
            const auto* shape = shapes.m_head;

            for (uint shapeIndex = 0; shapeIndex < shapes.m_count; ++shapeIndex)
            {
                switch (shape->GetType())
                {
                case b3ShapeType::e_hullShape:
                {
                    auto vertexBuffer = (DebugDrawVertex*)glMapNamedBuffer(m_debugDrawVbo, GL_WRITE_ONLY);
                    int vertexCount = 0;
                    auto pushVertex = [&vertexBuffer, &vertexCount](b3Vec3 v)
                    {
                        *vertexBuffer = DebugDrawVertex{ {v.x, v.y, v.z} };
                        ++vertexBuffer;
                        ++vertexCount;
                    };
                    auto hullShape = (b3HullShape*)shape;
                    auto hull = hullShape->m_hull;
                    for (uint faceIndex = 0; faceIndex < hull->faceCount; ++faceIndex)
                    {
                        auto face = hull->GetFace(faceIndex);
                        auto edge = hull->GetEdge(face->edge);
                        do
                        {
                            auto twin = hull->GetEdge(edge->twin);
                            auto v1 = hull->GetVertex(edge->origin);
                            auto v2 = hull->GetVertex(twin->origin);
                            pushVertex(v1);
                            pushVertex(v2);
                            edge = hull->GetEdge(edge->next);
                        } while (edge->next != face->edge);
                    }
                    glUnmapNamedBuffer(m_debugDrawVbo);

                    glDrawArrays(GL_LINES, 0, vertexCount);
                } break;
                default:
                    LOG("It's only handling hull shape for now");
                    break;
                }

                shape = shape->GetNext();
            }

            body = body->GetNext();
        }
    }

    // Editor
    m_editor.draw(input.m_windowWidth, input.m_windowHeight);
}

void GameState::updatePlayerMovement(float dt, const GameInput& input)
{
    if (m_cameraMoveState == DebugCameraMoveState::FreeLook)
    {
        return;
    }
    //looking direction. this is always unit vector from camera
    auto look = m_camera.m_transform.m_look; 
    //right vector from camera.
    auto right = m_camera.m_transform.right();
    
    bool isXZMove = false;
    Vec3 direction = {0.0f, 0.0f, 0.0f};

    if (m_player->m_isRolling == true)
    {
        if (dt >= 0.0f)
        {
            m_player->m_roll += dt;
        }
        isXZMove = true;

        if (m_player->m_roll >= 1.0f)
        {
            m_player->m_roll = 0.0f;
            m_player->m_entityRot = {0.0f, 0.0f, 0.0f};
            m_player->m_isRolling = false;
            isXZMove = false;
        }
    }

    if (input.m_moveUp.m_isDown == true)
    {
        Vec3 dir = { look.x, 0.0f, look.z};
        direction += glm::normalize(dir);
        isXZMove = true;
    }

    if (input.m_moveDown.m_isDown == true)
    {
        Vec3 dir = { -look.x, 0.0f, -look.z };
        direction += glm::normalize(dir);
        isXZMove = true;
    }

    if (input.m_moveLeft.m_isDown == true)
    {
        Vec3 dir = { -right.x, 0.0f, -right.z };
        direction += glm::normalize(dir);
        isXZMove = true;
    }

    if (input.m_moveRight.m_isDown == true)
    {
        Vec3 dir = { right.x, 0.0f, right.z };
        direction += glm::normalize(dir);
        isXZMove = true;
    }

    if (isXZMove == true)
    {
        if (input.m_actionRun.m_isDown == true) //state run
        {
            if (m_player->m_stamina > 10.0f)
            {
                setPlayerVelocityXZPlane(direction, m_player->m_speed);
                m_player->m_roll = 0.0f;
                m_player->m_entityRot = { 0.0f, 0.0f, 0.0f };
                m_player->m_isRolling = false;
                isXZMove = false;
                if (dt >= 0.0f)
                {
                    m_player->m_stamina -= m_player->m_staminaRecoverySpeed * dt;
                }
            }
            else
            {
                setPlayerVelocityXZPlane(direction, m_player->m_speed * 0.33f);
                if (dt >= 0.0f)
                {
                    m_player->m_stamina += 0.5f * m_player->m_staminaRecoverySpeed * dt;
                }
            }
        }
        else if (m_player->m_isRolling == true) //state roll
        {
            setPlayerVelocityXZPlane(m_player->m_rollingDirection, m_player->m_speed * 0.5f);
            
            b3Vec3 rotatingAxis = { m_player->m_rollingDirection.z, m_player->m_rollingDirection.y, -m_player->m_rollingDirection.x };

            auto rotation = b3Quat(rotatingAxis, TWO_PI_F * m_player->m_roll);
            m_player->m_entityRot = quatToEuler(rotation);
            m_player->m_entityRot.y = 0.0f;
            //FIXED_LOG("Rotation Axis : %f, %f, %f", rotatingAxis.x , rotatingAxis.y, rotatingAxis.z);
            //FIXED_LOG("Rotation Quat : %f, %f, %f", m_playerRot.x, m_playerRot.y, m_playerRot.z);

        }
        else //state walk
        {
            setPlayerVelocityXZPlane(direction, m_player->m_speed * 0.33f);
            if (dt >= 0.0f)
            {
                m_player->m_stamina += 0.5f * m_player->m_staminaRecoverySpeed * dt;
            }
        }
    }
    else
    {
        auto velocity = m_player->m_body->GetLinearVelocity();
        velocity.x = 0.0f;
        velocity.z = 0.0f;
        m_player->m_body->SetLinearVelocity(velocity);

        if (dt >= 0.0f)
        {
            m_player->m_stamina += m_player->m_staminaRecoverySpeed * dt;
        }
        
    }

    //jump
    if (input.m_actionJump.m_isPressed == true)
    {
        if (m_player->m_stamina > 10.0f)
        {
            m_player->m_body->ApplyForceToCenter(b3Vec3(0.0f, 925.0f, 0.0f), true);
            m_player->m_stamina -= 10.0f;
        }
    }
    //roll
    if (input.m_actionRoll.m_isPressed == true && m_player->m_isRolling == false)
    {
        if (glm::length(direction) > 0.0f && m_player->m_stamina > 30.0f)
        {
            m_player->m_isRolling = true;
            m_player->m_rollingDirection = direction;
            m_player->m_rollingDirection = glm::normalize(m_player->m_rollingDirection);
            m_player->m_stamina -= 30.0f;
        }
    }
}

void GameState::setPlayerVelocityXZPlane(const Vec3& direction, float speed)
{
    auto velocity = m_player->m_body->GetLinearVelocity();
    float tempVelocityYAxis = velocity.y;

    velocity.x = direction.x;
    velocity.y = 0.0f;
    velocity.z = direction.z;
    velocity.Normalize();

    velocity.x *= speed;
    velocity.y = tempVelocityYAxis;
    velocity.z *= speed;
    m_player->m_body->SetLinearVelocity(velocity);
}

void GameState::updatePlayerStatus(float dt)
{
    if (m_player->m_health <= 0.0f)
    {
        m_player->m_health = 0.0f;
    }

    if (m_player->m_stamina <= 0.0f)
    {
        m_player->m_stamina = 0.0f;
    }

    if (m_player->m_health >= m_player->m_maxHealth)
    {
        m_player->m_health = m_player->m_maxHealth;
    }

    if (m_player->m_stamina >= m_player->m_maxStamina)
    {
        m_player->m_stamina = m_player->m_maxStamina;
    }
}

void GameState::shutdown()
{
    m_collisionSystem.~CollisionSystem();
    m_tilemap->destroyTilemapBodyChunk(m_world, *m_tilemapArena);
    m_world.~b3World();
    m_editor.~EditorGui();
}

void debugCameraStateEnter(DebugCameraMoveState cameraMoveState, HWND window, const GameInput& input)
{
    POINT windowCenter =
    {
        input.m_windowWidth / 2,
        input.m_windowHeight / 2,
    };
    switch (cameraMoveState)
    {
    case DebugCameraMoveState::Idle:
        platformSetShowCursor(true);
        break;
    case DebugCameraMoveState::FreeLook:
    {
        platformSetShowCursor(false);
        // 이미 업데이트 함수에서 처리하고있는것 같아 일단 주석처리
        /*POINT windowCenterInDisplay = windowCenter;
        ClientToScreen(window, &windowCenterInDisplay);
        SetCursorPos(windowCenterInDisplay.x, windowCenterInDisplay.y);
        input.m_mouseX = windowCenter.x;
        input.m_mouseY = windowCenter.y;*/
    } break;
    case DebugCameraMoveState::FollowPlayer:
        platformSetShowCursor(true);
        break;
    }
}

void Entity::ApplyDamageInfo(const DamageInfo& damageInfo)
{
    if (m_isInfiniteHealth == false)
    {
        m_health -= damageInfo.m_deltaHP;
    }

    if (m_isInfiniteStamina == false)
    {
        m_stamina -= damageInfo.m_deltaSP;
    }
}
