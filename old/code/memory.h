#pragma once

// 메모리... 우주... 게임의 전체 메모리... 우주는 세상의 전체...
class GameMemory
{
public:
    size_t m_persistentMemorySize;
    size_t m_tempMemorySize;
    size_t m_totalSize;
    void* m_persistentMemory;
    void* m_tempMemory;
};

class MemoryArena
{
public:
    void* m_memory;
    size_t m_size;
    size_t m_used;
    size_t m_savedUsedSize;
    bool m_isTemp;

    void init(void* memory, size_t size)
    {
        m_memory = memory;
        m_size = size;
        m_used = 0;
        m_savedUsedSize = 0;
        m_isTemp = false;
    }

    template <typename T>
    void push(T** outObject, int count = 1)
    {
        *outObject = push<std::remove_reference_t<decltype(**outObject)>>(count);
    }
    template <typename T>
    T* push(int count = 1)
    {
        return (T*)push(sizeof(T) * count);
    }
    void* push(size_t size);
    void beginTemp();
    void endTemp();
    MemoryArena* makeSubArena(size_t size);
};

// TODO(illkwon): 아마 글로벌 메모리 할당 인터페이스를 가지는건 그리 좋은 아이디어는 아닐지도 모른다...
//#define MALLOC(n) debugGlobalRegionMalloc((int)(n), __FILE__, __FUNCTION__, __LINE__)
//#define FREE(p) globalRegionFree(p)
//#define FREE_PTR &globalRegionFree 
//#define MALLOC(n) malloc(n)
//#define FREE(p) free(p)
//#define FREE_PTR &free;

class MemoryHeader
{
public:
    class DebugTag
    {
    public:
        const char* m_file;
        const char* m_func;
        int m_line;
    };

    int m_size;// MemoryHeader를 포함한 블록의 사이즈
    int m_id;
    DebugTag m_tag;
    bool m_free;
    MemoryHeader* m_next;
    MemoryHeader* m_prev;
};

// 두개 이상 비어있는 메모리 블록은 절대 인접하지 않는다.
class MemoryRegion
{
public:
    int m_size;
    int m_used;
    int m_id;
    MemoryHeader m_head;
    MemoryHeader* m_freeBlock;

    void* alloc(int size);
    void free(void* p);
    void* debugAlloc(int size, const char* file, const char* func, int line);
};

void initGlobalMemoryRegion(void* memory, int size);
MemoryRegion* debugGetGlobalMemoryRegion();
void* globalRegionMalloc(int size);
void globalRegionFree(void* p);
void* debugGlobalRegionMalloc(int size, const char* file, const char* func, int line);

//TODO(illkwon)
#if 0
class NewAllocStubArgument
{
    int m_stub;
};

void* operator new(size_t n, NewAllocStubArgument, const char* file, const char* func, int line);
void operator delete(void* p);
void* operator new[](size_t n, NewAllocStubArgument, const char* file, const char* func, int line);
void operator delete[](void* p);

#define NEW new (NewAllocStubArgument{}, __FILE__, __FUNCTION__, __LINE__)
#endif

template <class T>
class StlRegionAllocator
{
public:
    typedef T value_type;

    StlRegionAllocator() = default;
    template <class U>
    StlRegionAllocator(const StlRegionAllocator<U>& u)
    {
    }
    ~StlRegionAllocator() = default;

    T* allocate(size_t n)
    {
        return (T*)MALLOC(n * sizeof(T));
    }

    void deallocate(T* p, size_t n)
    {
        FREE(p);
    }
};

template <class T1, class T2>
inline bool operator==(const StlRegionAllocator<T1>& a1, const StlRegionAllocator<T2>& a2)
{
    return true;
}
template <class T1, class T2>
inline bool operator!=(const StlRegionAllocator<T1>& a1, const StlRegionAllocator<T2>& a2)
{
    return false;
}