#include <cstdlib>
#include "general.h"

static Logger g_logger;
static bool g_isGlobalLoggerInited;

const Logger& debugGetGlobalLoggerInstance()
{
    return g_logger;
}
void debugInitGlobalLogger(void* memory, size_t size)
{
    ASSERT(!g_isGlobalLoggerInited);

    g_logger.m_buffer = (char*)memory;
    g_logger.m_size = size;
    g_logger.m_used = 1;// 마지막 문자는 항상 null이기 때문에 빈 문자열이라도 최소 1바이트는 차지한다.
    g_logger.m_lineCount = 0;

    g_isGlobalLoggerInited = true;
}
static void debugGlobalLoggerPush(const char* format, va_list argList)
{
    ASSERT(g_isGlobalLoggerInited);

    int writtenSize = vsnprintf(g_logger.m_buffer + g_logger.m_used - 1, g_logger.m_size - g_logger.m_used + 1, format, argList);
    ASSERT(writtenSize >= 0);
    ASSERT(g_logger.m_used + writtenSize <= g_logger.m_size);
    g_logger.m_used += writtenSize;
}
void debugGlobalLoggerPushNewLineLog(const char* format, ...)
{
    ASSERT(g_isGlobalLoggerInited);
    auto formatLength = strlen(format);
    char* lineFormat = (char*)(alloca(formatLength + 2));
    strcpy(lineFormat, format);
    lineFormat[formatLength] = '\n';
    lineFormat[formatLength + 1] = '\0';

    va_list argList;
    va_start(argList, format);
    debugGlobalLoggerPush(lineFormat, argList);
    va_end(argList);
    ++g_logger.m_lineCount;
}
void debugGlobalLoggerPushLog(const char* format, ...)
{
    ASSERT(g_isGlobalLoggerInited);
    va_list argList;
    va_start(argList, format);
    debugGlobalLoggerPush(format, argList);
    va_end(argList);
}
void debugFlushGlobalLoggerBuffer()
{
    ASSERT(g_isGlobalLoggerInited);
    g_logger.m_used = 1;
    g_logger.m_lineCount = 0;
}

/*
ReadFileData readEntireFile(const char* filename, const char* mode)
{
    FILE* file = fopen(filename, mode);
    SCOPE_EXIT(fclose(file));

    int fileSize = getFileSize(file);
    void* buffer = MALLOC(fileSize * sizeof(u8));
    memset(buffer, 0, fileSize * sizeof(u8));
    fread(buffer, sizeof(u8), fileSize, file);

    return { buffer, fileSize };
}
*/

ReadFileData readEntireFile(const char* filename, const char* mode, MemoryArena& arena)
{
    FILE* file = fopen(filename, mode);
    SCOPE_EXIT(fclose(file));

    int fileSize = getFileSize(file);
    void* buffer = arena.push(fileSize * sizeof(u8));
    memset(buffer, 0, fileSize * sizeof(u8));
    fread(buffer, sizeof(u8), fileSize, file);

    return { buffer, fileSize };
}