#include "asset.h"

// TODO(illkwon): 다시 생각해보자...
#if 0
class ObjImplToken
{
public:
    const char* start;
    const char* end;
};

static ObjImplToken getNextToken(const char* start, const char* end, const char* delims, int delimCount)
{
    auto checkDelim = [](char ch, const char* delims, int delimCount)
    {
        for (int i = 0; i < delimCount; ++i)
        {
            if (ch == delims[i])
                return true;
        }

        return false;
    };

    ObjImplToken token = {};
    token.start = start;
    while (checkDelim(*token.start, delims, delimCount) && token.start != end)
    {
        ++token.start;
    }

    token.end = token.start;
    while (!checkDelim(*token.end, delims, delimCount) && token.end != end)
    {
        ++token.end;
    }

    return token;
}

static ObjImplToken getNextToken(const char* start, const char* end)
{
    Array<char, 2> whitespaces = { ' ', '\t' };
    auto result = getNextToken(start, end, whitespaces.data(), (int)(whitespaces.size()));
    return result;
}


static Vec3 parseVec3(const char* start, const char* end)
{
    Vec3 result;
    auto token = getNextToken(start, end);
    result.x = strtof(token.start, nullptr);
    token = getNextToken(token.end, end);
    result.y = strtof(token.start, nullptr);
    token = getNextToken(token.end, end);
    result.z = strtof(token.start, nullptr);
    return result;
}

static Vec2 parseVec2(const char* start, const char* end)
{
    Vec2 result;
    auto token = getNextToken(start, end);
    result.x = strtof(token.start, nullptr);
    token = getNextToken(token.end, end);
    result.y = strtof(token.start, nullptr);
    return result;
};

static int parseInt(const char* start, const char* end)
{
    int result = 0;
    int sign = 1;

    const char* ch = start;
    while (ch != end)
    {
        if (*ch == '-')
        {
            sign = -1;
        }
        else if (*ch >= '0' && *ch <= '9')
        {
            int value = *ch - '0';
            result = result * 10 + value;
        }

        ++ch;
    }

    result *= sign;
    return result;
};


static ObjFaceIndex parseFaceIndex(const char* start, const char* end)
{
    ObjFaceIndex faceIndex = {};
    char delim = '/';
    ObjImplToken token = getNextToken(start, end, &delim, 1);
    faceIndex.m_vertexIndex = parseInt(token.start, token.end);
    token = getNextToken(token.end, end, &delim, 1);
    faceIndex.m_texCoordIndex = parseInt(token.start, token.end);
    token = getNextToken(token.end, end, &delim, 1);
    faceIndex.m_normalIndex = parseInt(token.start, token.end);

    return faceIndex;
}

static bool compareToken(const char* str, const ObjImplToken& token)
{
    int strLength = (int)(strlen(str));
    bool result = false;
    if (token.end - token.start == strLength &&
        strncmp(str, token.start, strLength) == 0)
    {
        result = true;
    }
    return result;
};

static void parseObj(const ReadFileData& file, ObjAsset* asset)
{
    const char* const start = (char*)(file.m_data);
    const char* ch = start;
    const char* endLine = ch;

    int requiredMemorySize = 0;

    int vertexCount = 0;
    int normalCount = 0;
    int texCoordCount = 0;
    int faceCount = 0;

    if (asset->mem)
    {
        asset->m_vertices.m_dataView = (Vec3*)(asset->mem);
        asset->m_normals.m_dataView = asset->m_vertices.end();
        asset->m_texCoords.m_dataView = (Vec2*)(asset->m_normals.end());
        asset->m_faces.m_dataView = (ObjFace*)(asset->m_texCoords.end());
    }

    while (ch - start < file.m_size)
    {
        while (*endLine != '\n' && (endLine - start < file.m_size))
        {
            ++endLine;
        }

        while (((*ch == ' ') || (*ch == '\t')) && ch != endLine)
        {
            ++ch;
        }

        if (*ch != '#' && *ch != '\n')
        {
            auto token = getNextToken(ch, endLine);

            if (compareToken("v", token))
            {
                ++vertexCount;
                if (asset->mem)
                {
                    Vec3 vertex = asset->m_vertices[vertexCount - 1] = parseVec3(token.end, endLine);
                    LOG("v: %f %f %f", vertex.x, vertex.y, vertex.z);
                }
            }
            else if (compareToken("vn", token))
            {
                ++normalCount;
                if (asset->mem)
                {
                    Vec3 normal = asset->m_normals[normalCount - 1] = parseVec3(token.end, endLine);
                    LOG("vn: %f %f %f", normal.x, normal.y, normal.z);
                }
            }
            else if (compareToken("vt", token))
            {
                ++texCoordCount;
                if (asset->mem)
                {
                    Vec2 texCoord = asset->m_texCoords[texCoordCount - 1] = parseVec2(token.end, endLine);
                    LOG("vt: %f %f", texCoord.x, texCoord.y);
                }
            }
            else if (compareToken("f", token))
            {
                ++faceCount;
                if (asset->mem)
                {
                    for (auto& faceIndex : asset->m_faces[faceCount - 1].m_indices)
                    {
                        token = getNextToken(token.end, endLine);
                        faceIndex = parseFaceIndex(token.start, token.end);
                    }
                    auto faceIndices = asset->m_faces[faceCount - 1].m_indices;
                    LOG("f: %d/%d/%d %d/%d/%d %d/%d/%d",
                        faceIndices[0].m_vertexIndex, faceIndices[0].m_texCoordIndex, faceIndices[0].m_normalIndex,
                        faceIndices[1].m_vertexIndex, faceIndices[1].m_texCoordIndex, faceIndices[1].m_normalIndex,
                        faceIndices[2].m_vertexIndex, faceIndices[2].m_texCoordIndex, faceIndices[2].m_normalIndex);
                }
            }
            else if (compareToken("mtllib", token))
            {
                //TODO(illkwon): Material
            }
            else if (compareToken("usemtl", token))
            {
                //TODO(illkwon): Material
            }
        }

        ++endLine;
        ch = endLine;
    }

    asset->m_vertices.m_size = vertexCount;
    asset->m_normals.m_size = normalCount;
    asset->m_texCoords.m_size = texCoordCount;
    asset->m_faces.m_size = faceCount;
}

void loadObj(const char* filename, ObjAsset* outObjAsset, MemoryArena& arena)
{
    arena.beginTemp();
    auto file = readEntireFile(filename, "r", arena);
    *outObjAsset = {};
    parseObj(file, outObjAsset);
    int totalMemorySize =
        outObjAsset->m_vertices.m_size * sizeof(Vec3) +
        outObjAsset->m_normals.m_size * sizeof(Vec3) +
        outObjAsset->m_texCoords.m_size * sizeof(Vec2) +
        outObjAsset->m_faces.m_size * sizeof(ObjFace);
    outObjAsset->mem = arena.push(totalMemorySize);
    parseObj(file, outObjAsset);
}

void freeObj(ObjAsset& objAsset)
{
    if (objAsset.mem)
    {
        FREE(objAsset.mem);
    }
}

#pragma pack(push, 1)
class VoxHeader
{
public:
    char m_name[4];
    int m_version;
};
class VoxChunkHeader
{
public:
    char m_id[4];
    int m_contentSize;
    int m_childrenSize;
};
class VoxChunkContentSize
{
public:
    int m_sizeX;
    int m_sizeY;
    int m_sizeZ;
};
class VoxChunkContentXYZI
{
public:
    int m_voxelCount;
};
class VoxChunkContentRGBA
{
public:
    uint m_palette[256];
};
#pragma pack(pop)

bool compareVoxChunkID(const VoxChunkHeader* chunk, const char* id)
{
    bool isSameID = (strncmp(chunk->m_id, id, arrayCount(chunk->m_id)) == 0);
    return isSameID;
};

static void nextVoxChunk(VoxChunkHeader** chunk, int* processedChildrenSize)
{
    *processedChildrenSize = (*chunk)->m_contentSize;
    *chunk = (VoxChunkHeader*)(
        (u8*)(*chunk) + sizeof(VoxChunkHeader) + (*chunk)->m_contentSize + (*chunk)->m_childrenSize);
}

static void* getVoxChunkContent(VoxChunkHeader* chunk)
{
    return ((u8*)chunk + sizeof(VoxChunkHeader));
}

class VoxModel
{
public:
    int m_sizeX;
    int m_sizeY;
    int m_sizeZ;
    int m_voxelCount;
    uint* m_voxels;
};

void loadVox(const char* filename, MemoryArena& arena)
{
    auto file = readEntireFile(filename, "rb", arena);
    SCOPE_EXIT(freeReadFile(file));
    auto bytes = (u8*)file.m_data;;
    auto vox = (VoxHeader*)bytes;
    ASSERT(strncmp(vox->m_name, "VOX ", 4) == 0 && vox->m_version == 150);
    auto mainChunk = (VoxChunkHeader*)(bytes + sizeof(VoxHeader));
    ASSERT(compareVoxChunkID(mainChunk, "MAIN"));
    int processedChildrenSize = 0;

    auto chunk = (VoxChunkHeader*)(
        (u8*)mainChunk + sizeof(VoxChunkHeader) + (mainChunk)->m_contentSize);

    int modelCount = 1;
    if (compareVoxChunkID(chunk, "PACK"))
    {
        modelCount = *(int*)((u8*)chunk + sizeof(VoxChunkHeader));
        nextVoxChunk(&chunk, &processedChildrenSize);
    }

    VoxModel* models = (VoxModel*)alloca(sizeof(VoxModel) * modelCount);
    int modelIndex = 0;

    for (int i = 0; i < modelCount; ++i)
    {
        auto& model = models[modelIndex];
        ASSERT(compareVoxChunkID(chunk, "SIZE"));
        {
            auto content = (VoxChunkContentSize*)getVoxChunkContent(chunk);
            model.m_sizeX = content->m_sizeX;
            model.m_sizeY = content->m_sizeY;
            model.m_sizeZ = content->m_sizeZ;
            nextVoxChunk(&chunk, &processedChildrenSize);
        }
        ASSERT(compareVoxChunkID(chunk, "XYZI"));
        {
            auto content = (VoxChunkContentXYZI*)getVoxChunkContent(chunk);
            auto voxelContents = (uint*)((u8*)content + sizeof(VoxChunkContentXYZI::m_voxelCount));
            model.m_voxelCount = content->m_voxelCount;
            model.m_voxels = (uint*)alloca(sizeof(uint) * model.m_voxelCount);
            for (int i = 0; i < model.m_voxelCount; ++i)
            {
                model.m_voxels[i] = voxelContents[i];
            }
            nextVoxChunk(&chunk, &processedChildrenSize);
        }
        ++modelIndex;
    }

    if (processedChildrenSize >= mainChunk->m_childrenSize)
    {
        return;
    }

    Array<uint, 256> palette = {};

    if (compareVoxChunkID(chunk, "RGBA"))
    {
        auto content = (VoxChunkContentRGBA*)getVoxChunkContent(chunk);
        for (int i = 0; i < 256; ++i)
        {
            palette[i] = content->m_palette[i];
        }
    }

    //TODO(illkwon): MATT청크 파싱
    nextVoxChunk(&chunk, &processedChildrenSize);
}
#endif
