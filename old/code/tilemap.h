#pragma once
#include "general.h"
#include "renderergl.h"
#include <list>
#include "collisionsystem.h"

const int MAX_LAYER_COUNT = 32;
const int MAX_TILE_COUNT = 1024;

class Tilemap;

class Tile
{
public:
    Tile(int x, int z, int layer, int type, Tilemap* tilemap);
    ~Tile();

    float getXPosition();
    float getYPosition();
    float getZPosition();
    Vec3 getPosition();

public:
    int m_x, m_z; 
    int m_layer;
    int m_type;
    bool m_isDrawable = true;
    bool m_hasBody = false;
    Mat4 m_tileModel;
    Tilemap* m_tilemap = nullptr;
};

class TileLayer
{
public:
    void init(int xCount, int zCount, int currentLayer, Tilemap* tilemap, MemoryArena& arena);
    void createTile(int x, int z, int layer, int type);
    void buildLayer(int prevXCount, int prevZCount, MemoryArena& arena);
    void updateActualTileCount();
    Tile& getTile(int x, int z);
    Vec3 getCenterGroundPos() const;
public:
    int m_xCount = 0;
    int m_zCount = 0;
    int m_currentLayer = 0;
    int m_actualTileCount = 0;
    Tilemap* m_tilemap = nullptr;
    Tile* m_tiles = nullptr;
};

struct b3BoxHull;
class b3World;
class b3Body;
class Camera;
class OpenglMesh;
class Tilemap
{
public:
    Tilemap(int xCount, int zCount, int layerCount, MemoryArena& arena);

    void draw(OpenglRenderable& tileRenderable, OpenglShader shader, const Mat4& viewMatrix, const Mat4& projMatrix, const Light& light);
    int getTileCount();
    void updateAllDrawable();
    void updateAdjDrawable(int layer, int x, int z, int type);
    int biggestZCount();
    int biggestXCount();
    bool loadTilemap(const char* filename, MemoryArena& arena);
    bool saveTilemap(const char* filename, MemoryArena& arena);
    void buildTilemapBodyChunk(b3World& world, MemoryArena& arena);
    void destroyTilemapBodyChunk(b3World& world, MemoryArena& arena);

    void buildTilemapMesh(OpenglMesh& mesh, MemoryArena& arena); //denied
    
public:
    class TilemapChunk
    {
    public:
        CollisionType type;
        b3Body* body = nullptr;
        b3BoxHull hull;
    };

public:
    float m_tileSize = 2.0f;
    int m_layerCount = 0;
    TileLayer* m_layers = nullptr;
    int m_isSelected[MAX_LAYER_COUNT];
    int m_prevBodyCount = 0;
    TilemapChunk* m_chunks = nullptr;
    int m_chunkCount = 0;
    size_t m_chunkStart = 0;
};
