#define NK_IMPLEMENTATION
#include <bounce\bounce.h>

#include "editor.h"
#include "sound.h"
#include "frame.h"
#include "tilemap.h"
#include "hmath.h"


class EditorVertex
{
public:
    Vec2 position;
    Vec2 uv;
    nk_byte col[4];
};

static constexpr nk_draw_vertex_layout_element g_vertex_layout[] = {
    {NK_VERTEX_POSITION, NK_FORMAT_FLOAT, NK_OFFSETOF(EditorVertex, position)},
    {NK_VERTEX_TEXCOORD, NK_FORMAT_FLOAT, NK_OFFSETOF(EditorVertex, uv)},
    {NK_VERTEX_COLOR, NK_FORMAT_R8G8B8A8, NK_OFFSETOF(EditorVertex, col)},
    {NK_VERTEX_LAYOUT_END}
};
static constexpr auto g_editorShaderSource =
R"(
#ifdef VERTEX_SHADER
uniform mat4 proj;
layout(location = 0) in vec2 position;
layout(location = 1) in vec2 texCoord;
layout(location = 2) in vec4 color;
out vec2 fragUV;
out vec4 fragColor;
void main()
{
    fragUV = texCoord;
    fragColor = color;
    gl_Position = proj * vec4(position.xy, 0, 1);
}
#endif
#ifdef FRAGMENT_SHADER
precision mediump float;
uniform sampler2D tex;
in vec2 fragUV;
in vec4 fragColor;
out vec4 outColor;
void main()
{
    outColor = fragColor * texture(tex, fragUV.st);
}
#endif
)";

EditorGui::EditorGui()
{
    m_volume = 1.0f;
    m_pausedPos = 0;

    nk_init_default(&m_context, 0);
    nk_font_atlas_init_default(&m_atlas);
    nk_font_atlas_begin(&m_atlas);    

    //TODO(illkwon): Modern Opengl api로 대체
    int w, h;
    auto image = nk_font_atlas_bake(&m_atlas, &w, &h, NK_FONT_ATLAS_RGBA32);
    glGenTextures(1, &m_fontTexture);
    glBindTexture(GL_TEXTURE_2D, m_fontTexture);
    glTextureParameteri(m_fontTexture, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTextureParameteri(m_fontTexture, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
    glBindTexture(GL_TEXTURE_2D, 0);

    nk_font_atlas_end(&m_atlas, nk_handle_id(m_fontTexture), &m_nullTexture);
    nk_style_set_font(&m_context, &m_atlas.default_font->handle);

    new (&m_shader) OpenglShader(g_editorShaderSource);

    uint vs = sizeof(EditorVertex);
    auto vp = offsetof(EditorVertex, position);
    auto vt = offsetof(EditorVertex, uv);
    auto vc = offsetof(EditorVertex, col);

    glCreateBuffers(1, &m_glObject.m_vbo);
    glCreateBuffers(1, &m_glObject.m_ebo);
    glCreateVertexArrays(1, &m_glObject.m_vao);

    
    auto positionAttrib = glGetAttribLocation(m_shader.m_internalID, "position");
    auto texCoordAttrib = glGetAttribLocation(m_shader.m_internalID, "texCoord");
    auto colorAttrib = glGetAttribLocation(m_shader.m_internalID, "color");
#if 1
    glBindVertexArray(m_glObject.m_vao);
    glBindBuffer(GL_ARRAY_BUFFER, m_glObject.m_vbo);

    glEnableVertexAttribArray(positionAttrib);
    glEnableVertexAttribArray(texCoordAttrib);
    glEnableVertexAttribArray(colorAttrib);
    glVertexAttribPointer(positionAttrib, 2, GL_FLOAT, GL_FALSE, vs, (void*)vp);
    glVertexAttribPointer(texCoordAttrib, 2, GL_FLOAT, GL_FALSE, vs, (void*)vt);
    glVertexAttribPointer(colorAttrib, 4, GL_UNSIGNED_BYTE, GL_TRUE, vs, (void*)vc);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
#else
    glEnableVertexArrayAttrib(glObject.vao, positionAttrib);
    glEnableVertexArrayAttrib(glObject.vao, texCoordAttrib);
    glEnableVertexArrayAttrib(glObject.vao, colorAttrib);
    glVertexArrayAttribFormat(glObject.vao, positionAttrib, 2, GL_FLOAT, false, vp);
    glVertexArrayAttribFormat(glObject.vao, texCoordAttrib, 2, GL_FLOAT, false, vt);
    glVertexArrayAttribFormat(glObject.vao, colorAttrib, 4, GL_UNSIGNED_BYTE, false, vc);
    glVertexArrayAttribBinding(glObject.vao, positionAttrib, 0);
    glVertexArrayAttribBinding(glObject.vao, texCoordAttrib, 1);
    glVertexArrayAttribBinding(glObject.vao, colorAttrib, 2);
    glVertexArrayVertexBuffer(glObject.vao, 0, glObject.vbo, 0, sizeof(EditorVertex));
    glVertexArrayVertexBuffer(glObject.vao, 1, glObject.vbo, 0, sizeof(EditorVertex));
    glVertexArrayVertexBuffer(glObject.vao, 2, glObject.vbo, 0, sizeof(EditorVertex));
#endif

    nk_buffer_init_default(&m_cmds);
}

EditorGui::~EditorGui()
{
    nk_font_atlas_clear(&m_atlas);
    nk_free(&m_context);
    glDeleteTextures(1, &m_fontTexture);
    nk_buffer_free(&m_cmds);
}

void EditorGui::processInput(const GameInput& input)
{
    nk_input_begin(&m_context);
    SCOPE_EXIT(nk_input_end(&m_context));
    if (input.m_mouseLeft.m_isPressed)
    {
        nk_input_button(&m_context, NK_BUTTON_LEFT, input.m_mouseX, input.m_mouseY, true);
    }
    else if (input.m_mouseLeft.m_isReleased)
    {
        nk_input_button(&m_context, NK_BUTTON_LEFT, input.m_mouseX, input.m_mouseY, false);
    }
    nk_input_motion(&m_context, input.m_mouseX, input.m_mouseY);
}

void EditorGui::soundPlayer(Sound& sound)
{
    auto ctx = &m_context;
    if (nk_begin(ctx, "Sound Player", nk_rect(280, 50, 300, 120), EDITOR_WINDOW_DEFAULT))
    {
        nk_layout_row_dynamic(ctx, 35, 3);
        if (nk_button_label(ctx, "Play"))
        {
            sound.playWaveFile(sound.m_secondaryBuffer, true, m_pausedPos);
        }
        if (nk_button_label(ctx, "Pause"))
        {
            m_pausedPos = sound.pauseWaveFile(sound.m_secondaryBuffer);
        }
        if (nk_button_label(ctx, "Stop"))
        {
            sound.stopWaveFile(sound.m_secondaryBuffer);
            m_pausedPos = 0;
        }

        nk_layout_row_dynamic(ctx, 25, 1);        
        m_volume = nk_propertyf(ctx, "#Volume:", 0.0f, m_volume, 1.0f, 0.01f, 0.005f);
        auto change = sound.calculateVolume(m_volume);
        sound.setVolumeDirectly(sound.m_secondaryBuffer, change);
    }
    nk_end(ctx);
}

void EditorGui::lightEditor(Light& light)
{
    auto ctx = &m_context;

    if (nk_begin(ctx, "Light Control", nk_rect(0, 150, 450, 400), EDITOR_WINDOW_DEFAULT))
    {
        lightEditorLayout(light);
    }
    nk_end(ctx);
}

void EditorGui::lightEditorLayout(Light& light)
{
    auto ctx = &m_context;

    nk_layout_row_dynamic(ctx, 400, 3);

    if (nk_group_begin(ctx, "Position", NK_WINDOW_BORDER | NK_WINDOW_MOVABLE | NK_WINDOW_TITLE))
    {
        nk_layout_row_dynamic(ctx, 30, 1); // nested row
        nk_label(ctx, "X", NK_TEXT_LEFT);
        nk_slider_float(ctx, -100.0f, &light.m_position.x, 100.0f, 0.1f);

        nk_layout_row_dynamic(ctx, 30, 1); // nested row
        nk_label(ctx, "Y", NK_TEXT_LEFT);
        nk_slider_float(ctx, -100.0f, &light.m_position.y, 100.0f, 0.1f);

        nk_layout_row_dynamic(ctx, 30, 1); // nested row
        nk_label(ctx, "Z", NK_TEXT_LEFT);
        nk_slider_float(ctx, -100.0f, &light.m_position.z, 100.0f, 0.1f);

        nk_group_end(ctx);
    }

    if (nk_group_begin(ctx, "Color", NK_WINDOW_BORDER | NK_WINDOW_MOVABLE | NK_WINDOW_TITLE))
    {
        nk_layout_row_dynamic(ctx, 30, 1); // nested row
        nk_label(ctx, "R", NK_TEXT_LEFT);
        nk_slider_float(ctx, 0, &light.m_color.r, 1.0f, 0.1f);

        nk_layout_row_dynamic(ctx, 30, 1); // nested row
        nk_label(ctx, "G", NK_TEXT_LEFT);
        nk_slider_float(ctx, 0, &light.m_color.g, 1.0f, 0.1f);

        nk_layout_row_dynamic(ctx, 30, 1); // nested row
        nk_label(ctx, "B", NK_TEXT_LEFT);
        nk_slider_float(ctx, 0, &light.m_color.b, 1.0f, 0.1f);

        nk_group_end(ctx);
    }

    if (nk_group_begin(ctx, "Power", NK_WINDOW_BORDER | NK_WINDOW_MOVABLE | NK_WINDOW_TITLE))
    {
        nk_layout_row_dynamic(ctx, 30, 1); // nested row
        nk_label(ctx, "Level", NK_TEXT_LEFT);
        nk_slider_float(ctx, 0, &light.m_power, 100.0f, 1.0f);


        nk_group_end(ctx);
    }
}

void EditorGui::logger(const Logger& logger)
{
    //TODO(illkwon): 정확한 윈도우 크기 계산 필요
    int rowHeight = 20;
    int height = logger.m_lineCount * rowHeight + 100;
    int maxLineWidth = 0;
    {
        const char* ch = logger.m_buffer;
        int lineWidth = 0;
        while (*ch != '\0')
        {
            if (*ch == '\n')
            {
                maxLineWidth = std::max(maxLineWidth, lineWidth);
                lineWidth = 0;
            }
            ++lineWidth;
            ++ch;
        }
        maxLineWidth = std::max(maxLineWidth, lineWidth);
        maxLineWidth *= 9;
    }

    if (nk_begin(&m_context, "DataView", nk_rect(0, 0, (float)(maxLineWidth), (float)(height)), EDITOR_WINDOW_DEFAULT))
    {
        loggerLayout(logger);
    }
    nk_end(&m_context);
}

void EditorGui::loggerLayout(const Logger& logger)
{
    auto ctx = &m_context;

    nk_layout_row_dynamic(ctx, 11, 1);
    const char* ch = logger.m_buffer;
    const char* start = ch;
    while (*ch != '\0')
    {
        if (*ch == '\n')
        {
            nk_text(ctx, start, (int)(ch - start), NK_TEXT_ALIGN_LEFT);
            start = ch + 1;
        }
        ++ch;
    }
    nk_text(ctx, start, (int)(ch - start), NK_TEXT_ALIGN_LEFT);
}

void EditorGui::tilemapEditor(Tilemap& tileMap, MemoryArena& arena, b3World& world, const b3BoxHull* colliderShape)
{
    auto ctx = &m_context;

    float windowSize = (float)tileMap.biggestXCount() * 45.0f;

    if (nk_begin(ctx, "Tile Map Editor", nk_rect(50, 50, windowSize, 600), EDITOR_WINDOW_DEFAULT))
    {
        tileMapEditorLayout(tileMap, arena, world, colliderShape);
    }
    
    nk_end(ctx);
}

void EditorGui::tileMapEditorLayout(Tilemap& tileMap, MemoryArena& arena, b3World& world, const b3BoxHull* colliderShape)
{
    auto ctx = &m_context;

    nk_layout_row_dynamic(ctx, 20, 1);
    nk_label(ctx, "TileMap Size Control", NK_TEXT_LEFT);

    nk_layout_row_dynamic(ctx, 35, 2);
    if (nk_button_label(ctx, "Save TileMap"))
    {
        tileMap.saveTilemap("tilemap.bin", arena);
    }
    if (nk_button_label(ctx, "Load TileMap"))
    {
        tileMap.loadTilemap("tilemap.bin", arena);
        for (int i = 0; i < tileMap.m_layerCount; ++i)
        {
            tileMap.m_layers[i].buildLayer(tileMap.m_layers[i].m_xCount, tileMap.m_layers[i].m_zCount, arena);
        }
        tileMap.updateAllDrawable();
        //tileMap.buildTilemapBody(world, colliderShape, arena);

        tileMap.buildTilemapBodyChunk(world, arena);
    }


    nk_layout_row_dynamic(ctx, 35, 2);
    if (nk_button_label(ctx, "Add Layer"))
    {
        ++tileMap.m_layerCount;
        if (tileMap.m_layerCount > MAX_LAYER_COUNT)
        {
            tileMap.m_layerCount = MAX_LAYER_COUNT;
        }

        for (int i = 0; i < tileMap.m_layerCount; ++i)
        {
            tileMap.m_layers[i].buildLayer(tileMap.m_layers[i].m_xCount, tileMap.m_layers[i].m_zCount, arena);
        }
        tileMap.updateAllDrawable();
        //tileMap.buildTilemapBody(world, colliderShape, arena);
        tileMap.buildTilemapBodyChunk(world, arena);
    }
    if (nk_button_label(ctx, "Del Layer"))
    {
        --tileMap.m_layerCount;
        if (tileMap.m_layerCount < 1)
        {
            tileMap.m_layerCount = 1;
        }

        for (int i = 0; i < tileMap.m_layerCount; ++i)
        {
            tileMap.m_layers[i].buildLayer(tileMap.m_layers[i].m_xCount, tileMap.m_layers[i].m_zCount, arena);
        }
        tileMap.updateAllDrawable();
        //tileMap.buildTilemapBody(world, colliderShape, arena);
        tileMap.buildTilemapBodyChunk(world, arena);
    }

    nk_layout_row_dynamic(ctx, 35, 2);
    if (nk_button_label(ctx, "Add Width"))
    {
        int currentTileCount = tileMap.getTileCount();
        for (int i = 0; i < MAX_LAYER_COUNT; ++i)
        {
            if (currentTileCount >= MAX_TILE_COUNT * MAX_LAYER_COUNT)
            {
                break;
            }

            tileMap.m_layers[i].m_xCount++;
            currentTileCount++;

            if (i < tileMap.m_layerCount)
            {
                tileMap.m_layers[i].buildLayer(tileMap.m_layers[i].m_xCount-1, tileMap.m_layers[i].m_zCount, arena);
            }
            tileMap.updateAllDrawable();
            //tileMap.buildTilemapBody(world, colliderShape, arena);
            tileMap.buildTilemapBodyChunk(world, arena);
        }
    }
    if (nk_button_label(ctx, "Del Width"))
    {
        int currentTileCount = tileMap.getTileCount();
        for (int i = 0; i < MAX_LAYER_COUNT; ++i)
        {
            if (currentTileCount <= 1 || tileMap.m_layers[i].m_xCount <= 1)
            {
                break;
            }

            tileMap.m_layers[i].m_xCount--;
            currentTileCount--;

            if (i < tileMap.m_layerCount)
            {
                tileMap.m_layers[i].buildLayer(tileMap.m_layers[i].m_xCount+1, tileMap.m_layers[i].m_zCount, arena);
            }
            tileMap.updateAllDrawable();
            //tileMap.buildTilemapBody(world, colliderShape, arena);
            tileMap.buildTilemapBodyChunk(world, arena);
        }
    }

    nk_layout_row_dynamic(ctx, 35, 2);
    if (nk_button_label(ctx, "Add Depth"))
    {
        int currentTileCount = tileMap.getTileCount();
        for (int i = 0; i < MAX_LAYER_COUNT; ++i)
        {
            if (currentTileCount >= MAX_TILE_COUNT * MAX_LAYER_COUNT)
            {
                break;
            }

            tileMap.m_layers[i].m_zCount++;
            currentTileCount++;

            if (i < tileMap.m_layerCount)
            {
                tileMap.m_layers[i].buildLayer(tileMap.m_layers[i].m_xCount, tileMap.m_layers[i].m_zCount-1, arena);
            }
            tileMap.updateAllDrawable();
            //tileMap.buildTilemapBody(world, colliderShape, arena);
            tileMap.buildTilemapBodyChunk(world, arena);
        }
    }
    if (nk_button_label(ctx, "Del Depth"))
    {
        int currentTileCount = tileMap.getTileCount();
        for (int i = 0; i < MAX_LAYER_COUNT; ++i)
        {
            if (currentTileCount <= 1 || tileMap.m_layers[i].m_zCount <= 1)
            {
                break;
            }

            tileMap.m_layers[i].m_zCount--;
            currentTileCount--;

            if (i < tileMap.m_layerCount)
            {
                tileMap.m_layers[i].buildLayer(tileMap.m_layers[i].m_xCount, tileMap.m_layers[i].m_zCount+1, arena);
            }
            tileMap.updateAllDrawable();
            //tileMap.buildTilemapBody(world, colliderShape, arena);
            tileMap.buildTilemapBodyChunk(world, arena);
        }
    }

    nk_layout_row_dynamic(ctx, 30, 1);
    nk_selectable_label(ctx, "Tile Layer Selectible", NK_TEXT_CENTERED, &m_isTilemapLayerSelector);

    if (m_isTilemapLayerSelector > 0)
    {
        nk_layout_row_dynamic(ctx, 30, 1);
        //TODO : std::string 대체 해야함. 또는 개선점 찾아야함.
        std::string nameOfLabel;
        for (int i = (tileMap.m_layerCount - 1); i >= 0; --i)
        {
            nameOfLabel = "Layer";
            nameOfLabel += std::to_string(i);

            if (nk_selectable_label(ctx, nameOfLabel.c_str(), NK_TEXT_CENTERED, &tileMap.m_isSelected[i]))
            {
                m_selectedTilemapLayer = i;

                for (int others = 0; others < tileMap.m_layerCount; ++others)
                {
                    if (others != i)
                    {
                        tileMap.m_isSelected[others] = 0;
                    }
                    else
                    {
                        tileMap.m_isSelected[others] = 1;
                    }
                }
            }
        }
    }
    else
    {
        nk_layout_row_dynamic(ctx, 30, 1);
        //TODO : std::string 대체 해야함. 또는 개선점 찾아야함.
        std::string nameOfLabel = "Layer";
        nameOfLabel += std::to_string(m_selectedTilemapLayer);

        nk_label(ctx, nameOfLabel.c_str(), NK_TEXT_CENTERED);
    }

    int zCount = tileMap.m_layers[m_selectedTilemapLayer].m_zCount;
    int xCount = tileMap.m_layers[m_selectedTilemapLayer].m_xCount;

    for (int z = 0; z < zCount; ++z)
    {
        nk_layout_row_dynamic(ctx, 30, xCount);
        for (int x = 0; x < xCount; ++x)
        {
            auto tile = std::to_string(x) + ", " + std::to_string(z);
            if (nk_selectable_label(ctx, tile.c_str(), NK_TEXT_CENTERED, &tileMap.m_layers[m_selectedTilemapLayer].m_tiles[z * xCount + x].m_type))
            {
                tileMap.updateAdjDrawable(m_selectedTilemapLayer, x, z, tileMap.m_layers[m_selectedTilemapLayer].m_tiles[z * xCount + x].m_type);
                //tileMap.buildTilemapBody(world, colliderShape, arena);
                tileMap.buildTilemapBodyChunk(world, arena);
            }
            /*int result = nk_propertyi(ctx, tile.c_str(), 0, tileMap.m_tileMap[m_selectedTilemapLayer].m_tiles[z * xCount + x].m_type, tileMap.m_tileType, 1, 0.005f);

            if (tileMap.m_tileMap[m_selectedTilemapLayer].m_tiles[z * xCount + x].m_type != result)
            {
                tileMap.m_tileMap[m_selectedTilemapLayer].m_tiles[z * xCount + x].m_type = result;
                tileMap.updateAdjDrawable(m_selectedTilemapLayer, x, z, tileMap.m_tileMap[m_selectedTilemapLayer].m_tiles[z * xCount + x].m_type);
            }*/
        }
    }
}

void EditorGui::draw(int windowWidth, int windowHeight)
{
    constexpr nk_anti_aliasing aa = NK_ANTI_ALIASING_OFF;
    constexpr int MAX_VERTEX_BUFFER = 512 * 1024;
    constexpr int MAX_ELEMENT_BUFFER = 128 * 1024;

    glViewport(0, 0, windowWidth, windowHeight);
    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_SCISSOR_TEST);

    float ortho[16] =
    {
        2.f / windowWidth, 0, 0, 0,
        0, -2.f / windowHeight, 0, 0,
        0, 0, -1, 0,
        -1, 1, 0, 1,
    };

    glUseProgram(m_shader.m_internalID);
    glUniform1i(glGetUniformLocation(m_shader.m_internalID, "tex"), 0);
    glUniformMatrix4fv(glGetUniformLocation(m_shader.m_internalID, "proj"), 1, GL_FALSE, ortho);
    glBindVertexArray(m_glObject.m_vao);
    glBindBuffer(GL_ARRAY_BUFFER, m_glObject.m_vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_glObject.m_ebo);
    glNamedBufferData(m_glObject.m_vbo, MAX_VERTEX_BUFFER, nullptr, GL_STREAM_DRAW);
    glNamedBufferData(m_glObject.m_ebo, MAX_ELEMENT_BUFFER, nullptr, GL_STREAM_DRAW);
    auto vertices = (EditorVertex*)glMapNamedBuffer(m_glObject.m_vbo, GL_WRITE_ONLY);
    auto elements = glMapNamedBuffer(m_glObject.m_ebo, GL_WRITE_ONLY);
    nk_convert_config config = {};
    config.vertex_layout = g_vertex_layout;
    config.vertex_size = sizeof(EditorVertex);
    config.vertex_alignment = alignof(EditorVertex);
    config.null = m_nullTexture;
    config.circle_segment_count = 22;
    config.curve_segment_count = 22;
    config.arc_segment_count = 22;
    config.global_alpha = 1.f;
    config.shape_AA = aa;
    config.line_AA = aa;

    nk_buffer vbuf, ebuf;
    nk_buffer_init_fixed(&vbuf, vertices, MAX_VERTEX_BUFFER);
    nk_buffer_init_fixed(&ebuf, elements, MAX_ELEMENT_BUFFER);
    nk_convert(&m_context, &m_cmds, &vbuf, &ebuf, &config);
    glUnmapNamedBuffer(m_glObject.m_vbo);
    glUnmapNamedBuffer(m_glObject.m_ebo);

    const nk_draw_index* offset = nullptr;
    const nk_draw_command* cmd;
    nk_draw_foreach(cmd, &m_context, &m_cmds)
    {
        if (cmd->elem_count > 0)
        {
            glBindTextureUnit(0, cmd->texture.id);
            glScissor(
                (int)(cmd->clip_rect.x),
                (int)(windowHeight - cmd->clip_rect.y - cmd->clip_rect.h),
                (int)(cmd->clip_rect.w),
                (int)(cmd->clip_rect.h));
            glDrawElements(GL_TRIANGLES, cmd->elem_count, GL_UNSIGNED_SHORT, offset);
            offset += cmd->elem_count;
        }
    }
    nk_clear(&m_context);

    glUseProgram(0);
    glBindVertexArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glDisable(GL_BLEND);
    glDisable(GL_SCISSOR_TEST);
}