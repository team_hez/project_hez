workspace "hez_ws"
    configurations {"Debug", "Release"}
    platforms {"win64"}
    warnings "Extra"

project "hez"
    kind "ConsoleApp"
    language "C++"
    targetdir "build/%{cfg.buildcfg}"
    systemversion "10.0.15063.0"
    flags {"MultiProcessorCompile", "NoMinimalRebuild", "StaticRuntime",
           "NoBufferSecurityCheck"}

    includedirs {"include"}
    libdirs {"lib"}
    debugdir "data"

    files {"**.h", "**.hpp", "**.cpp"}
    removefiles {"code/unittest.cpp"}
    removefiles {"code/external/glad_wgl.cpp", "code/external/glad.cpp"}
    removefiles {"code/external/bounce/**"}

    filter "platforms:win64"
        system "windows"
        architecture "x64"
        defines {"_CRT_SECURE_NO_WARNINGS"}
        buildoptions {"-std:c++latest"}
        links {"opengl32", "dsound", "dxguid", "winmm", "bounce_%{cfg.buildcfg}", "Xinput9_1_0"}
        disablewarnings {"4505", "4456", "4201", "4100", "4189", "4458", "4819", "4127", "4701", "4291"}

    filter "configurations:Debug"
        defines {"DEBUG"}
        symbols "On"

    filter "configurations:Release"
        optimize "On"

project "bounce"
    kind "StaticLib"
    language "C++"
    targetdir "build/%{cfg.buildcfg}"
    systemversion "10.0.15063.0"
    flags {"MultiProcessorCompile", "NoMinimalRebuild", "StaticRuntime",
           "NoBufferSecurityCheck"}

    postbuildcommands {"copy build\\%{cfg.buildcfg}\\bounce.lib lib\\bounce_%{cfg.buildcfg}.lib"}

    includedirs {"include"}
    debugdir "data"

    files {"code/external/bounce/**"}

    filter "platforms:win64"
        system "windows"
        architecture "x64"
        defines {"_CRT_SECURE_NO_WARNINGS"}
        buildoptions {"-std:c++latest"}
        disablewarnings {"4505", "4456", "4201", "4100", "4189", "4458", "4819", "4127", "4701", "4291"}

    filter "configurations:Debug"
        defines {"DEBUG"}
        symbols "On"

    filter "configurations:Release"
        optimize "On"


project "unittest"
    kind "ConsoleApp"
    language "C++"
    targetdir "build/%{cfg.buildcfg}"
    systemversion "10.0.15063.0"
    flags {"MultiProcessorCompile", "NoMinimalRebuild", "StaticRuntime",
           "NoBufferSecurityCheck", "NoIncrementalLink"}

    includedirs {"include"}

    files {"code/unittest.cpp"}

    filter "platforms:win64"
        system "windows"
        architecture "x64"
        defines {"_CRT_SECURE_NO_WARNINGS"}
        buildoptions {"-std:c++latest"}
        links {"opengl32"}
        disablewarnings {"4505", "4456", "4201", "4100", "4189", "4458", "4819", "4127"}

    filter "configurations:Debug"
        defines {"DEBUG"}
        symbols "On"

    filter "configurations:Release"
        optimize "On"
