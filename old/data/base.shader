#ifdef VERTEX_SHADER

// Input Vertex Data
layout(location = 0) in vec3 vertexPosition_modelspace;
// layout(location = 1) in vec2 vertexUV;
layout(location = 1) in vec3 vertexColor;
layout(location = 2) in vec3 vertexNormal_modelspace;
layout(location = 3) in vec2 vertexVisibility;

// Output Data - pass to fragment
// out vec2 UV;
out vec3 position_worldSpace;
out vec3 normal_cameraSpace;
out vec3 eyeDirection_cameraSpace;
out vec3 lightDirection_cameraSpace;
out vec3 fragmentColor;
out vec2 visibility;

// Constant Datas
uniform mat4 MVP;
uniform mat4 V;
uniform mat4 M;
uniform vec3 lightPosition_worldSpace;

void main()
{
	// Position of vertex
	gl_Position =  MVP * vec4(vertexPosition_modelspace,1);

	// Position of vertex in worldspace : model matrix * position
	position_worldSpace = (M * vec4(vertexPosition_modelspace,1)).xyz;

	// vector from vertex to camera - in camera space where camera position is origin
	vec3 vertexPosition_cameraspace = ( V * M * vec4(vertexPosition_modelspace,1)).xyz;
	eyeDirection_cameraSpace = vec3(0, 0, 0) - vertexPosition_cameraspace;

	// vector from vertex to light, in camera space
	// M is identity
	vec3 LightPosition_cameraspace = ( V * vec4(lightPosition_worldSpace, 1)).xyz;
	lightDirection_cameraSpace = LightPosition_cameraspace + eyeDirection_cameraSpace;

	// Normal of vertex, in camera space
	// only correct if M does not scale the model. use its inverse if not.
	normal_cameraSpace = ( V * M * vec4(vertexNormal_modelspace,0) ).xyz;

	// UV of the vertex
	// UV = vertexUV;
    fragmentColor = vertexColor;
    visibility = vertexVisibility;
}
#endif



#ifdef FRAGMENT_SHADER
in vec3 position_worldSpace;
in vec3 normal_cameraSpace;
in vec3 eyeDirection_cameraSpace;
in vec3 lightDirection_cameraSpace;
in vec3 fragmentColor;
in vec2 visibility;

out vec3 outColor;

uniform vec3 lightPosition_worldSpace;
uniform float lightPower;
uniform vec3 lightColor;

void main()
{
	//if (visibility.x == 0.0)
	//{
	//	discard;
	//}

	// Material properties
	vec3 materialDiffuseColor = fragmentColor;
	vec3 materialAmbientColor = vec3(0.1, 0.1, 0.1) * materialDiffuseColor;
	vec3 materialSpecularColor = vec3(0.4, 0.4, 0.4);

	// Light Distance
	float distance = length(lightPosition_worldSpace - position_worldSpace);

	vec3 n = normalize( normal_cameraSpace ); 			// Normal of fragment in camera space
	vec3 l = normalize( lightDirection_cameraSpace );   // Direction of the light
	float cosTheta = clamp( dot(n,l), 0, 1);

	vec3 E = normalize(eyeDirection_cameraSpace);		// Eye vector
	vec3 R = reflect(-l, n);							// Direction of reflection
	float cosAlpha = clamp( dot(E,R), 0, 1);			// cosine of the angle between eye-reflect

    outColor = 
    	materialAmbientColor +
    	materialDiffuseColor * lightColor * lightPower * cosTheta / (distance * distance) + 
    	materialSpecularColor * lightColor * lightPower * pow(cosAlpha, 5) / (distance * distance);
}
#endif