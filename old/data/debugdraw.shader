#ifdef VERTEX_SHADER

layout(location = 0) in vec3 vertexPosition;

uniform mat4 MVP;

void main()
{
	// Position of vertex
	gl_Position = MVP * vec4(vertexPosition.xyz, 1);
	//gl_Position = vec4(vertexPosition.xy, 0, 1);
}
#endif


#ifdef FRAGMENT_SHADER

out vec3 outColor;

uniform vec3 color;

void main()
{
    outColor = color;
}

#endif